﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    /// <summary>
    /// the data access implementation for <see cref="IGroupDataAccess"/>
    /// </summary>
    public partial class GroupeDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="GroupeDataAccess"/>
    /// </summary>
    public partial class GroupeDataAccess : DataAccess<Group, int>, IGroupDataAccess
    {
        public GroupeDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory) : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Group> SetDefaultIncludsForListRetrieve(IQueryable<Group> query)
            => query.Include(e => e.Clients);

        protected override IQueryable<Group> SetDefaultIncludsForSingleRetrieve(IQueryable<Group> query)
            => query.Include(e => e.Clients);
    }
}
