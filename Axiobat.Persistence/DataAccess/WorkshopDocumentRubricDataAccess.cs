﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    public partial class WorkshopDocumentRubricDataAccess : DataAccess<WorkshopDocumentRubric, string>, IWorkshopDocumentRubricDataAccess
    {
        public WorkshopDocumentRubricDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory) : base(context, loggerFactory)
        {
        }

        protected override IQueryable<WorkshopDocumentRubric> SetPagedResultFilterOptions<IFilter>(
            IQueryable<WorkshopDocumentRubric> query, IFilter filterOption)
        {
            if (filterOption is RubricFilterOptions filtre)
            {
                if (filtre.WorkshopId.IsValid())
                    query = query.Where(e => e.WorkshopId == filtre.WorkshopId);
            }

            return query;
        }

        protected override IQueryable<WorkshopDocumentRubric> SetDefaultIncludsForListRetrieve(IQueryable<WorkshopDocumentRubric> query)
            => query.Include(e => e.Documents);
    }
}
