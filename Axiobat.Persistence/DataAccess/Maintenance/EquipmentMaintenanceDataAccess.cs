﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using DataContext;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the data access implantation for <see cref="IEquipmentMaintenanceDataAccess"/>
    /// </summary>
    public partial class EquipmentMaintenanceDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="EquipmentMaintenanceDataAccess"/>
    /// </summary>
    public partial class EquipmentMaintenanceDataAccess : DataAccess<EquipmentMaintenance, string>, IEquipmentMaintenanceDataAccess
    {
        public EquipmentMaintenanceDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }
    }
}
