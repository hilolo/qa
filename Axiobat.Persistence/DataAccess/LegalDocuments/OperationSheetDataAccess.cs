﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Axiobat.Domain.Constants;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Threading.Tasks;

    /// <summary>
    /// data access implementation for <see cref="IOperationSheetDataAccess"/>
    /// </summary>
    public partial class OperationSheetDataAccess : DataAccess<OperationSheet, string>, IOperationSheetDataAccess
    {
        /// <summary>
        /// add the operation sheet technicians
        /// </summary>
        /// <param name="added">the list of operation sheets to be added</param>
        public async Task AddOperationSheetTechniciansAsync(IEnumerable<OperationSheets_Technicians> added)
        {
            try
            {
                _context.OperationSheets_Technicians.AddRange(added);
                await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to add the list of operation sheets technicians");
            }
        }

        /// <summary>
        /// remove the operation sheet technicians
        /// </summary>
        /// <param name="removed">the list of operation sheets to remove</param>
        public async Task RemoveOperationSheetTechniciansAsync(IEnumerable<OperationSheets_Technicians> removed)
        {
            try
            {
                _context.OperationSheets_Technicians.RemoveRange(removed);
                await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to remove the list of operation sheets technicians");
            }
        }

        /// <summary>
        /// get list of operation sheets owned by the given client
        /// </summary>
        /// <param name="clientId">the id of the client</param>
        /// <returns>the list of operation sheets</returns>
        public Task<IEnumerable<OperationSheet>> GetOperationSheetsByClientIdAsync(string clientId)
            => _context.OperationSheets.Where(e => e.ClientId == clientId).ToIEnumerableAsync();

        /// <summary>
        /// update the status of all operation sheets associated with the given invoice
        /// </summary>
        /// <param name="invoiceId">the id of the invoice</param>
        /// <param name="status">the new status</param>
        public Task UpdateInvoiceOperationSheetStatusAsync(string invoiceId, string status)
        {
            var sql = $"UPDATE {TablesNames.OperationSheets} SET {nameof(OperationSheet.Status)} = '{status}' WHERE {nameof(OperationSheet.InvoiceId)} = '{invoiceId}'";
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return _context.Database.ExecuteSqlCommandAsync(sql);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        public Task<IEnumerable<OperationSheet>> GetTechnicianOperationSheetsAsync(Guid technicianId)
        {
            return _context.OperationSheets
                .Join(_context.OperationSheets_Technicians, o => o.Id, ot => ot.OperationSheetId,
                    (o, ot) => new { OperationSheets = o, OperationSheetsTechnician = ot })
                .Where(q => q.OperationSheetsTechnician.TechnicianId == technicianId)
                .Select(e => e.OperationSheets)
                .ToIEnumerableAsync();
        }

        public Task<OperationSheet[]> GetNewestObjectsAsync(DateTimeOffset? lastSyncDate, string[] syncObjectsIds)
        {
            return _context.OperationSheets
                .Include(e => e.Technicians)
                    .ThenInclude(e => e.Technician)
                .Where(e => e.LastModifiedOn > lastSyncDate && !syncObjectsIds.Contains(e.Id))
                .ToArrayAsync();
        }

        public Task<OperationSheet[]> GetFirstsynchronizationDataAsync()
        {
            return _context.OperationSheets
                   .Include(e => e.Technicians)
                       .ThenInclude(e => e.Technician)
                   .ToArrayAsync();
        }
    }

    /// <summary>
    /// partial part for <see cref="OperationSheetDataAccess"/>
    /// </summary>
    public partial class OperationSheetDataAccess : DataAccess<OperationSheet, string>, IOperationSheetDataAccess
    {
        public OperationSheetDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<OperationSheet> SetPagedResultFilterOptions<IFilter>(IQueryable<OperationSheet> query, IFilter filterOption)
        {
            if (filterOption is OperationSheetFilterOptions filter)
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.StartDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.EndDate < filter.DateEnd);

                if (filter.WorkshopId.IsValid())
                    query = query.Where(e => e.WorkshopId == filter.WorkshopId);

                if (filter.ExternalPartnerId.IsValid())
                    query = query.Where(e => e.ClientId == filter.ExternalPartnerId);

                if (filter.Status.Any())
                {
                    var status = new HashSet<string>(filter.Status);

                    var isFirst = true;
                    var predicate = PredicateBuilder.True<OperationSheet>();
                    if (filter.Status.Contains(OperationSheetStatus.Late))
                    {
                        predicate = predicate.And(e => e.Status == OperationSheetStatus.Planned && e.EndDate.Date < DateTime.Today);
                        status.Remove(OperationSheetStatus.Late);
                        status.Remove(OperationSheetStatus.Planned);
                        isFirst = false;
                    }
                    if (filter.Status.Contains(OperationSheetStatus.Planned))
                    {
                        if (isFirst)
                            predicate = predicate.And(e => e.Status == OperationSheetStatus.Planned && e.EndDate.Date >= DateTime.Today);
                        else
                            predicate = predicate.Or(e => e.Status == OperationSheetStatus.Planned && e.EndDate.Date >= DateTime.Today);

                        status.Remove(OperationSheetStatus.Planned);
                        isFirst = false;
                    }
                    if (status.Any())
                    {
                        if (isFirst)
                            predicate = predicate.And(e => status.Contains(e.Status));
                        else
                            predicate = predicate.Or(e => status.Contains(e.Status));
                    }

                    query = query.Where(predicate);
                }

                if (filter.TechniciansId.Any())
                {
                    query = query
                        .Join(_context.OperationSheets_Technicians, p => p.Id, s => s.OperationSheetId, (o, ot) => new { OperationSheet = o, OperationSheets_Technicians = ot })
                        .Where(q => filter.TechniciansId.Contains(q.OperationSheets_Technicians.TechnicianId))
                        .Select(q => q.OperationSheet);
                }
            }

            return query;
        }

        protected override IQueryable<OperationSheet> SetDefaultIncludsForSingleRetrieve(IQueryable<OperationSheet> query)
            => query
                .Include(e => e.Invoice)
                .Include(e => e.Quote)
                .Include(e => e.Workshop)
                .Include(e => e.Technicians)
                    .ThenInclude(e => e.Technician);

        protected override IQueryable<OperationSheet> SetDefaultIncludsForListRetrieve(IQueryable<OperationSheet> query)
            => query
                .Include(e => e.Workshop)
                .Include(e => e.Technicians)
                    .ThenInclude(e => e.Technician);
    }
}
