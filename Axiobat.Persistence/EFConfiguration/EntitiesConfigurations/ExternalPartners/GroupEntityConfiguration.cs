﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class GroupEntityConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.ToTable("Groups");

            builder.HasMany(e => e.Clients)
                .WithOne(e => e.Groupe)
                .HasForeignKey(e => e.GroupeId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasQueryFilter(e => !e.IsDeleted);
        }
    }
}
