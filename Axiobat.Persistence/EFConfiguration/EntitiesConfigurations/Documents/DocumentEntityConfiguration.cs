﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    /// <summary>
    /// document entities mapping
    /// </summary>
    internal class DocumentEntityConfiguration<TEntity>
        where TEntity : Document
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            // properties
            builder.Property(e => e.Reference)
                .HasMaxLength(50);

            builder.Property(e => e.Purpose)
                .HasMaxLength(500);

            builder.Property(e => e.Note)
                .HasMaxLength(500);

            builder.Property(e => e.Status)
                .HasMaxLength(50);

            builder.Property(e => e.PaymentCondition)
                .HasMaxLength(1000);

            builder.Ignore(e => e.DocumentType);

            builder.HasQueryFilter(e => !e.IsDeleted);

            // conversions
            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            // indexes
            builder.HasIndex(e => e.Status);

            builder.HasIndex(e => e.Reference);
        }
    }
}
