﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Axiobat.Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class TurnoverGoalEntityCofiguration : IEntityTypeConfiguration<TurnoverGoal>
    {
        public void Configure(EntityTypeBuilder<TurnoverGoal> builder)
        {
            builder.ToTable("TurnoverGoals");

            builder.Property(e => e.MonthlyGoals)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<TurnoverMonthlyGoal>>())
                 .HasColumnType("LONGTEXT");
        }
    }
}
