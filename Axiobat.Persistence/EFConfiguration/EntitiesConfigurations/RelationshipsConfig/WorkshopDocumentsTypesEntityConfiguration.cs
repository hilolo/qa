﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class WorkshopDocumentsTypesEntityConfiguration : IEntityTypeConfiguration<WorkshopDocumentsTypes>
    {
        public void Configure(EntityTypeBuilder<WorkshopDocumentsTypes> builder)
        {
            // table name
            builder.ToTable("WorkshopDocuments_Types");

            // key
            builder.HasKey(e => new { e.TypeId, e.DocumentId });

            // relationships
            builder.HasOne(e => e.Type)
                .WithMany(p => p.Documents)
                .HasForeignKey(e => e.TypeId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Document)
                .WithMany(p => p.Types)
                .HasForeignKey(e => e.DocumentId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
