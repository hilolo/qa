﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class WorkshopDocumentTypeEntityConfiguration : IEntityTypeConfiguration<WorkshopDocumentType>
    {
        public void Configure(EntityTypeBuilder<WorkshopDocumentType> builder)
        {
            // table name
            builder.ToTable("WorkshopDocumentTypes");
        }
    }
}
