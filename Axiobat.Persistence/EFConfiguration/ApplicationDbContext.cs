﻿namespace Axiobat.Persistence.DataContext
{
    using App.Common;
    using Domain.Enums;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Newtonsoft.Json.Linq;
    using Axiobat.Domain.Constants;

    /// <summary>
    /// the Application DbContext, should implement <see cref="IDataSource"/>
    /// </summary>
    public partial class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<GlobalHistory> GlobalHistory { get; set; }

        // workshop management
        public DbSet<ConstructionWorkshop> Workshops { get; set; }
        public DbSet<WorkshopDocument> WorkshopDocuments { get; set; }
        public DbSet<WorkshopDocumentRubric> WorkshopDocumentsRubrics { get; set; }
        public DbSet<WorkshopDocumentsTypes> WorkshopDocuments_Types { get; set; }
        public DbSet<Mission> Missions { get; set; }

        // external partners
        public DbSet<Client> Clients { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }

        // products
        public DbSet<Lot> Lots { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductLabel> Products_Labels { get; set; }
        public DbSet<ProductSupplier> Products_Suppliers { get; set; }
        public DbSet<LotProduct> Lots_Products { get; set; }

        // groups
        public DbSet<Group> Groupes { get; set; }

        // configuration entities
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<ProductCategoryType> ProductCategoryTypes { get; set; }
        public DbSet<PaymentMethod> PaymentMethod { get; set; }
        public DbSet<AccountingPeriod> AccountingPeriods { get; set; }
        public DbSet<FinancialAccount> FinancialAccounts { get; set; }
        public DbSet<ChartAccountItem> ChartOfAccounts { get; set; }
        public DbSet<Classification> Classifications { get; set; }
        public DbSet<WorkshopDocumentType> WorkshopDocumentTypes { get; set; }
        public DbSet<TurnoverGoal> TurnoverGoal { get; set; }
        public DbSet<ApplicationConfiguration> ApplicationConfigurations { get; set; }
        public DbSet<TurnoverGoal> TurnoverGoals { get; set; }
        public DbSet<RecurringDocument> RecurringDocuments { get; set; }
        public DbSet<SupplierOrder> SupplierOrders { get; set; }

        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<CreditNote> CreditNotes { get; set; }
        public DbSet<OperationSheet> OperationSheets { get; set; }
        public DbSet<OperationSheets_Technicians> OperationSheets_Technicians { get; set; }
        public DbSet<Invoices_Payments> Invoices_Payments { get; set; }
        public DbSet<Expenses_Payments> Expenses_Payments { get; set; }

        // Maintenances
        public DbSet<EquipmentMaintenance> EquipmentMaintenances { get; set; }
        public DbSet<MaintenanceContract> MaintenanceContracts { get; set; }
        public DbSet<MaintenanceOperationSheet> MaintenanceOperationSheets { get; set; }
        public DbSet<MaintenanceVisit> MaintenanceVisits { get; set; }
        public DbSet<PublishingContract> PublishingContract { get; set; }
    }

    public partial class ApplicationDbContext
    {
        /// <summary>
        /// default constructor with option builder
        /// </summary>
        /// <param name="options">the options builder</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        /// <summary>
        /// used for configuring entities
        /// </summary>
        /// <param name="modelBuilder">the model builder</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
            => modelBuilder
                .ApplyStringDefaultSize()
                .ApplyBaseEntityConfiguration()
                .ApplyAllConfigurations();

        /// <summary>
        /// this method is used to detach all entities being tracked by the change tracker
        /// </summary>
        public void DetachAllEntities()
        {
            var changedEntriesCopy = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                        e.State == EntityState.Modified ||
                        e.State == EntityState.Unchanged ||
                        e.State == EntityState.Deleted)
                        .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }

        /// <summary>
        /// this method is used to seed data
        /// </summary>
        /// <returns></returns>
        public void SeedData(bool seedData = false, bool updateEquipmentIds = false)
        {
            if (updateEquipmentIds)
            {
                //FixDataIssue();
                //FixEquipmentDetailsIds();
                //FixSearchTermsAndInvoiceStatus();
                FixMissionStatus();
            }

            if (seedData)
            {
                //Generate("ProductCategoryType.json", ProductCategoryTypes.ToList());

                // 1 truncate table data
                var trancateSQL = $@"SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE {TablesNames.ApplicationConfigurations};
TRUNCATE TABLE {TablesNames.ChartAccountCategories};
TRUNCATE TABLE {TablesNames.WorkshopDocumentTypes};
TRUNCATE TABLE {TablesNames.PaymentMethods};
TRUNCATE TABLE {TablesNames.Roles};
TRUNCATE TABLE {TablesNames.FinancialAccount};
TRUNCATE TABLE {TablesNames.Units};
TRUNCATE TABLE {TablesNames.TurnoverGoals};
TRUNCATE TABLE {TablesNames.Classification};
TRUNCATE TABLE {TablesNames.ProductCategoryType};
TRUNCATE TABLE {TablesNames.Countries};";

                Database.ExecuteSqlCommand(trancateSQL);

                ApplicationConfigurations.AddRange(ReadFromFile<ApplicationConfiguration>("ApplicationConfiguration.json"));
                WorkshopDocumentTypes.AddRange(ReadFromFile<WorkshopDocumentType>("WorkshopDocumentTypes.json"));
                ChartOfAccounts.AddRange(ReadFromFile<ChartAccountItem>("ChartOfAccounts.json"));
                Classifications.AddRange(ReadFromFile<Classification>("Classification.json"));
                PaymentMethod.AddRange(ReadFromFile<PaymentMethod>("PaymentMethod.json"));
                Countries.AddRange(ReadFromFile<Country>("Countries.json"));
                Units.AddRange(ReadFromFile<Unit>("Unites.json"));
                Roles.AddRange(ReadFromFile<Role>("Roles.json"));
                TurnoverGoals.AddRange(ReadFromFile<TurnoverGoal>("TurnoverGoals.json"));
                FinancialAccounts.AddRange(ReadFromFile<FinancialAccount>("FinancialAccount.json"));
                ProductCategoryTypes.AddRange(ReadFromFile<ProductCategoryType>("ProductCategoryType.json"));
            }

            SetRequiredData();
            SaveChanges();
            Database.ExecuteSqlCommand("SET FOREIGN_KEY_CHECKS=1");
        }

        private void FixMissionStatus()
        {
            var listMission = Missions.Where(m => m.Type == null || m.Type == "").ToList();

            if (listMission.Any())
            {
                foreach (var item in listMission)
                {
                    item.Type = InvoiceStatus.InProgress;
                }

                SaveChanges();
            }
        }

        private void FixSearchTermsAndInvoiceStatus()
        {
            var ListInvoices = Invoices
                .Include(e => e.Payments)
                .Where(e => e.Status == InvoiceStatus.Closed)
                .ToList();

            foreach (var item in ListInvoices)
            {
                if (!item.Payments.Any())
                {
                    item.Status = InvoiceStatus.InProgress;
                }
            }

            var chanties = Workshops
                .Include(e => e.Rubrics)
                .ToList();

            var rebricToAdd = new LinkedList<WorkshopDocumentRubric>();

            foreach (var item in chanties.Where(e => !e.Rubrics.Any()))
            {
                rebricToAdd.AddLast(new WorkshopDocumentRubric
                {
                    Value = "Commandes",
                    SearchTerms = "Commandes",
                    Id = Guid.NewGuid().ToString(),
                    WorkshopId = item.Id,
                    Type = DocumentRubricType.Orders,
                });

                rebricToAdd.AddLast(new WorkshopDocumentRubric
                {
                    Value = "Devis",
                    SearchTerms = "Devis",
                    Id = Guid.NewGuid().ToString(),
                    WorkshopId = item.Id,
                    Type = DocumentRubricType.Quote,
                });

                rebricToAdd.AddLast(new WorkshopDocumentRubric
                {
                    Value = "Facturation",
                    SearchTerms = "Facturation",
                    Id = Guid.NewGuid().ToString(),
                    WorkshopId = item.Id,
                    Type = DocumentRubricType.Billing,
                });
            }

            var maintenceOperations = MaintenanceOperationSheets.ToList();
            foreach (var item in maintenceOperations)
            {
                item.BuildSearchTerms();
            }

            Invoices.UpdateRange(ListInvoices);
            WorkshopDocumentsRubrics.AddRange(rebricToAdd);
            MaintenanceOperationSheets.UpdateRange(maintenceOperations);
            SaveChanges();
        }

        private IEnumerable<IEntity> ReadFromFile<IEntity>(string fileName)
        {
            var filePath = Path.Combine(FileHelper.SeedDataFolder, fileName);
            return FileHelper.ReadFileText(filePath).FromJson<IEnumerable<IEntity>>();
        }

        private void Generate<TEntity>(string fileName, List<TEntity> lists)
        {
            var filePath = Path.Combine(FileHelper.SeedDataFolder, fileName);
            File.WriteAllText(filePath, lists.ToJson());
        }

        private void FixDataIssue()
        {
            var quotes = Quotes.ToList();
            var invoices = Invoices.ToList();
            var workshops = Workshops.ToList();
            var creditNotes = CreditNotes.ToList();
            var contract = MaintenanceContracts.ToList();
            var operationSheets = OperationSheets.ToList();
            var maintenanceVisits = MaintenanceVisits.ToList();
            var maintenanceContracts = MaintenanceContracts.ToList();
            var equipmentMaintenances = EquipmentMaintenances.ToList();
            var maintenanceOperationSheets = MaintenanceOperationSheets.ToList();
            var recurringDocuments = RecurringDocuments
                .ToList()
                .Where(recurring => recurring.DocumentType == DocumentType.Invoice)
                .Where(recurring => !(recurring.Document is null));

            var equipmentMaintenancesItems = equipmentMaintenances.ToDictionary(e => e.Id);
            var recurringDocumentsClientsIds = recurringDocuments.Select(recurring =>
            {
                if (!(recurring.Document is null))
                {
                    return recurring.Document.ClientId();
                }

                return null;
            })
            .Where(clientId => clientId.IsValid());

            var clientIds =
                recurringDocumentsClientsIds
               .Concat(quotes.Select(e => e.ClientId))
               .Concat(invoices.Select(e => e.ClientId))
               .Concat(workshops.Select(e => e.ClientId))
               .Concat(creditNotes.Select(e => e.ClientId))
               .Concat(operationSheets.Select(e => e.ClientId))
               .Concat(maintenanceVisits.Select(e => e.ClientId))
               .Concat(maintenanceContracts.Select(e => e.ClientId))
               .Concat(maintenanceOperationSheets.Select(e => e.ClientId))
               .Distinct()
               .ToArray();

            var clients = Clients.Where(e => clientIds.Contains(e.Id))
                .IgnoreQueryFilters()
                .ToDictionary(e => e.Id);

            foreach (var entity in quotes)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;
            }
            foreach (var entity in contract)
            {
                foreach (var item in entity.EquipmentMaintenance)
                {
                    foreach (var operation in item.MaintenanceOperations)
                    {
                        if (equipmentMaintenancesItems.TryGetValue(operation.Id, out EquipmentMaintenance equipment))
                        {
                            operation.SubOperations = operation.SubOperations;
                        }
                        else
                        {
                            foreach (var subOperation in operation.SubOperations)
                            {
                                if (!subOperation.Id.IsValid())
                                    subOperation.Id = Guid.NewGuid().ToString();
                            }
                        }
                    }
                }
            }
            foreach (var entity in invoices)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;
            }
            foreach (var entity in workshops)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;
            }
            foreach (var entity in creditNotes)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;
            }
            foreach (var entity in operationSheets)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;
            }
            foreach (var entity in maintenanceVisits)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;
            }
            foreach (var entity in recurringDocuments)
            {
                var clientId = entity.Document.ClientId();
                if (clientId.IsValid() && clients.TryGetValue(clientId, out Client client))
                {
                    entity.Document["client"] = JObject.FromObject(client);
                }
            }
            foreach (var entity in maintenanceContracts)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;
            }
            foreach (var entity in equipmentMaintenances)
            {
                foreach (var operation in entity.MaintenanceOperations)
                {
                    if (!operation.Id.IsValid())
                        operation.Id = Guid.NewGuid().ToString();

                    foreach (var subOperation in operation.SubOperations)
                    {
                        if (!subOperation.Id.IsValid())
                            subOperation.Id = Guid.NewGuid().ToString();
                    }
                }
            }
            foreach (var entity in maintenanceOperationSheets)
            {
                if (entity.ClientId.IsValid() && clients.TryGetValue(entity.ClientId, out Client client))
                    entity.Client = client;

                foreach (var item in entity.EquipmentDetails)
                {
                    foreach (var operation in item.MaintenanceOperations)
                    {
                        if (equipmentMaintenancesItems.TryGetValue(operation.Id, out EquipmentMaintenance equipment))
                        {
                            operation.SubOperations = operation.SubOperations;
                        }
                        else
                        {
                            foreach (var subOperation in operation.SubOperations)
                            {
                                if (!subOperation.Id.IsValid())
                                    subOperation.Id = Guid.NewGuid().ToString();
                            }
                        }
                    }
                }
            }

            var expenses = Expenses.ToList();
            var supplierOrders = SupplierOrders.ToList();

            var suppliersIds = expenses.Select(e => e.SupplierId)
                .Concat(supplierOrders.Select(e => e.SupplierId))
                .Distinct()
                .ToArray();

            var suppliers = Suppliers
                .IgnoreQueryFilters()
                .Where(e => suppliersIds.Contains(e.Id))
                .ToDictionary(e => e.Id);

            foreach (var entity in expenses)
            {
                if (entity.SupplierId.IsValid() && suppliers.TryGetValue(entity.SupplierId, out Supplier supplier))
                    entity.Supplier = supplier;
            }
            foreach (var entity in supplierOrders)
            {
                if (entity.SupplierId.IsValid() && suppliers.TryGetValue(entity.SupplierId, out Supplier supplier))
                    entity.Supplier = supplier;
            }

            Quotes.UpdateRange(quotes);
            Invoices.UpdateRange(invoices);
            Workshops.UpdateRange(workshops);
            CreditNotes.UpdateRange(creditNotes);
            MaintenanceContracts.UpdateRange(contract);
            OperationSheets.UpdateRange(operationSheets);
            MaintenanceVisits.UpdateRange(maintenanceVisits);
            RecurringDocuments.UpdateRange(recurringDocuments);
            EquipmentMaintenances.UpdateRange(equipmentMaintenances);
            MaintenanceOperationSheets.UpdateRange(maintenanceOperationSheets);

            Expenses.UpdateRange(expenses);
            SupplierOrders.UpdateRange(supplierOrders);

            SaveChanges();
        }

        private void FixEquipmentDetailsIds()
        {
            var operationSheets = MaintenanceOperationSheets.ToList();

            foreach (var entity in operationSheets)
            {
                foreach (var equipenet in entity.EquipmentDetails)
                {
                    if (!equipenet.Id.IsValid())
                    {
                        equipenet.Id = Guid.NewGuid().ToString();
                    }

                    foreach (var operations in equipenet.MaintenanceOperations)
                    {
                        if (!operations.Id.IsValid())
                        {
                            operations.Id = Guid.NewGuid().ToString();
                        }

                        foreach (var subOperations in operations.SubOperations)
                        {
                            subOperations.Id = Guid.NewGuid().ToString();
                        }
                    }
                }
            }

            MaintenanceOperationSheets.UpdateRange(operationSheets);
            SaveChanges();
        }

        private void SetRequiredData()
        {
            // check default Roles
            var AdminRole = Roles.FirstOrDefault(r => r.Id == Role.SuperAdminRole);
            if (AdminRole is null)
            {
                Roles.Add(new Role
                {
                    Id = Role.SuperAdminRole,
                    Name = "SuperAdmin",
                    SearchTerms = "superAdmin",
                    NormalizedName = "SUPERADMIN",
                    CreatedOn = DateTime.Now,
                    LastModifiedOn = DateTime.Now,
                    Type = RoleType.SuperAdminRole,
                    ConcurrencyStamp = "81B5352D-666F-46D5-BFB2-66B09DCF56F9",
                    Permissions = new List<Permissions> { Permissions.WebAppAccess, Permissions.MobileAppAccess },
                });
            }

            // add default SuperAdmin User
            var superAdminUser = Users.FirstOrDefault(u => u.Id == User.SuperAdmin);
            if (superAdminUser is null)
            {
                Users.Add(new User
                {
                    Id = User.SuperAdmin,
                    FirstName = "AxioBat",
                    LastName = "Admin",
                    Email = "contact@axiobat.fr",
                    UserName = "contact@axiobat.fr",
                    NormalizedEmail = "CONTACT@AXIOBAT.FR",
                    NormalizedUserName = "CONTACT@AXIOBAT.FR",
                    EmailConfirmed = true,
                    LockoutEnabled = false,
                    Statut = Status.Active,
                    CreatedOn = DateTime.Now,
                    LastModifiedOn = DateTime.Now,
                    RoleId = Role.SuperAdminRole,
                    ConcurrencyStamp = "FE9CCA3E-3F6B-498A-B022-E98051FA2B6D",
                    PasswordHash = "AQAAAAEAACcQAAAAEI+smQZORlSXkRaZyn+ZP1ZxcWEkdwANU3DJUpl+A9+nxLGzDotwSFMupeudULpVQw==",
                    ChangesHistory = new List<ChangesHistory>
                    {
                        new ChangesHistory
                        {
                            Action = ChangesHistoryType.Added,
                            DateAction = DateTime.Now,
                            Fields = null,
                            User = new MinimalUser(User.SuperAdmin, "contact@couleur-avenir.com"),
                        }
                    }
                });
            }
        }
    }
}
