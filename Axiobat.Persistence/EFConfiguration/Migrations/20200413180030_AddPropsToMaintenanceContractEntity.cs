﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddPropsToMaintenanceContractEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ExpirationAlertEnabled",
                table: "MaintenanceContracts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ExpirationAlertPeriod",
                table: "MaintenanceContracts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ExpirationAlertPeriodType",
                table: "MaintenanceContracts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpirationAlertEnabled",
                table: "MaintenanceContracts");

            migrationBuilder.DropColumn(
                name: "ExpirationAlertPeriod",
                table: "MaintenanceContracts");

            migrationBuilder.DropColumn(
                name: "ExpirationAlertPeriodType",
                table: "MaintenanceContracts");
        }
    }
}
