﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddypeAndWorkshopIdToRubric : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "WorkshopDocumentsRubrics",
                nullable: false,
                defaultValue: 3);

            migrationBuilder.AddColumn<string>(
                name: "WorkshopId",
                table: "WorkshopDocumentsRubrics",
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopDocumentsRubrics_WorkshopId",
                table: "WorkshopDocumentsRubrics",
                column: "WorkshopId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkshopDocumentsRubrics_ConstructionWorkshops_WorkshopId",
                table: "WorkshopDocumentsRubrics",
                column: "WorkshopId",
                principalTable: "ConstructionWorkshops",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkshopDocumentsRubrics_ConstructionWorkshops_WorkshopId",
                table: "WorkshopDocumentsRubrics");

            migrationBuilder.DropIndex(
                name: "IX_WorkshopDocumentsRubrics_WorkshopId",
                table: "WorkshopDocumentsRubrics");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "WorkshopDocumentsRubrics");

            migrationBuilder.DropColumn(
                name: "WorkshopId",
                table: "WorkshopDocumentsRubrics");
        }
    }
}
