﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class RemoveExpirationAlertPeriodTypeFromMaintenanceContractEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpirationAlertPeriodType",
                table: "MaintenanceContracts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExpirationAlertPeriodType",
                table: "MaintenanceContracts",
                nullable: false,
                defaultValue: 0);
        }
    }
}
