﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddPhoneNumberToMission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "WorkshopDocumentsRubrics",
                nullable: false,
                defaultValue: 4,
                oldClrType: typeof(int),
                oldDefaultValue: 3);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Missions",
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Missions");

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "WorkshopDocumentsRubrics",
                nullable: false,
                defaultValue: 3,
                oldClrType: typeof(int),
                oldDefaultValue: 4);
        }
    }
}
