﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddDataToApplicationConfigiration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO ApplicationConfigurations (`ConfigurationType`, `Value`) VALUES ('mission_technician_task_types', '[]') ON DUPLICATE KEY UPDATE `Value` = '[]';");
            migrationBuilder.Sql("INSERT INTO ApplicationConfigurations (`ConfigurationType`, `Value`) VALUES ('mission_call_types', '[]') ON DUPLICATE KEY UPDATE `Value` = '[]';");
            migrationBuilder.Sql("INSERT INTO ApplicationConfigurations (`ConfigurationType`, `Value`) VALUES ('mission_appointment_types', '[]') ON DUPLICATE KEY UPDATE `Value` = '[]';");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
