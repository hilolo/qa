﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class ChangeTechnicianTaskToMission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TechnicianTasks");

            migrationBuilder.CreateTable(
                name: "Missions",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    MissionKind = table.Column<string>(maxLength: 256, nullable: true, defaultValue: "technician_task"),
                    Type = table.Column<string>(maxLength: 256, nullable: true),
                    Title = table.Column<string>(type: "LONGTEXT", maxLength: 256, nullable: true),
                    Object = table.Column<string>(type: "LONGTEXT", maxLength: 256, nullable: true),
                    Status = table.Column<string>(maxLength: 256, nullable: true),
                    AllDayLong = table.Column<bool>(nullable: false),
                    StartingDate = table.Column<DateTime>(nullable: false),
                    EndingDate = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<string>(maxLength: 256, nullable: false),
                    TechnicianId = table.Column<Guid>(nullable: false),
                    AdditionalInfo = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Missions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Missions_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Missions_Users_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Missions_ClientId",
                table: "Missions",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Missions_SearchTerms",
                table: "Missions",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Missions_TechnicianId",
                table: "Missions",
                column: "TechnicianId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Missions");

            migrationBuilder.CreateTable(
                name: "TechnicianTasks",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    AllDayLong = table.Column<bool>(nullable: false),
                    ClientId = table.Column<string>(maxLength: 256, nullable: false),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    EndingDate = table.Column<DateTime>(nullable: true),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    Object = table.Column<string>(type: "LONGTEXT", maxLength: 256, nullable: true),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    StartingDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(maxLength: 256, nullable: true),
                    TechnicianId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(type: "LONGTEXT", maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TechnicianTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TechnicianTasks_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TechnicianTasks_Users_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TechnicianTasks_ClientId",
                table: "TechnicianTasks",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_TechnicianTasks_SearchTerms",
                table: "TechnicianTasks",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_TechnicianTasks_TechnicianId",
                table: "TechnicianTasks",
                column: "TechnicianId");
        }
    }
}
