﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddTurnoverTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TurnoverGoals",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Goal = table.Column<float>(nullable: false),
                    MonthlyGoals = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TurnoverGoals", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TurnoverGoals_SearchTerms",
                table: "TurnoverGoals",
                column: "SearchTerms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TurnoverGoals");
        }
    }
}
