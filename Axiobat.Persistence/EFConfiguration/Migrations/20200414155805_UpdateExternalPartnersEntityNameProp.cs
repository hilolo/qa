﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class UpdateExternalPartnersEntityNameProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Suppliers",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "Prenom",
                table: "Clients",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Clients",
                newName: "FirstName");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Suppliers",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EnableAutoRenewal",
                table: "MaintenanceContracts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Suppliers");

            migrationBuilder.DropColumn(
                name: "EnableAutoRenewal",
                table: "MaintenanceContracts");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Suppliers",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Clients",
                newName: "Prenom");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Clients",
                newName: "Name");
        }
    }
}
