﻿namespace Axiobat.Presentation.BackgroundServices
{
    using Application.Models;
    using Application.Services;
    using Application.Services.Configuration;
    using Domain.Entities;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// this class is used to generate turnovers automatically, it runs once at the end of the user
    /// </summary>
    public partial class TurnoverGoalGeneratorBackgroundService
    {
        protected override async Task RunAsync(IServiceScope scope)
        {
            var dateService = scope.GetService<IDateService>();
            var configurationService = scope.GetService<IApplicationConfigurationService>();

            var yearStart = new DateTime(DateTime.Today.Year + 1, 1, 1);
            var yearEnd = new DateTime(DateTime.Today.Year + 1, 12, 31);

            await configurationService.PutAsync<TurnoverGoalModel, TurnoverGoal, int>(
                new TurnoverGoalModel
                {
                    Goal = 0,
                    Id = yearStart.Year,
                    MonthlyGoals = dateService.GetDates(yearStart, yearEnd)
                        .Select(date => new TurnoverMonthlyGoal
                        {
                            Goal = 0,
                            Month = date.Month,
                        })
                        .ToArray()
                });
        }
    }

    public partial class TurnoverGoalGeneratorBackgroundService : BaseBackgroundService
    {
        public TurnoverGoalGeneratorBackgroundService(IServiceProvider services)
            : base(services) { }
    }
}
