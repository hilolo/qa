﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using Application.Services;
    using Domain.Entities;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    public class ExpenseModelValidation : BaseDocumentModelValidation<ExpensePutModel, Expense>
    {
        public ExpenseModelValidation(ILoggerFactory loggerFactory,
            IConstructionWorkshopService workshopService)
            : base(loggerFactory, workshopService)
        {
            RuleFor(e => e.Reference)
                .NotEmpty()
                    .WithMessage("reference is required")
                    .WithErrorCode(MessageCode.ReferenceIsRequired)
                .NotNull()
                    .WithMessage("reference is required")
                    .WithErrorCode(MessageCode.ReferenceIsRequired);

            RuleFor(e => e.OrderDetails)
                .SetValidator(new OrderDetailsModelValidation(loggerFactory));
        }
    }
}
