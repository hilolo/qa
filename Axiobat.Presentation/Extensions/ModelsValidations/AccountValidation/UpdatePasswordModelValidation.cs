﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using Axiobat.Application.Services.AccountManagement;
    using FluentValidation;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// the user validation model
    /// </summary>
    public class UpdatePasswordModelValidation : BaseValidator<UserUpdatePasswordModel>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public UpdatePasswordModelValidation(ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            RuleFor(e => e.NewPassword)
                .NotNull()
                    .WithMessage("you must supply a value for the new password")
                    .WithErrorCode(MessageCode.PasswordIsRequired)
                .NotEmpty()
                    .WithMessage("you must supply a value for the new password")
                    .WithErrorCode(MessageCode.PasswordIsRequired);

            RuleFor(e => e.OldPassword)
                .NotNull()
                    .WithMessage("you must supply a value for the old password")
                    .WithErrorCode(MessageCode.PasswordIsRequired)
                .NotEmpty()
                    .WithMessage("you must supply a value for the old password")
                    .WithErrorCode(MessageCode.PasswordIsRequired);
        }
    }

    /// <summary>
    /// the user validation model
    /// </summary>
    public class UpdateUserPasswordLModelValidation : BaseValidator<UpdateUserPasswordModel>
    {
        private readonly IUsersManagementService _service;

        /// <summary>
        /// default constructor
        /// </summary>
        public UpdateUserPasswordLModelValidation(ILoggerFactory loggerFactory, IUsersManagementService service)
            : base(loggerFactory)
        {
            _service = service;

            RuleFor(e => e.UserId)
                .MustAsync(UserIdExistAsync)
                    .WithMessage("there is no user with the given id")
                    .WithErrorCode(MessageCode.UserNotExist);

            RuleFor(e => e.Password)
                .NotNull()
                    .WithMessage("you must supply a value for the old password")
                    .WithErrorCode(MessageCode.PasswordIsRequired)
                .NotEmpty()
                    .WithMessage("you must supply a value for the old password")
                    .WithErrorCode(MessageCode.PasswordIsRequired);
        }

        private Task<bool> UserIdExistAsync(Guid userId, CancellationToken cancellationToken)
            => _service.IsExistAsync(userId);
    }
}
