﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// health check API controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public partial class CheckController : ControllerBase
    {
        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <returns>the configuration</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult Get()
            => Ok(new { status = "OK" });
    }
}