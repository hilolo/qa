﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Documents;
    using Application.Services.Localization;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// Configuration management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class ConfigurationController : BaseController<ApplicationConfiguration>
    {
        //[HttpGet("test")]
        //[ProducesResponseType(200)]
        //[ProducesResponseType(404)]
        //[ProducesResponseType(500)]
        //public async Task<ActionResult> Get([FromServices] RecurringDocumentBackgroundService service)
        //{
        //    await service.ExecuteAsync();
        //    return Ok(":)");
        //}

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("{type}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> Get([FromRoute(Name = "type")] string configurationType)
            => Ok(await _service.GetAsync(configurationType));

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <param name="model">the model used to update the configuration</param>
        /// <returns>the configuration</returns>
        [HttpPut("{type}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> UpdateConfiguration(
            [FromRoute(Name = "type")] string configurationType,
            [FromBody] ConfigurationPutModel model, [FromServices] IOperationSheetService operationSheetService)
        {
            await _service.UpdateAsync(configurationType, model.Value);

            if (configurationType == ApplicationConfigurationType.GoogleConfiguration)
                await operationSheetService.updateForCalendarIdAsync();

            return Ok();
        }

        /// <summary>
        /// generate a reference for the given document type
        /// </summary>
        /// <param name="documentType">the type of the document to generate reference for it</param>
        /// <returns>reference</returns>
        [HttpGet("{docType}/reference")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<string>> GenerateReference(
            [FromRoute(Name = "docType")] DocumentType documentType)
                => Ok(await _service.GenerateRefereceAsync(documentType));

        /// <summary>
        /// generate a reference for the given document type
        /// </summary>
        /// <param name="documentType">the type of the document to generate reference for it</param>
        /// <returns>reference</returns>
        [HttpGet("{docType}/reference/Increment")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> IncrementReference(
            [FromRoute(Name = "docType")] DocumentType documentType)
        {
            await _service.IncrementReferenceAsync(documentType);
            return Ok();
        }
    }

    /// <summary>
    /// partial part for <see cref="ConfigurationController"/>
    /// </summary>
    public partial class ConfigurationController
    {
        #region Countries

        /// <summary>
        /// retrieve list of countries
        /// </summary>
        /// <returns>list of countries</returns>
        [HttpGet("countries")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<Country>>> Getcountries()
        {
            return ActionResultForAsync(_service.GetAllCountriesAsync());
        }

        /// <summary>
        /// retrieve country with the give code
        /// </summary>
        /// <returns>the country with the given code</returns>
        [HttpGet("countries/{code}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<Country>>> Getcountries([FromRoute(Name = "code")] string countryCode)
        {
            return ActionResultForAsync(_service.GetCountryByCodeAsync(countryCode));
        }

        #endregion

        #region Tax configuration

        /// <summary>
        /// if the given tax exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the tax model</param>
        /// <returns>the new tax version</returns>
        [HttpPut("Tax")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<TaxModel>>> PutTax([FromBody] TaxModel model)
        {
            return ActionResultFor(await _service.PutAsync<TaxModel, Tax, string>(model));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <param name="filter">the filter options used to return and paginate data</param>
        /// <returns>the configuration</returns>
        [HttpPost("Tax")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<TaxModel>>> GetTaxes([FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<Tax, TaxModel>(filter));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Tax/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<TaxModel>>> GetTaxById([FromRoute(Name = "id")] string taxId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<string, Tax, TaxModel>(taxId));
        }

        #endregion

        #region Unit configuration

        /// <summary>
        /// if the given Unit exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the Unit model</param>
        /// <returns>the new Unit version</returns>
        [HttpPut("Unit")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<UnitModel>>> PutUnit([FromBody] UnitModel model)
        {
            return ActionResultFor(await _service.PutAsync<UnitModel, Unit, string>(model));
        }

        /// <summary>
        /// retrieve the list of unites paginated
        /// </summary>
        /// <param name="filter">the filter options used to return and paginate data</param>
        /// <returns>the list of unites paginated</returns>
        [HttpPost("Unit")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<UnitModel>>> GetUnits([FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<Unit, UnitModel>(filter));
        }

        /// <summary>
        /// retrieve the list of all unites
        /// </summary>
        /// <returns>the configuration</returns>
        [HttpGet("Unit")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<UnitModel>>> GetUnits()
        {
            return ActionResultForAsync(_service.GetAllAsync<Unit, UnitModel>());
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Unit/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<UnitModel>>> GetUnitById([FromRoute(Name = "id")] string UnitId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<string, Unit, UnitModel>(UnitId));
        }

        #endregion

        #region Category configuration

        /// <summary>
        /// if the given Category exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the Category model</param>
        /// <returns>the new Category version</returns>
        [HttpPut("Category")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ChartAccountItemModel>>> PutCategory(
            [FromServices] IChartOfAccountsService service,
            [FromBody] ChartAccountItemPutModel[] model)
        {
            await service.PutAsync(model);
            return Ok(Result.Success());
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <param name="filter">the filter options used to return and paginate data</param>
        /// <returns>the configuration</returns>
        [HttpGet("Category")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ListResult<ChartAccountItemModel>>> GetCategories(
            [FromServices] IChartOfAccountsService service)
        {
            return await service.GetAllAsync<ChartAccountItemModel>();
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Category/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ChartAccountItemModel>>> GetCategoryById(
            [FromServices] IChartOfAccountsService service,
            [FromRoute(Name = "id")] string CategoryId)
        {
            return await service.GetByIdAsync<ChartAccountItemModel>(CategoryId);
        }

        /// <summary>
        /// retrieve list of categories with the given type
        /// </summary>
        /// <param name="categoryType">the type of the category to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Category/type/{type}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ListResult<ChartAccountItemModel>>> GetCategoryById(
            [FromServices] IChartOfAccountsService service,
            [FromRoute(Name = "type")] ChartAccountType categoryType)
        {
            var categories = await service.GetCategoriesByTypeAsync<ChartAccountItemModel>(categoryType);
            return Result.ListSuccess(categories);
        }

        /// <summary>
        /// delete the Category with the given id
        /// </summary>
        /// <param name="categoryId">the id of the Workshop Document Type to delete</param>
        /// <returns>the operation result</returns>
        [HttpDelete("Category/{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> DeleteCategoryById(
            [FromServices] IChartOfAccountsService service,
            [FromRoute(Name = "id")] string categoryId)
        {
            return await service.DeleteAsync(categoryId);
        }

        #endregion

        #region Classification configuration

        /// <summary>
        /// get list of all classification
        /// </summary>
        /// <param name="service">the classification service</param>
        /// <returns>list of classification</returns>
        [HttpGet("Classification")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ListResult<ClassificationModel>>> GetCategories(
            [FromServices] IClassificationService service)
        {
            return await service.GetAllAsync<ClassificationModel>();
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Classification/{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ClassificationModel>>> GetClassificationById(
            [FromServices] IClassificationService service,
            [FromRoute(Name = "id")] int ClassificationId)
        {
            return await service.GetByIdAsync<ClassificationModel>(ClassificationId);
        }

        /// <summary>
        /// retrieve list of categories with the given type
        /// </summary>
        /// <param name="ClassificationType">the type of the Classification to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Classification/type/{type}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ListResult<ClassificationModel>>> GetClassificationById(
            [FromServices] IClassificationService service,
            [FromRoute(Name = "type")] ClassificationType ClassificationType)
        {
            var categories = await service.GetCategoriesByTypeAsync<ClassificationModel>(ClassificationType);
            return Result.ListSuccess(categories);
        }

        /// <summary>
        /// if the given Classification exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the Classification model</param>
        /// <returns>the new Classification version</returns>
        [HttpPut("Classification")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> PutClassification(
            [FromServices] IClassificationService service,
            [FromBody] ClassificationPutModel[] model)
        {
            await service.PutAsync(model);
            return Result.Success();
        }

        /// <summary>
        /// delete the Classification with the given id
        /// </summary>
        /// <param name="ClassificationId">the id of the Workshop Document Type to delete</param>
        /// <returns>the operation result</returns>
        [HttpDelete("Classification/{id:int}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> DeleteClassificationById(
            [FromServices] IClassificationService service,
            [FromRoute(Name = "id")] int ClassificationId)
        {
            await service.DeleteAsync(ClassificationId);
            return Result.Success();
        }

        #endregion

        #region Labels configurations

        /// <summary>
        /// if the given Label exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the Label model</param>
        /// <returns>the new Label version</returns>
        [HttpPut("Label")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<LabelModel>>> PutLabel(
            [FromBody] LabelModel model)
        {
            return ActionResultFor(await _service.PutAsync<LabelModel, Label, string>(model));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpPost("Label")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<LabelModel>>> GetLabeles(
            [FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<Label, LabelModel>(filter));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Label")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<LabelModel>>> GetLabeles()
        {
            return ActionResultForAsync(_service.GetAllAsync<Label, LabelModel>());
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved</param>
        /// <returns>the configuration</returns>
        [HttpGet("Label/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<LabelModel>>> GetLabelById(
            [FromRoute(Name = "id")] string LabelId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<string, Label, LabelModel>(LabelId));
        }

        /// <summary>
        /// delete the label with the given id
        /// </summary>
        /// <param name="LabelId">the id of the label to delete</param>
        /// <returns>the operation result</returns>
        [HttpDelete("Label/{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteLabelById(
            [FromRoute(Name = "id")] string LabelId)
        {
            return ActionResultForAsync(_service.DeleteAsync<Label, string>(LabelId));
        }

        #endregion

        #region PaymentMethods configurations

        /// <summary>
        /// if the given Payment Method exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the PaymentMethod model</param>
        /// <returns>the new PaymentMethod version</returns>
        [HttpPut("PaymentMethod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<PaymentMethodModel>>> PutPaymentMethod(
            [FromBody] PaymentMethodModel model)
        {
            return ActionResultFor(await _service.PutAsync<PaymentMethodModel, PaymentMethod, int>(model));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="filter">the filter model used to retrieve the result</param>
        /// <returns>the list of Payment Methods paginated</returns>
        [HttpPost("PaymentMethod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<PaymentMethodModel>>> GetPaymentMethodes(
            [FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<PaymentMethod, PaymentMethodModel>(filter));
        }

        /// <summary>
        /// retrieve the list of all Payment Methods
        /// </summary>
        /// <returns>the list of all  Payment Methods</returns>
        [HttpGet("PaymentMethod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<PaymentMethodModel>>> GetPaymentMethodes()
        {
            return ActionResultForAsync(_service.GetAllAsync<PaymentMethod, PaymentMethodModel>());
        }

        /// <summary>
        /// retrieve the Payment Method with the given id
        /// </summary>
        /// <param name="PaymentMethodId">the id of the Payment Method</param>
        /// <returns>the Payment Method with the given id</returns>
        [HttpGet("PaymentMethod/{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<PaymentMethodModel>>> GetPaymentMethodById(
            [FromRoute(Name = "id")] int PaymentMethodId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<int, PaymentMethod, PaymentMethodModel>(PaymentMethodId));
        }

        /// <summary>
        /// delete the Payment Method with the given id
        /// </summary>
        /// <param name="PaymentMethodId">the id of the Payment Method to delete</param>
        /// <returns>the operation result</returns>
        [HttpDelete("PaymentMethod/{id:int}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeletePaymentMethodById(
            [FromRoute(Name = "id")] int PaymentMethodId)
        {
            return ActionResultForAsync(_service.DeleteAsync<PaymentMethod, int>(PaymentMethodId));
        }

        #endregion

        #region Financial Accounts configurations

        /// <summary>
        /// if the given Financial Account exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the FinancialAccount model</param>
        /// <returns>the new FinancialAccount version</returns>
        [HttpPut("FinancialAccount")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<FinancialAccountModel>>> PutFinancialAccount(
            [FromBody] FinancialAccountModel model)
        {
            return ActionResultFor(await _service.PutAsync<FinancialAccountModel, FinancialAccount, int>(model));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="filter">the filter model used to retrieve the result</param>
        /// <returns>the list of Financial Accounts paginated</returns>
        [HttpPost("FinancialAccount")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<FinancialAccountModel>>> GetFinancialAccountes(
            [FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<FinancialAccount, FinancialAccountModel>(filter));
        }

        /// <summary>
        /// retrieve the list of all Financial Accounts
        /// </summary>
        /// <returns>the list of all  Financial Accounts</returns>
        [HttpGet("FinancialAccount")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<FinancialAccountModel>>> GetFinancialAccountes()
        {
            return ActionResultForAsync(_service.GetAllAsync<FinancialAccount, FinancialAccountModel>());
        }

        /// <summary>
        /// retrieve the Financial Account with the given id
        /// </summary>
        /// <param name="FinancialAccountId">the id of the Financial Account</param>
        /// <returns>the Financial Account with the given id</returns>
        [HttpGet("FinancialAccount/{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<FinancialAccountModel>>> GetFinancialAccountById(
            [FromRoute(Name = "id")] int FinancialAccountId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<int, FinancialAccount, FinancialAccountModel>(FinancialAccountId));
        }

        /// <summary>
        /// delete the Financial Account with the given id
        /// </summary>
        /// <param name="FinancialAccountId">the id of the Financial Account to delete</param>
        /// <returns>the operation result</returns>
        [HttpDelete("FinancialAccount/{id:int}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteFinancialAccountById(
            [FromRoute(Name = "id")] int FinancialAccountId)
        {
            return ActionResultForAsync(_service.DeleteAsync<FinancialAccount, int>(FinancialAccountId));
        }

        #endregion

        #region Workshop Document Types configurations

        /// <summary>
        /// if the given Workshop Document Type exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the WorkshopDocumentType model</param>
        /// <returns>the new WorkshopDocumentType version</returns>
        [HttpPut("WorkshopDocumentType")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<WorkshopDocumentTypeModel>>> PutWorkshopDocumentType(
            [FromBody] WorkshopDocumentTypeModel model)
        {
            return ActionResultFor(await _service.PutAsync<WorkshopDocumentTypeModel, WorkshopDocumentType, string>(model));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="filter">the filter model used to retrieve the result</param>
        /// <returns>the list of Workshop Document Types paginated</returns>
        [HttpPost("WorkshopDocumentType")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<WorkshopDocumentTypeModel>>> GetWorkshopDocumentTypees(
            [FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<WorkshopDocumentType, WorkshopDocumentTypeModel>(filter));
        }

        /// <summary>
        /// retrieve the list of all Workshop Document Types
        /// </summary>
        /// <returns>the list of all  Workshop Document Types</returns>
        [HttpGet("WorkshopDocumentType")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<WorkshopDocumentTypeModel>>> GetWorkshopDocumentTypees()
        {
            return ActionResultForAsync(_service.GetAllAsync<WorkshopDocumentType, WorkshopDocumentTypeModel>());
        }

        /// <summary>
        /// retrieve the Workshop Document Type with the given id
        /// </summary>
        /// <param name="WorkshopDocumentTypeId">the id of the Workshop Document Type</param>
        /// <returns>the Workshop Document Type with the given id</returns>
        [HttpGet("WorkshopDocumentType/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopDocumentTypeModel>>> GetWorkshopDocumentTypeById(
            [FromRoute(Name = "id")] string WorkshopDocumentTypeId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<string, WorkshopDocumentType, WorkshopDocumentTypeModel>(WorkshopDocumentTypeId));
        }

        /// <summary>
        /// delete the Workshop Document Type with the given id
        /// </summary>
        /// <param name="WorkshopDocumentTypeId">the id of the Workshop Document Type to delete</param>
        /// <returns>the operation result</returns>
        [HttpDelete("WorkshopDocumentType/{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteWorkshopDocumentTypeById(
            [FromRoute(Name = "id")] string WorkshopDocumentTypeId)
        {
            return ActionResultForAsync(_service.DeleteAsync<WorkshopDocumentType, string>(WorkshopDocumentTypeId));
        }

        #endregion

        #region Accounting Period configurations

        /// <summary>
        /// Close the current accounting period
        /// </summary>
        /// <returns>the operation result</returns>
        [HttpGet("AccountingPeriod/Close/{id:int?}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> PutAccountingPeriod(
            [FromServices] IAccountingPeriodService accountingPeriodService,
            [FromRoute(Name = "id")] int? accountingPeriodId = null)
        {
            if (accountingPeriodId.HasValue)
                await accountingPeriodService.CloseAccountingPeriodAsync(accountingPeriodId.Value);
            else
                await accountingPeriodService.CloseCurrentAccountingPeriodAsync();

            return Result.Success();
        }

        /// <summary>
        /// if the given Accounting Period exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the AccountingPeriod model</param>
        /// <returns>the new AccountingPeriod version</returns>
        [HttpPut("AccountingPeriod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<AccountingPeriodModel>>> PutAccountingPeriod(
            [FromServices] IAccountingPeriodService accountingPeriodService,
            [FromBody] AccountingPeriodModel model)
        {
            if (model.Id > 0) // this is an update
                return ActionResultForAsync(accountingPeriodService.UpdateAsync<AccountingPeriodModel, AccountingPeriodModel>(model.Id, model));

            return ActionResultForAsync(accountingPeriodService.CreateAsync<AccountingPeriodModel, AccountingPeriodModel>(model));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="filter">the filter model used to retrieve the result</param>
        /// <returns>the list of Accounting Period paginated</returns>
        [HttpPost("AccountingPeriod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<AccountingPeriodModel>>> GetAccountingPeriodes(
            [FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<AccountingPeriod, AccountingPeriodModel>(filter));
        }

        /// <summary>
        /// retrieve the list of all Accounting Period
        /// </summary>
        /// <returns>the list of all  Accounting Period</returns>
        [HttpGet("AccountingPeriod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<AccountingPeriodModel>>> GetAccountingPeriodes()
        {
            return ActionResultForAsync(_service.GetAllAsync<AccountingPeriod, AccountingPeriodModel>());
        }

        /// <summary>
        /// retrieve the Accounting Period with the given id
        /// </summary>
        /// <param name="AccountingPeriodId">the id of the Financial Account</param>
        /// <returns>the Financial Account with the given id</returns>
        [HttpGet("AccountingPeriod/{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<AccountingPeriodModel>>> GetAccountingPeriodById(
            [FromRoute(Name = "id")] int AccountingPeriodId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<int, AccountingPeriod, AccountingPeriodModel>(AccountingPeriodId));
        }

        #endregion

        #region Workshop Document Rubric configurations

        /// <summary>
        /// if the given Workshop Document Rubric exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the WorkshopDocumentRubric model</param>
        /// <returns>the new WorkshopDocumentRubric version</returns>
        [HttpPut("WorkshopDocumentRubric")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<WorkshopDocumentRubricModel>>> PutWorkshopDocumentRubric([FromBody] WorkshopDocumentRubricModel model)
        {
            return ActionResultFor(await _service.PutAsync<WorkshopDocumentRubricModel, WorkshopDocumentRubric, string>(model));
        }

        /// <summary>
        /// retrieve the configuration with the given type
        /// </summary>
        /// <param name="filter">the filter model used to retrieve the result</param>
        /// <returns>the list of Workshop Document Rubric paginated</returns>
        [HttpPost("WorkshopDocumentRubric")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<WorkshopDocumentRubricModel>>> GetWorkshopDocumentRubrices(
            [FromBody] FilterOptions filter)
        {
            return ActionResultForAsync(_service.GetAllAsync<WorkshopDocumentRubric, WorkshopDocumentRubricModel>(filter));
        }

        /// <summary>
        /// retrieve the list of all Workshop Document Rubric
        /// </summary>
        /// <returns>the list of all  Workshop Document Rubric</returns>
        [HttpGet("WorkshopDocumentRubric")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<WorkshopDocumentRubricModel>>> GetWorkshopDocumentRubrices()
        {
            return ActionResultForAsync(_service.GetAllAsync<WorkshopDocumentRubric, WorkshopDocumentRubricModel>());
        }

        /// <summary>
        /// retrieve the Workshop Document Rubric with the given id
        /// </summary>
        /// <param name="WorkshopDocumentRubricId">the id of the Financial Account</param>
        /// <returns>the Financial Account with the given id</returns>
        [HttpGet("WorkshopDocumentRubric/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopDocumentRubricModel>>> GetWorkshopDocumentRubricById(
            [FromRoute(Name = "id")] string WorkshopDocumentRubricId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<string, WorkshopDocumentRubric, WorkshopDocumentRubricModel>(WorkshopDocumentRubricId));
        }

        #endregion

        #region Goals

        /// <summary>
        /// if the given Payment Method exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="model">the PaymentMethod model</param>
        /// <returns>the new PaymentMethod version</returns>
        [HttpPut("Goals")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<TurnoverGoalModel>>> PutGoals(
            [FromBody] TurnoverGoalModel model)
        {
            return ActionResultFor(await _service.PutAsync<TurnoverGoalModel, TurnoverGoal, int>(model));
        }


        /// <summary>
        /// retrieve the list of all Payment Methods
        /// </summary>
        /// <returns>the list of all  Payment Methods</returns>
        [HttpGet("Goals")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<TurnoverGoalModel>>> GetGoals()
        {
            return ActionResultForAsync(_service.GetAllAsync<TurnoverGoal, TurnoverGoalModel>());
        }

        /// <summary>
        /// retrieve the Payment Method with the given id
        /// </summary>
        /// <param name="PaymentMethodId">the id of the Payment Method</param>
        /// <returns>the Payment Method with the given id</returns>
        [HttpGet("Goals/{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<TurnoverGoalModel>>> GetGoalsById(
            [FromRoute(Name = "id")] int PaymentMethodId)
        {
            return ActionResultForAsync(_service.GetByIdAsync<int, TurnoverGoal, TurnoverGoalModel>(PaymentMethodId));
        }

        /// <summary>
        /// delete the Payment Method with the given id
        /// </summary>
        /// <param name="PaymentMethodId">the id of the Payment Method to delete</param>
        /// <returns>the operation result</returns>
        [HttpDelete("Goals/{id:int}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteGoalsById(
            [FromRoute(Name = "id")] int PaymentMethodId)
        {
            return ActionResultForAsync(_service.DeleteAsync<TurnoverGoal, int>(PaymentMethodId));
        }

        #endregion
    }

    /// <summary>
    /// partial part for <see cref="ConfigurationController"/>
    /// </summary>
    public partial class ConfigurationController
    {
        private readonly IApplicationConfigurationService _service;

        /// <summary>
        /// create an instant of <see cref="ConfigurationController"/>
        /// </summary>
        public ConfigurationController(
            IApplicationConfigurationService service,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = service;
        }

    }
}