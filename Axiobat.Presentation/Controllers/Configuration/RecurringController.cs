﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Configuration;
    using Domain.Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Application.Models;
    using System.Threading.Tasks;
    using Axiobat.Application.Enums;
    using Microsoft.AspNetCore.Http;
    using FluentValidation.AspNetCore;

    /// <summary>
    /// RecurringDocument management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class RecurringController : BaseController<RecurringDocument>
    {
        /// <summary>
        /// get list of all Recurrings
        /// </summary>
        /// <returns>list of all Recurrings</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<RecurringDocumentModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<RecurringDocumentModel>());

        /// <summary>
        /// get a paged result of the Recurrings list using the given <see cref="RecurringFilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Recurrings as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<RecurringDocumentModel>>> Get([FromBody] RecurringFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<RecurringDocumentModel, RecurringFilterOptions>(filter));

        /// <summary>
        /// retrieve Recurring with the given id
        /// </summary>
        /// <param name="recurringId">the id of the Recurring to be retrieved</param>
        /// <returns>the Recurring</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<RecurringDocumentModel>>> Get([FromRoute(Name = "id")] string recurringId)
            => ActionResultForAsync(_service.GetByIdAsync<RecurringDocumentModel>(recurringId));

        /// <summary>
        /// create a new Recurring record
        /// </summary>
        /// <param name="model">the model to create the Recurring from it</param>
        /// <returns>the newly created Recurring</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<RecurringDocumentModel>>> Create(
            [FromBody, CustomizeValidator(RuleSet = "default,validateStartDate")] RecurringDocumentPutModel model)
        {
            var result = await _service.CreateAsync<RecurringDocumentModel, RecurringDocumentPutModel>(model);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Recurring informations
        /// </summary>
        /// <param name="model">the model to use for updating the Recurring</param>
        /// <param name="recurringId">the id of the Recurring to be updated</param>
        /// <returns>the updated Recurring</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<RecurringDocumentModel>>> Update(
            [FromRoute(Name = "id")] string recurringId,
            [FromBody, CustomizeValidator(RuleSet = "default")] RecurringDocumentPutModel model)
            => ActionResultForAsync(_service.UpdateAsync<RecurringDocumentModel, RecurringDocumentPutModel>(recurringId, model));

        /// <summary>
        /// update the Recurring status
        /// </summary>
        /// <param name="model">the model to use for updating the Recurring status</param>
        /// <param name="recurringId">the id of the Recurring to be updated</param>
        /// <returns>the updated Recurring/returns>
        [HttpPut("{id}/Update/Status")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> UpdateStatus([FromBody] DocumentUpdateStatusModel model,
            [FromRoute(Name = "id")] string recurringId)
        {
            await _service.UpdateStatusAsync(recurringId, model);
            return Ok(Result.Success());
        }

        /// <summary>
        /// delete the Recurring with the given id
        /// </summary>
        /// <param name="recurringId">the id of the Recurring to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string recurringId)
            => ActionResultForAsync(_service.DeleteAsync(recurringId));
    }

    /// <summary>
    /// partial part for <see cref="RecurringController"/>
    /// </summary>
    public partial class RecurringController
    {
        private readonly IRecurringDocumentService _service;

        public RecurringController(
            IRecurringDocumentService service,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = service;
        }
    }
}