﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Maintenance;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// controller for managing <see cref="EquipmentMaintenance"/>
    /// </summary>
    [Route("api/[controller]")]
    public partial class EquipmentMaintenanceController : BaseController<EquipmentMaintenance>
    {
        /// <summary>
        /// get list of all Credit Notes
        /// </summary>
        /// <returns>list of all Credits</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<EquipmentMaintenanceModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<EquipmentMaintenanceModel>());

        /// <summary>
        /// get a paged result of the EquipmentMaintenances list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of EquipmentMaintenances as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<EquipmentMaintenanceListModel>>> Get([FromBody] FilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<EquipmentMaintenanceListModel, FilterOptions>(filter));

        /// <summary>
        /// retrieve EquipmentMaintenance with the given id
        /// </summary>
        /// <param name="EquipmentMaintenanceId">the id of the EquipmentMaintenance to be retrieved</param>
        /// <returns>the EquipmentMaintenance</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<EquipmentMaintenanceModel>>> Get([FromRoute(Name = "id")] string EquipmentMaintenanceId)
            => ActionResultForAsync(_service.GetByIdAsync<EquipmentMaintenanceModel>(EquipmentMaintenanceId));

        /// <summary>
        /// create a new EquipmentMaintenance record
        /// </summary>
        /// <param name="EquipmentMaintenanceModel">the model to create the EquipmentMaintenance from it</param>
        /// <returns>the newly created EquipmentMaintenance</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<EquipmentMaintenanceModel>>> Create(
            [FromBody] EquipmentMaintenanceModel EquipmentMaintenanceModel)
        {
            var result = await _service.CreateAsync<EquipmentMaintenanceModel, EquipmentMaintenanceModel>(EquipmentMaintenanceModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the EquipmentMaintenance informations
        /// </summary>
        /// <param name="EquipmentMaintenanceModel">the model to use for updating the EquipmentMaintenance</param>
        /// <param name="EquipmentMaintenanceId">the id of the EquipmentMaintenance to be updated</param>
        /// <returns>the updated EquipmentMaintenance</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<EquipmentMaintenanceModel>>> Update(
            [FromBody] EquipmentMaintenanceModel EquipmentMaintenanceModel, [FromRoute(Name = "id")] string EquipmentMaintenanceId)
            => ActionResultForAsync(_service.UpdateAsync<EquipmentMaintenanceModel, EquipmentMaintenanceModel>(EquipmentMaintenanceId, EquipmentMaintenanceModel));

        /// <summary>
        /// delete the Equipment Maintenance with the given id
        /// </summary>
        /// <param name="EquipmentMaintenanceId">the id of the EquipmentMaintenance to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string EquipmentMaintenanceId)
            => ActionResultForAsync(_service.DeleteAsync(EquipmentMaintenanceId));

        /// <summary>
        /// check if the given name is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="equipmentName">the name to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Name/{EquipmentName}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "EquipmentName")] string equipmentName)
            => await _service.IsNameUniqueAsync(equipmentName);
    }

    /// <summary>
    /// partial part for <see cref="EquipmentMaintenanceController"/>
    /// </summary>
    public partial class EquipmentMaintenanceController : BaseController<EquipmentMaintenance>
    {
        private readonly IEquipmentMaintenanceService _service;

        /// <summary>
        /// create an instant of <see cref="EquipmentMaintenanceController"/>
        /// </summary>
        /// <param name="loggedInUserService"></param>
        /// <param name="translationService"></param>
        /// <param name="loggerFactory"></param>
        public EquipmentMaintenanceController(
            IEquipmentMaintenanceService service,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = service;
        }
    }
}