﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the controller for the accounting management
    /// </summary>
    [Route("api/[controller]")]
    public partial class AccountingController : BaseController
    {
        /// <summary>
        /// get the Journal as paged Result
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost("Journal")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<JournalEntry>>> GetJournal([FromBody]JournalFilterOptions filterModel)
            => ActionResultForAsync(_service.GetJournalAsync(filterModel));

        /// <summary>
        /// export the Journal as an excel file in form of a byte array
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the exported file as a byte array</returns>
        [HttpPost("Journal/Export")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> ExportJournal([FromBody]JournalFilterOptions filterModel)
            => ActionResultForAsync(_service.ExportJournalAsync(filterModel));
    }

    /// <summary>
    /// partial part for <see cref="AccountingController"/>
    /// </summary>
    public partial class AccountingController : BaseController
    {
        private readonly IAccountingService _service;

        public AccountingController(
            IAccountingService accountingService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = accountingService;
        }
    }
}