﻿namespace Axiobat.Presentation.Controllers.Products
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Products;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the product management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class ProductController : BaseController
    {
        /// <summary>
        /// get list of all Products
        /// </summary>
        /// <returns>list of all Products</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<ProductModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<ProductModel>());

        /// <summary>
        /// get a paged result of the Products list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Products as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<ProductModel>>> Get([FromBody] ProductFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<ProductModel, FilterOptions>(filter));

        /// <summary>
        /// retrieve Product with the given id
        /// </summary>
        /// <param name="ProductId">the id of the Product to be retrieved</param>
        /// <returns>the Product</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ProductModel>>> Get([FromRoute(Name = "id")] string ProductId)
            => ActionResultForAsync(_service.GetByIdAsync<ProductModel>(ProductId));

        /// <summary>
        /// create a new Product record
        /// </summary>
        /// <param name="ProductModel">the model to create the Product from it</param>
        /// <returns>the newly created Product</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ProductModel>>> Create(
            [FromBody] ProductPutModel ProductModel)
        {
            var result = await _service.CreateAsync<ProductModel, ProductPutModel>(ProductModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Product informations
        /// </summary>
        /// <param name="ProductModel">the model to use for updating the Product</param>
        /// <param name="ProductId">the id of the Product to be updated</param>
        /// <returns>the updated Product</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ProductModel>>> Update([FromRoute(Name = "id")] string ProductId, [FromBody] ProductPutModel ProductModel) =>
            ActionResultForAsync(_service.UpdateAsync<ProductModel, ProductPutModel>(ProductId, ProductModel));

        /// <summary>
        /// delete the Product with the given id
        /// </summary>
        /// <param name="ProductId">the id of the Product to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string ProductId)
            => ActionResultForAsync(_service.DeleteAsync(ProductId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// save the given memo to the given entity
        /// </summary>
        /// <param name="model">the memo to be saved</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Create/Memo")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<Memo>>> SaveMemoAsync(
            [FromRoute(Name = "id")] string productId,
            [FromBody] MemoModel model)
        {
            var result = await _service.SaveMemoAsync(productId, model);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), result);
        }

        /// <summary>
        /// delete the memo from the entity using the given model
        /// </summary>
        /// <param name="productId">the id of the product</param>
        /// <param name="model">the <see cref="DeleteMemoModel"/> model</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Delete/Memo")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteMemoAsync(
            [FromRoute(Name = "id")] string productId,
            [FromBody] DeleteMemoModel model)
            => ActionResultForAsync(_service.DeleteMemosAsync(productId, model));

        /// <summary>
        /// update the memo from the entity using the given model
        /// </summary>
        /// <param name="productId">the id of the product</param>
        /// <param name="memoId">the id of the memo</param>
        /// <param name="model">the <see cref="MemoModel"/> model</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Update/Memo/{memoId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<Memo>>> UpdateMemoAsync(
            [FromRoute(Name = "id")] string productId,
            [FromRoute(Name = "memoId")] string memoId,
            [FromBody] MemoModel model)
            => ActionResultForAsync(_service.UpdateMemoAsync(productId, memoId, model));

        /// <summary>
        /// get list of products categories types
        /// </summary>
        /// <returns>list of products categories types</returns>
        [HttpGet("Categories")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ListResult<ProductCategoryTypeModel>>> GetCategories()
        {
            var data = await _service.GetProductCategoriesTypeAsync<ProductCategoryTypeModel>();
            return Result.ListSuccess(data);
        }
    }

    public partial class ProductController
    {
        private readonly IProductService _service;

        /// <summary>
        /// create an instant of <see cref="ProductController"/>
        /// </summary>
        /// <param name="loggedInUserService">the logged in user service</param>
        /// <param name="translationService">the translation service</param>
        /// <param name="loggerFactory">the logger factory</param>
        public ProductController(
            IProductService productService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = productService;
        }
    }
}