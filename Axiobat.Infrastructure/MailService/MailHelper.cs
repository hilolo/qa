﻿namespace Axiobat.Infrastructure.MailService
{
    using System;

    /// <summary>
    /// a class that holds Mails helpers
    /// </summary>
    internal static class MailHelper
    {
        public const string Action_Type = "Action_Type";
        public const string Action_Url = "Action_Url";
        public const string Sent_From_Email = "Sent_From_Email";
        public const string User_Company_Name = "User_Company_Name";
        public const string user_userName = "user_userName";
        public const string User_Email = "User_Email";
        public const string User_Full_Name = "User_Full_Name";
        public const string User_Phone_Number = "User_Phone_Number";
        public const string Invitation_Sender_Full_Name = "Invitation_Sender_Full_Name";
        public const string Transaction_Total_Amount = "Transaction_Total_Amount";
        public const string TransactionId = "TransactionId";
        public const string PackDetails = "Pack_Details";
        public const string PaymentMethod = "Payment_Method";

        /// <summary>
        /// this method is used to Encode the value string, only used to Encode the query string value
        /// </summary>
        /// <returns>the encoded parameter</returns>
        internal static string Encode(this string value) => System.Web.HttpUtility.UrlEncode(value);

        /// <summary>
        /// this method is used to Encode the value string, only used to Encode the query string value
        /// </summary>
        /// <returns>the encoded parameter</returns>
        internal static string Encode(this Guid value) => System.Web.HttpUtility.UrlEncode(value.ToString());

        /// <summary>
        /// this method is used to Encode the value string, only used to Encode the query string value
        /// </summary>
        /// <returns>the encoded parameter</returns>
        internal static string Encode(this int value) => System.Web.HttpUtility.UrlEncode(value.ToString());
    }
}
