﻿namespace Axiobat.Infrastructure.PushNotification
{
    using App.Common;
    using Application.Models;
    using Application.Services.PushNotification;
    using Application.Services.Configuration;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// the PushNotification Service implementation of <see cref="IPushNotificationService"/>
    /// </summary>
    public partial class PushNotificationService : IPushNotificationService
    {
        /// <summary>
        /// send a push notification to a specific user, using the given template
        /// </summary>
        /// <param name="usersId">the id of the user to send the messages to it</param>
        /// <param name="messageTemplteId">the id of the template to be used</param>
        /// <param name="placeHolders">the place holders to set for the message template if any</param>
        /// <returns>the operation result</returns>
        public Task SendNotificationAsync(string userId, string messageTemplate, params PlaceHolder[] placeHolders)
            => SendNotificationAsync(new string[] { userId }, messageTemplate, placeHolders);

        /// <summary>
        /// send a push notification to list of users, using the given template
        /// </summary>
        /// <param name="usersIds">the ids of the users to send the messages to them</param>
        /// <param name="messageTemplteId">the id of the template to be used</param>
        /// <param name="placeHolders">the place holders to set for the message template if any</param>
        /// <returns>the operation result</returns>
        public async Task SendNotificationAsync(IEnumerable<string> usersIds, string messageTemplteId, params PlaceHolder[] placeHolders)
        {
            var messages = GetPushNotificationContent().FromJson<PushNotificationMessage[]>();
            if (messages is null)
            {
                _logger.LogError(LogEvent.SendPushNotification, "failed to retrieve push notification messages");
                return;
            }

            var message = messages.FirstOrDefault(m => m.Id == messageTemplteId);
            if (message is null)
            {
                _logger.LogError(LogEvent.SendPushNotification, "Failed to retrieve message with [id: {messageTemplteId}], not exist", messageTemplteId);
                return;
            }

            message.SetPlaceHolders(placeHolders);
            await SendPushNotificationAsync(usersIds, message);
        }

        /// <summary>
        /// send the push notification request to the given users
        /// </summary>
        /// <param name="usersIds">the ids of the users to send the notification for</param>
        /// <param name="message">the message to send</param>
        /// <returns>the operation result</returns>
        private async Task SendPushNotificationAsync(IEnumerable<string> usersIds, PushNotificationMessage message)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var oneSignalOptions = _settings.GetOneSignalOptions();
                    if (oneSignalOptions is null)
                    {
                        _logger.LogError(LogEvent.SendPushNotification, "no settings has been found for push notifications");
                        return;
                    }

                    var requestBody = new
                    {
                        app_id = oneSignalOptions.AppId,
                        include_external_user_ids = usersIds,
                        contents = message.GetContentAsObject(),
                        headings = message.GetTitlesAsObject()
                    }
                    .ToJson();

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("basic", oneSignalOptions.APIKey);
                    var response = await client.PostAsync(oneSignalOptions.BaseUrl, new StringContent(requestBody, Encoding.UTF8, "application/json"));
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        _logger.LogWarning(LogEvent.SendPushNotification, "Failed to send the push notification request response : [{@response}]", response);
                        return;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _logger.LogError(LogEvent.SendPushNotification, ex, "Failed to send push notification message");
            }
        }
    }

    /// <summary>
    /// partial part for <see cref="PushNotificationService"/>
    /// </summary>
    public partial class PushNotificationService
    {
        private readonly ILogger _logger;
        private readonly IApplicationSettingsAccessor _settings;

        /// <summary>
        /// create an instant of <see cref="PushNotificationService"/>
        /// </summary>
        /// <param name="loggerFactory">the logger factory</param>
        /// <param name="options">the <see cref="IOptionsMonitor{TOptions}"/> instant</param>
        public PushNotificationService(ILoggerFactory loggerFactory, IApplicationSettingsAccessor applicationSettingsAccessor)
        {
            _logger = loggerFactory.CreateLogger<PushNotificationService>();
            _settings = applicationSettingsAccessor;
        }

        /// <summary>
        /// return the content of the Push Notification messages file as string
        /// </summary>
        /// <returns>the content of the file</returns>
        private static string GetPushNotificationContent()
        {
            var ContentDataFolder = Path.Combine(FileHelper.GetCurrentExecutingAssemblyDirectory(), "ContentData");
            return File.ReadAllText(Path.Combine(ContentDataFolder, "PushNotificationsMessages.json"));
        }
    }
}
