﻿namespace Axiobat.Infrastructure.FileServices.Helpers
{
    using Application.Models;
    using Application.Enums;
    using Application.Services.Localization;
    using OfficeOpenXml;
    using System.Collections.Generic;
    using OfficeOpenXml.Style;
    using System.Drawing;
    using Axiobat.Domain.Entities;
    using System.Linq;

    /// <summary>
    /// set of helper functions for the excel library
    /// </summary>
    public partial class ExcelService
    {
        internal byte[] GenerateJournalExcelFile(IEnumerable<JournalEntry> journalData, JournalType journalType)
        {
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                var columns = GetColumns(journalType);
                var excelWorksheet = excelPackage.Workbook.Worksheets.Add("Sheet 1");

                var columnIndex = 1;
                foreach (var column in columns)
                {
                    excelWorksheet.Cells[1, columnIndex].Value = column;
                    columnIndex++;
                }

                var startingLine = 2;
                foreach (var journalEntry in journalData)
                {
                    excelWorksheet.Cells[startingLine, 1].Value = journalEntry.JournalCode;
                    excelWorksheet.Cells[startingLine, 2].Value = journalEntry.EntryDate.ToString("dd/MM/yyyy");
                    excelWorksheet.Cells[startingLine, 3].Value = journalEntry.ChartAccount;
                    excelWorksheet.Cells[startingLine, 4].Value = journalEntry.PieceNumber;
                    excelWorksheet.Cells[startingLine, 5].Value = journalEntry.ExternalPartner;

                    excelWorksheet.Cells[startingLine, 6].Value = journalEntry.Amount;
                    excelWorksheet.Cells[startingLine, 7].Value = 0;

                    if (journalEntry.DocumentType == Domain.Enums.DocumentType.CreditNote)
                    {
                        excelWorksheet.Cells[startingLine, 6].Value = 0;
                        excelWorksheet.Cells[startingLine, 7].Value = journalEntry.Amount;
                    }

                    if (journalEntry is CashJournalModel cashJournalEntry)
                    {
                        excelWorksheet.Cells[startingLine, 8].Value = cashJournalEntry.Tiers;
                        excelWorksheet.Cells[startingLine, 9].Value = cashJournalEntry.PaymentType;
                    }

                    // add credits part
                    foreach (var credit in journalEntry.Credits)
                    {
                        startingLine++;

                        excelWorksheet.Cells[startingLine, 1].Value = credit.JournalCode;
                        excelWorksheet.Cells[startingLine, 2].Value = credit.EntryDate.ToString("dd/MM/yyyy");
                        excelWorksheet.Cells[startingLine, 3].Value = credit.ChartAccount;
                        excelWorksheet.Cells[startingLine, 4].Value = credit.PieceNumber;
                        excelWorksheet.Cells[startingLine, 5].Value = credit.ExternalPartner;

                        excelWorksheet.Cells[startingLine, 6].Value = 0;
                        excelWorksheet.Cells[startingLine, 7].Value = credit.Amount;

                        if (journalEntry.DocumentType == Domain.Enums.DocumentType.CreditNote)
                        {
                            excelWorksheet.Cells[startingLine, 6].Value = credit.Amount;
                            excelWorksheet.Cells[startingLine, 7].Value = 0;
                        }

                        if (credit is CashJournalModel creditCashJournalEntry)
                        {
                            excelWorksheet.Cells[startingLine, 8].Value = creditCashJournalEntry.Tiers;
                            excelWorksheet.Cells[startingLine, 9].Value = creditCashJournalEntry.PaymentType;
                        }
                    }

                    startingLine++;
                }

                SetExcelTableHeaderStyle(excelWorksheet, columns.Count);
                return excelPackage.GetAsByteArray();
            }
        }

        internal List<string> GetColumns(JournalType journalType)
        {
            var columns = new List<string>
            {
                T("Code journal"),
                T("Date"),
                T("Numéro de compte"),
                T("Numéro de pièce"),
            };

            if (journalType == JournalType.Expense)
                columns.Add(T("Fournisseur"));

            else if (journalType == JournalType.Sales)
                columns.Add(T("Client"));

            columns.Add(T("Débit"));
            columns.Add(T("Crédit"));

            if (journalType == JournalType.Bank || journalType == JournalType.Cash)
            {
                columns.Add(T("Tiers"));
                columns.Add(T("Type paiement"));
            }

            return columns;
        }

        internal byte[] GenerateContratExcelFile(MaintenanceContract contratEntretien)
        {
            using (ExcelPackage excelPackage = new ExcelPackage())
            {


                var listEquipement = contratEntretien.EquipmentMaintenance;
                foreach (var gammeMaintenanaceEquipement in listEquipement)
                {
                    var nomEquipement = gammeMaintenanaceEquipement.EquipmentName;
                    var brand = gammeMaintenanaceEquipement.Brand;
                    var Model = gammeMaintenanaceEquipement.Model;
                    var Serie = gammeMaintenanaceEquipement.SerialNumber;
                    var Date = gammeMaintenanaceEquipement.InstallationDate;
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add(nomEquipement);//nom de sheets
                                                                                            //Width
                    ws.Column(1).Width = 50;
                    ws.Column(2).Width = 5;
                    ws.Column(3).Width = 5;
                    ws.Column(4).Width = 5;
                    ws.Column(5).Width = 5;
                    ws.Column(6).Width = 5;
                    ws.Column(7).Width = 5;
                    ws.Column(8).Width = 5;
                    ws.Column(9).Width = 5;
                    ws.Column(10).Width = 5;
                    ws.Column(11).Width = 5;
                    ws.Column(12).Width = 5;
                    ws.Column(13).Width = 5;
                    ws.Column(14).Width = 40;
                    //Height first Row
                    ws.Row(1).Height = 25;
                    //Merge
                    //ws.Cells[3, 1, 4, 13].Merge = true;
                    ws.Cells["A1:M1"].Merge = true;
                    ws.Cells["A2:M2"].Merge = true;
                    ws.Cells["B6:M7"].Merge = true;
                    ws.Cells["B3:M3"].Merge = true;
                    ws.Cells["A3:A3"].Merge = true;
                    ws.Cells["A4:A4"].Merge = true;
                    ws.Cells["B4:M4"].Merge = true;
                    ws.Cells[6, 1, 8, 1].Merge = true;
                    ws.Cells["N6:N8"].Merge = true;

                    // Sets Headers
                    ws.Cells["A1:M1"].Value = "Gamme de maintenance préventive";
                    ws.Cells["A2:M2"].Value = nomEquipement;
                    ws.Cells["A3:A3"].Value = "Marque : " + brand;
                    ws.Cells["B3:M3"].Value = "Model : " + Model;
                    ws.Cells["A4:A4"].Value = "Serie : " + Serie;
                    ws.Cells["B4:M4"].Value = "Date d'instalation : " + Date;
                    //ws.Cells[3, 1, 4, 13].Value = brand + " " + Model + " " + Serie;
                    ws.Cells["B6:M7"].Value = "Périodicité";
                    ws.Cells[6, 1, 8, 1].Value = "Libellé opération";
                    ws.Cells["B8"].Value = "J";
                    ws.Cells["C8"].Value = "F";
                    ws.Cells["D8"].Value = "M";
                    ws.Cells["E8"].Value = "A";
                    ws.Cells["F8"].Value = "M";
                    ws.Cells["G8"].Value = "J";
                    ws.Cells["H8"].Value = "J";
                    ws.Cells["I8"].Value = "A";
                    ws.Cells["J8"].Value = "S";
                    ws.Cells["K8"].Value = "O";
                    ws.Cells["L8"].Value = "N";
                    ws.Cells["M8"].Value = "D";

                    Address site = null;
                    if (contratEntretien.Site != null)
                    {
                        Address siteClient = contratEntretien.Site;
                        site = siteClient;
                    }

                    ws.Cells["N1"].Value = contratEntretien.Client.FullName;
                    ws.Cells["N2"].Value = site.Designation;
                    ws.Cells["N3"].Value = site.Street;
                    //ws.Cells["N4"].Value = "";
                    //ws.Cells["N5"].Value = "";
                    ws.Cells["N6:N8"].Value = "Observations - Outillage spécifique - Pièces détachées";

                    int i = 0;
                    foreach (var LotEquipement in gammeMaintenanaceEquipement.MaintenanceOperations)
                    {
                        var nbrOperation = LotEquipement.SubOperations.Count();
                        //Cas Periodecite dans le libelle
                        if (nbrOperation == 0)
                        {
                            ws.Cells[i + 9, 1].Value = LotEquipement.Name;
                            var mois = LotEquipement.Periodicity;
                            ExcelColumn col = ws.Column(1);
                            // this will resize the width to 50
                            col.AutoFit();
                            col.Width = 50;
                            // this is just a style property, and doesn't actually execute any recalculations
                            col.Style.WrapText = true;
                            if (col.Width > 50)
                            {
                                col.Width = 50;
                            }
                            using (ExcelRange rng = ws.Cells[i + 9, 1])
                            {
                                // rng.AutoFitColumns(1);
                                rng.Style.Font.Bold = true;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Color.SetColor(Color.Black);
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                            }
                            foreach (var m in mois)
                            {
                                ws.Cells[i + 9, (int)m + 1].Value = "x";
                                using (ExcelRange rng = ws.Cells[i + 9, 2, i + 9, 13])
                                {
                                    rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                                }
                            }
                            //n10+i
                            using (ExcelRange rng = ws.Cells[i + 9, 14])
                            {
                                //rng.Style.Font.Bold = true;
                                rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Top.Color.SetColor(Color.Black);
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Color.SetColor(Color.Black);
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Color.SetColor(Color.Black);
                            }
                            i = i + 1;

                        }
                        //Cas Periodicite dans operations
                        else
                        {
                            ws.Cells[i + 9, 1, i + 9, 13].Merge = true;
                            ws.Cells[i + 9, 1, i + 9, 13].Value = LotEquipement.Name;
                            //N9+i
                            using (ExcelRange rng = ws.Cells[i + 9, 1, i + 9, 13])
                            {
                                rng.Style.Font.Bold = true;
                                rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Top.Color.SetColor(Color.Black);

                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                            }
                            using (ExcelRange rng = ws.Cells[i + 9, 14])
                            {
                                //rng.Style.Font.Bold = true;
                                rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Top.Color.SetColor(Color.Black);
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Color.SetColor(Color.Black);
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                            }
                            foreach (var OperationsEquipement in LotEquipement.SubOperations)
                            {
                                ws.Cells[i + 10, 1].Value = OperationsEquipement.Name;
                                var mois = OperationsEquipement.Periodicity;
                                //select last observation for operation
                                var lastObservation = "";

                                //if (contratEntretien.VisiteMaintenance != null)
                                //{
                                //    List<ListObservationModel> ListObservation = new List<ListObservationModel>();
                                //    foreach (var visite in contratEntretien.VisiteMaintenance)
                                //    {
                                //        var ficheInterventionMaintenance = visite.FicheInterventionMaintenance;
                                //        if (ficheInterventionMaintenance != null && ficheInterventionMaintenance.Observation != null)
                                //        {
                                //            List<ListObservationModel> observationInFicheIntervention = JsonConvert.DeserializeObject<List<ListObservationModel>>(ficheInterventionMaintenance.Observation);

                                //            ListObservation.AddRange(observationInFicheIntervention);
                                //        }


                                //    }
                                //    if (ListObservation.Count() > 0)
                                //    {
                                //        lastObservation = ListObservation.Where(x => x.id == OperationsEquipement.Id).Select(x => x.observations).LastOrDefault();
                                //    }

                                //}


                                ws.Cells[i + 10, 14].Value = lastObservation; //colomn for observation
                                ExcelColumn col = ws.Column(1);
                                // this will resize the width to 50
                                col.AutoFit();
                                col.Width = 50;
                                // this is just a style property, and doesn't actually execute any recalculations
                                col.Style.WrapText = true;
                                if (col.Width > 50)
                                {
                                    col.Width = 50;
                                }
                                //n10+i
                                using (ExcelRange rng = ws.Cells[i + 10, 14])
                                {
                                    //rng.Style.Font.Bold = true;
                                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Top.Color.SetColor(Color.Black);
                                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Right.Color.SetColor(Color.Black);
                                }
                                using (ExcelRange rng = ws.Cells[i + 10, 1])
                                {
                                    // rng.AutoFitColumns(1);
                                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                                }

                                foreach (var m in mois)
                                {
                                    ws.Cells[i + 10, (int)m + 1].Value = "x";
                                    using (ExcelRange rng = ws.Cells[i + 10, 2, i + 10, 13])
                                    {
                                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                                    }
                                }
                                i = i + 1;
                            }
                            i = i + 1;
                        }

                    }
                    Color DeepBlueHexCode = ColorTranslator.FromHtml("#065cb3");

                    // Format Header of Table
                    using (ExcelRange rng = ws.Cells["A1:M1"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Font.Size = 12;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Font.Color.SetColor(Color.Black);
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);


                    }
                    using (ExcelRange rng = ws.Cells["A2:M2"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Font.Size = 12;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Font.Color.SetColor(Color.Black);
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                    }


                    using (ExcelRange rng = ws.Cells["A5:M5"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid

                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray

                    }
                    using (ExcelRange rng = ws.Cells["B6:M7"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Font.Size = 12;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Top.Color.SetColor(Color.Black);
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);

                    }
                    using (ExcelRange rng = ws.Cells["B8:M8"])
                    {

                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);

                    }

                    using (ExcelRange rng = ws.Cells["A3:A3"])
                    {

                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);

                    }

                    using (ExcelRange rng = ws.Cells["B3:M3"])
                    {

                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);

                    }

                    using (ExcelRange rng = ws.Cells["A4:A4"])
                    {

                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);

                    }

                    using (ExcelRange rng = ws.Cells["B4:M4"])
                    {

                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);

                    }

                    //A6 -A8
                    using (ExcelRange rng = ws.Cells[6, 1, 8, 1])
                    {
                        // ws.Column(1).Width = 100;
                        rng.Style.Font.Bold = true;
                        rng.Style.Font.Size = 12;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DarkGray
                        rng.Style.Font.Color.SetColor(Color.Black);
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Top.Color.SetColor(Color.Black);
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Right.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                    }
                    using (ExcelRange rng = ws.Cells["N1"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DeepBlueHexCode
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Right.Color.SetColor(Color.Black);

                    }

                    using (ExcelRange rng = ws.Cells["N2:N4"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DeepBlueHexCode
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Right.Color.SetColor(Color.Black);

                    }
                    using (ExcelRange rng = ws.Cells["N5"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DeepBlueHexCode
                        rng.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Top.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Right.Color.SetColor(Color.Black);

                    }
                    using (ExcelRange rng = ws.Cells["N6:N8"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Font.Size = 9;
                        rng.Style.Fill.BackgroundColor.SetColor(DeepBlueHexCode); //Set color to DeepBlueHexCode
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                        rng.Style.Border.Top.Color.SetColor(Color.Black);
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Color.SetColor(Color.Black);
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Right.Color.SetColor(Color.Black);

                    }
                }


                //return pck;
                var excelsBytes = excelPackage.GetAsByteArray();
                return excelsBytes;
            }
        }
    }

    /// <summary>
    /// partial part for <see cref="ExcelService"/>
    /// </summary>
    public partial class ExcelService
    {
        private readonly ITranslationService _translationService;

        public ExcelService(ITranslationService translationService)
        {
            _translationService = translationService;
        }

        private string T(string text)
        {
            return _translationService.Get(text);
        }

        /// <summary>
        /// this function is used to set the style of the header
        /// </summary>
        /// <param name="excelWorksheet">the excel Worksheet to style the header for it</param>
        /// <param name="columnsCount">the count of columns to format</param>
        private static void SetExcelTableHeaderStyle(ExcelWorksheet excelWorksheet, int columnsCount)
        {
            // Format Header of Table
            using (ExcelRange rng = excelWorksheet.Cells[1, 1, 1, columnsCount])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                rng.Style.Fill.BackgroundColor.SetColor(Color.Gold); //Set color to DarkGray
                rng.Style.Font.Color.SetColor(Color.Black);
                rng.AutoFitColumns();
            }
        }
    }
}
