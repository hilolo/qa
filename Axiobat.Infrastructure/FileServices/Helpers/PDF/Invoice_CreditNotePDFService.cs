﻿namespace Axiobat.Infrastructure.FileServices.Helpers
{
    using App.Common;
    using Axiobat.Application.Services.Configuration;
    using Axiobat.Domain.Constants;
    using Axiobat.Domain.Entities.Configuration;
    using Domain.Entities;
    using iTextSharp.text;
    using iTextSharp.text.html.simpleparser;
    using iTextSharp.text.pdf;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public partial class Invoice_CreditNotePDFService
    {
        /// <summary>
        /// generate a PDF for Invoice CreditNote
        /// </summary>
        /// <param name="invoice">the invoice document instant</param>
        /// <returns>the PDF document as Byte[]</returns>
        internal byte[] Generate()
        {
            using (var memoryStream = new MemoryStream())
            {
                // create document and PDF writer
                var document = new iTextSharp.text.Document(PageSize.A4, 30, 30, 5, 70);
                var writer = PdfWriter.GetInstance(document, memoryStream);
                writer.PageEvent = new Footer(_pdfOptions);

                document.Open();

                // required data
                var orderDetails = _document.GetProprety<WorkshopOrderDetails>("OrderDetails");
                var client = _document.GetProprety<ClientDocument>("Client");
                var creationDate = _document.GetProprety<DateTime>("CreationDate");
                //var dueDate = _document.GetProprety<DateTime>("CreationDate");

                var dueDate = _document.GetProprety<DateTime>("DueDate");
                var billingAddress = client.BillingAddress;
                var detailLot = _document.GetProprety<bool>("LotDetails");

                // open the document

                // add document header section
                document.Add(CreateHeaderSection(_configuration, _doctype, _document.Reference, client.Reference, client.FullName, creationDate, dueDate, billingAddress, _pdfOptions));

                // add document Address informations section
                document.Add(CreateDocumentAddressSection(billingAddress));

                // Add order details section
                CreateOrderDetailsSection(document, _document.Purpose, orderDetails, detailLot);

                // add Tax details Section
                document.Add(CreateTaxDetailsSection(_document.PaymentCondition.IsValid() ? _document.PaymentCondition : _configuration.PaymentCondition, creationDate, dueDate,
                    orderDetails.GetTaxDetails(), orderDetails));               

                // add Payment Condition section
                //document.Add(CreatePaymentConditionSection(_document.PaymentCondition.IsValid() ? _document.PaymentCondition : _configuration.PaymentCondition, creationDate, dueDate));                //document.Add(CreatePaymentConditionSection(_document.PaymentCondition.IsValid() ? _document.PaymentCondition : _configuration.PaymentCondition, creationDate, dueDate));

                // add Page footer section
                document.Add(CreateFooterSection(_document.Note.IsValid() ? _document.Note : _configuration.Note));

                document.NewPage();

                //document.Add(OnEndPage(writer, document));

                // close document
                document.Close();

                // export the file as byte array
                return memoryStream.ToArray();
            }
        }

        internal byte[] GenerateFilter()
        {
            using (var memoryStream = new MemoryStream())
            {
                // create document and PDF writer
                var document = new iTextSharp.text.Document(PageSize.A4, 30, 30, 5, 60);
                var writer = PdfWriter.GetInstance(document, memoryStream);
                writer.PageEvent = new Footer(_pdfOptions);
                List<byte[]> bytes = new List<byte[]>();

                document.Open();

                foreach (var doc in _documentList)
                {

                    // required data
                    var orderDetails = doc.GetProprety<WorkshopOrderDetails>("OrderDetails");
                    var client = doc.GetProprety<ClientDocument>("Client");
                    var creationDate = doc.GetProprety<DateTime>("CreationDate");
                    var dueDate = doc.GetProprety<DateTime>("DueDate");
                    var billingAddress = client.BillingAddress;
                    var detailLot = _document.GetProprety<bool>("LotDetails");

                    // open the document

                    // add document header section
                    document.Add(CreateHeaderSection(_configuration, _doctype, doc.Reference, client.Reference, client.FullName, creationDate, dueDate, billingAddress, _pdfOptions));

                    // add document Address informations section
                    document.Add(CreateDocumentAddressSection(billingAddress));

                    // Add order details section
                    CreateOrderDetailsSection(document, doc.Purpose, orderDetails, detailLot);

                    // add Tax details Section
                    //document.Add(CreateTaxDetailsSection(orderDetails.GetTaxDetails(), orderDetails));

                    // add Page footer section
                    document.Add(CreateFooterSection(doc.Note.IsValid() ? doc.Note : _configuration.Note));

                    // add Payment Condition section
                    document.Add(CreatePaymentConditionSection(doc.PaymentCondition.IsValid() ? doc.PaymentCondition : _configuration.PaymentCondition, creationDate, dueDate));

                    document.NewPage();

                }

                bytes.Add(memoryStream.ToArray());
                // close document
                document.Close();

                // export the file as byte array
                return memoryStream.ToArray();
            }
        }

    }

    public partial class Invoice_CreditNotePDFService
    {
        private readonly Domain.Entities.Document _document;
        private readonly DocumentConfiguration _configuration;
        private readonly IEnumerable<Domain.Entities.Document> _documentList;
        private readonly PdfOptions _pdfOptions;
        private readonly string _doctype;
        public Invoice_CreditNotePDFService(Domain.Entities.Document document, DocumentConfiguration configuration,
            IEnumerable<Domain.Entities.Document> documentList, string doctype, PdfOptions pdfOptions)
        {
            _document = document;
            _configuration = configuration;
            _documentList = documentList;
            _doctype = doctype;
            _pdfOptions = pdfOptions;
        }

        private static IElement CreateHeaderSection(DocumentConfiguration configuration, string documentType, string documentReference, string clientReference, string nomclient, DateTime creationDate, DateTime dueDate, Address billingAddress, PdfOptions pdfOptions)
        {
            documentType = (documentType == "invoice") ? "Facture" : "Avoirs";


            PdfPTable table_header = new PdfPTable(2)
            {
                WidthPercentage = 100,
                SpacingAfter = 20f,
            };

            // Left header
            PdfPTable leftRow = new PdfPTable(1);
            leftRow.DefaultCell.Border = Rectangle.NO_BORDER;
            // logo
            ConvertBase64ToImage convert = new ConvertBase64ToImage();
            if (pdfOptions.Images.Logo != null && pdfOptions.Images.Logo != "")
            {
                string content = convert.Replace(pdfOptions.Images.Logo);


                var logo = Image.GetInstance(FileHelper.GetByteArray(content));

                logo.ScaleAbsolute(130f, 50f);
                leftRow.AddCell(new PdfPCell(logo) { FixedHeight = 50f, BorderWidth = 0, PaddingTop = 10, PaddingLeft = 0, HorizontalAlignment = Element.ALIGN_LEFT });

            }

            leftRow.AddCell(new PdfPCell() { FixedHeight = 7f, BorderWidth = 0 });
            PdfPTable leftRowseco = new PdfPTable(1);
            //info societe
            var Email = pdfOptions.Header.Email;
            var Address = pdfOptions.Header.Address;

            //          public string PhoneNumber { get; set; }

            //public string Email { get; set; }

            //public Address Address { get; set; }

            // leftRow.AddCell(new PdfPCell(CreateHTMLParagraph(configuration.Header.EnsureValue())) { BorderWidth = 0, PaddingLeft = 15f, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
            //leftRow.AddCell(new PdfPCell(new Paragraph(, PDFFonts.H10Bold2)) { PaddingLeft = 50f, BorderWidth = 0, BorderColor = PDFColors.BorderBotom });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk((pdfOptions.Header.CompanyName == null) ? "" : pdfOptions.Header.CompanyName, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });


            if (Address != null)
            {
                leftRow.AddCell(new PdfPCell(new Paragraph((Address.Street == null) ? "" : Address.Street, PDFFonts.H10B)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
                var ville = (Address.City == null) ? "" : Address.City;
                var PostalCode = (Address.PostalCode == null) ? "" : Address.PostalCode;

                leftRow.AddCell(new PdfPCell(new Paragraph(PostalCode + " " + ville, PDFFonts.H10B)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });

            }
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(string.IsNullOrEmpty(pdfOptions.Header.PhoneNumber) ? "" : "Tél: " + pdfOptions.Header.PhoneNumber, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(string.IsNullOrEmpty(pdfOptions.Header.Email) ? "" : "Email: " + pdfOptions.Header.Email, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });


            //if (Email != null && Email != "")
            //{
            //    leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(Email, PDFFonts.H10B))) { BorderWidth = 0,  HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
            //}


            leftRow.AddCell(new PdfPCell(leftRowseco) { BorderWidth = 0 });
            leftRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
            table_header.AddCell(new PdfPCell(leftRow) { BorderWidth = 0, PaddingTop = 10 });

            // Right table
            PdfPTable rightRow = new PdfPTable(1)
            {
                WidthPercentage = 80
            };
            rightRow.AddCell(new PdfPCell() { FixedHeight = 30f, BorderWidth = 0 });

            PdfPTable infoFacture = new PdfPTable(2);


            //Reference
            infoFacture.AddCell(new PdfPCell(new Paragraph(new Chunk($"{documentType} {documentReference} ", PDFFonts.H10Bold2)))
            { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });


            //Date Creation/echance
            Phrase phraseDateCreation = new Phrase();
            Phrase phraseDateExpiration = new Phrase();
            phraseDateCreation.Add(new Chunk($"Date création : \n{creationDate.ToString("dd/MM/yyyy")}", PDFFonts.H10Bold2));
            //phraseDateCreation.Add(new Chunk(creationDate.ToString("dd/MM/yyyy"), PDFFonts.H10));
            phraseDateExpiration.Add(new Chunk($"Date échéance :\n{dueDate.ToString("dd/MM/yyyy")}", PDFFonts.H10Bold2));
            //phraseDateExpiration.Add(new Chunk(dueDate.ToString("dd/MM/yyyy"), PDFFonts.H10));
            infoFacture.AddCell(new PdfPCell(phraseDateCreation) { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            infoFacture.AddCell(new PdfPCell(phraseDateExpiration) { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            // Client
            infoFacture.AddCell(new PdfPCell(new Paragraph(new Chunk($"Client : {nomclient}  ", PDFFonts.H10Bold2)))
            { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });


            //info client
            PdfPTable infoClient = new PdfPTable(6);
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk($"{documentType} n° ", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Date", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Client", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(documentReference, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });


            Phrase phraseClientnom = new Phrase();
            Phrase phraseclientReference = new Phrase();
            PdfPTable clientInfo = new PdfPTable(1);

            //   doc.Add(p);
            phraseClientnom.Add(new Chunk("Nom : ", PDFFonts.H10B));
            phraseClientnom.Add(new Chunk((nomclient == null) ? " " : nomclient, PDFFonts.H15));
            phraseclientReference.Add(new Chunk("Réference : ", PDFFonts.H10B));
            phraseclientReference.Add(new Chunk((clientReference == null) ? " " : clientReference, PDFFonts.H15));
            clientInfo.AddCell(new PdfPCell(phraseClientnom) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            clientInfo.AddCell(new PdfPCell(phraseclientReference) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            infoClient.AddCell(new PdfPCell(clientInfo) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            // infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(clientReference, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            rightRow.AddCell(new PdfPCell(infoFacture) { BorderWidth = 0, PaddingTop = 0 });
            rightRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });


            // Left header

            PdfPTable adresseRow = new PdfPTable(1);
            adresseRow.WidthPercentage = 80;
            adresseRow.DefaultCell.Border = Rectangle.NO_BORDER;

            adresseRow.AddCell(new PdfPCell(new Paragraph(new Chunk("ADRESSE FACTURATION:", PDFFonts.H10B))) { BorderWidth = 0 });
            adresseRow.AddCell(new PdfPCell(new Paragraph(new Chunk(billingAddress.Street, PDFFonts.H15))) { BorderWidth = 0, Padding = 1f, });
            adresseRow.AddCell(new PdfPCell(new Paragraph(new Chunk(billingAddress.Complement, PDFFonts.H15))) { BorderWidth = 0, Padding = 1f });
            adresseRow.AddCell(new PdfPCell(new Paragraph(new Chunk($"{billingAddress.PostalCode} {billingAddress.City}", PDFFonts.H15))) { BorderWidth = 0, Padding = 1f });

            rightRow.AddCell(new PdfPCell(adresseRow) { BorderWidth = 0, PaddingTop = 10 });

            table_header.AddCell(new PdfPCell(rightRow) { BorderWidth = 0, PaddingTop = 0 });
            return table_header;
        }

        private IElement CreateDocumentAddressSection(Address Address)
        {
            // document.Open();
            PdfPTable table_header = new PdfPTable(2);
            //table_header.WidthPercentage = 100;

            //// Left header
            //PdfPTable leftRow = new PdfPTable(1);

            //table_header.AddCell(new PdfPCell(leftRow) { BorderWidth = 0, PaddingTop = 10 });

            //PdfPTable rightRow = new PdfPTable(1);
            //rightRow.WidthPercentage = 80;
            //rightRow.DefaultCell.Border = Rectangle.NO_BORDER;

            //rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk("ADRESSE FACTURATION:", PDFFonts.H10B))) { BorderWidth = 0 });
            //rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk(Address.Street, PDFFonts.H10))) { BorderWidth = 0 });
            //rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk(Address.Complement, PDFFonts.H10))) { BorderWidth = 0 });
            //rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk($"{Address.PostalCode} {Address.City}", PDFFonts.H10))) { BorderWidth = 0 });

            //table_header.AddCell(new PdfPCell(rightRow) { BorderWidth = 0, PaddingTop = 10 });
            return table_header;
        }

        private IElement CreateFooterSection(string note)
        {
            PdfPTable piedtab = new PdfPTable(1) { SpacingBefore = 10f, WidthPercentage = 100, SpacingAfter = 5f };
            PdfPTable pdfTab = new PdfPTable(1);
            PdfPTable piedContenu = new PdfPTable(1);
            if (note != "")
            {
                piedtab.AddCell(new PdfPCell(new Paragraph(new Chunk("Notes :", PDFFonts.H10B)))
                { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            }
            piedContenu.AddCell(new PdfPCell(CreateHTMLParagraph(note)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            piedContenu.AddCell(new PdfPCell(new Paragraph()) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, PaddingRight = 15 });
            pdfTab.AddCell(new PdfPCell(piedContenu) { BorderWidth = 0 });
            piedtab.AddCell(new PdfPCell(pdfTab) { BorderWidth = 0 });
            return piedtab;
        }

        private static PdfPTable CreatePaymentConditionSection(string paymentCondition, DateTime creationDate, DateTime dueDate)
        {
            PdfPTable conditiontab = new PdfPTable(1) { SpacingBefore = 10f, WidthPercentage = 100, SpacingAfter = 5f };
            if (paymentCondition != "")
            {
                conditiontab.AddCell(new PdfPCell(new Paragraph(new Chunk("Conditions de réglement :", PDFFonts.H10B)))
                { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            }
            conditiontab.AddCell(new PdfPCell(new Phrase
            {
                CreateHTMLParagraph(paymentCondition)
            })
            { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });

            return conditiontab;
        }

        private void CreateOrderDetailsSection(iTextSharp.text.Document PDFDocument, string purpose, WorkshopOrderDetails orderDetails, bool detailLot)
        {
            PdfPTable prestations = new PdfPTable(1)
            {                
                SpacingBefore = 20f,
                WidthPercentage = 100
            };
            prestations.DefaultCell.Border = Rectangle.TABLE;
            prestations.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });

            PdfPTable articlestop = new PdfPTable(1)
            {
                WidthPercentage = 100
            };
            articlestop.DefaultCell.Border = Rectangle.NO_BORDER;

            if (purpose.IsValid() && purpose != null && purpose != "")
            {

                Phrase phraseObjet = new Phrase();
                PdfPTable clientInfo = new PdfPTable(1);

                //   doc.Add(p);
                phraseObjet.Add(new Chunk("Objet : ", PDFFonts.H10B));
                phraseObjet.Add(new Chunk(purpose, PDFFonts.H15));

                articlestop.AddCell(new PdfPCell(new Paragraph(phraseObjet)) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT, Colspan = 2 });
                prestations.AddCell(new PdfPCell(articlestop) { BorderWidth = 0, PaddingBottom = 10 });
                PDFDocument.Add(prestations);
            }

            PdfPTable articlestab = new PdfPTable(12)
            { SpacingBefore = 10f, WidthPercentage = 100 };
            articlestab.DefaultCell.Border = Rectangle.NO_BORDER;

            // Header table                        
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Désignation", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 7 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Quantité", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Prix U.", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Prix HT", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            foreach (var productDetails in orderDetails.LineItems)
            {
                if (productDetails.Type == Domain.Enums.ProductType.Product)
                {
                    var product = (productDetails.Product as JObject).ToObject<MinimalProductDetails>();
                    string designation = product.Reference + "-" + product.Designation == "" ? product.Name : product.Designation;
                    PdfPTable ArticleDeisgnation = new PdfPTable(1);
                    ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk(designation, PDFFonts.H15))) { BorderWidth = 0 });
                    ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk(product.Description, PDFFonts.HItalic))) { BorderWidth = 0, PaddingLeft = 10f });

                    articlestab.AddCell(new PdfPCell(ArticleDeisgnation) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 7 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0}", productDetails.Quantity), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", product.TotalHT) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", product.TotalHT * productDetails.Quantity) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                }

                if (productDetails.Type == Domain.Enums.ProductType.Lot)
                {
                    if (!detailLot && _doctype == "invoice")
                    {
                        var SLotDetail = (productDetails.Product as JObject).ToObject<MinimalLotDetails>();
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(SLotDetail.Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 3f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(" Sous Total HT: " + string.Format("{0:0.00}", SLotDetail.TotalHT), PDFFonts.HItalic))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });

                    }
                    else
                    {

                        var Lot = (productDetails.Product as JObject).ToObject<MinimalLotDetails>();
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(Lot.Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 3f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(" Sous Total HT: " + string.Format("{0:0.00}", Lot.TotalHT), PDFFonts.HItalic))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });

                        foreach (var lotProductDetails in Lot.Products)
                        {
                            PdfPTable ArticleDeisgnation = new PdfPTable(1);
                            string designation = lotProductDetails.ProductDetails.Reference + "-" + lotProductDetails.ProductDetails.Designation == "" ? lotProductDetails.ProductDetails.Name : lotProductDetails.ProductDetails.Designation;
                            ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk("     " + designation, PDFFonts.H15))) { BorderWidth = 0 });
                            ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk(lotProductDetails.ProductDetails.Description, PDFFonts.HItalic))) { BorderWidth = 0, PaddingLeft = 10f });
                            articlestab.AddCell(new PdfPCell(ArticleDeisgnation) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 7 });
                            
                            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0}", lotProductDetails.Quantity), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", lotProductDetails.ProductDetails.TotalHT) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", lotProductDetails.ProductDetails.TotalHT * lotProductDetails.Quantity) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                        }

                    }


                }

                PDFDocument.Add(articlestab);
                articlestab.DeleteBodyRows();
            }

            if (orderDetails.LineItems.Count < 4)
            {
                for (int i = 0; i < 3; i++)
                {
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15)))
                    {
                        BorderWidthBottom = 0,
                        BorderWidthLeft = 0,
                        BorderWidth = 0,
                        Padding = 5f,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 12
                    });

                    PDFDocument.Add(articlestab);
                    articlestab.DeleteBodyRows();
                }
            }
            else
            {
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15)))
                {
                    BorderWidthBottom = 0,
                    BorderWidthLeft = 0,
                    BorderWidth = 0,
                    Padding = 5f,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    Colspan = 12
                });

                PDFDocument.Add(articlestab);
                articlestab.DeleteBodyRows();
            }

            articlestab.DeleteBodyRows();
        }

        private static IElement CreateTaxDetailsSection(string paymentCondition, DateTime creationDate, DateTime dueDate,IEnumerable<TaxDetails> taxDetails, WorkshopOrderDetails orderDetails)
        {
            PdfPTable tabtva = new PdfPTable(3) { SpacingBefore = 25f, WidthPercentage = 100 };

            tabtva.DefaultCell.Border = Rectangle.NO_BORDER;
            tabtva.SetWidths(new float[] { 350f, 70f, 300f });

            PdfPTable tab_calculeMultiTva = new PdfPTable(1);
            tab_calculeMultiTva.DefaultCell.Border = Rectangle.NO_BORDER;
            tab_calculeMultiTva.WidthPercentage = 100;
            PdfPTable eclatementTVA = CreatePaymentConditionSection(paymentCondition, creationDate, dueDate);

            //eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("TVA", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("Base H.T", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("Montant", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            //foreach (var tax in taxDetails)
            //{
            //    eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("" + string.Format("{0:0.00}%", tax.Tax), PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //    eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", tax.TotalHT) + " €", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //    eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", tax.TotalTax) + " €", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //}

            tab_calculeMultiTva.AddCell(new PdfPCell(eclatementTVA) { BorderWidth = 0});

            tab_calculeMultiTva.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });
            tabtva.AddCell(new PdfPCell(tab_calculeMultiTva) { BorderWidth = 0 });
            // space between tabs
            tabtva.AddCell(new PdfPCell() { BorderWidth = 0 });

            PdfPTable condition = new PdfPTable(1);
            PdfPTable tableCalcul = new PdfPTable(2);

            tableCalcul.DefaultCell.Border = Rectangle.NO_BORDER;
            tableCalcul.SetWidths(new float[] { 350f, 150f });
            var MontantHt = orderDetails.TotalHT * (1 + (orderDetails.Proportion / 100));
            var totalHtSansRemise = orderDetails.GetTotalHT();

            if ((orderDetails.GlobalDiscount.Value != 0))
            {
                var MontantSousHt = totalHtSansRemise / (1 + (orderDetails.Proportion / 100));
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk("\nSous Total HT ", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantSousHt) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });

                var symbol = orderDetails.GlobalDiscount.Type == Domain.Enums.TypeValue.Percentage ? "%" : "€";
                var txtMsg = "\nRemise globale " + (symbol == "%" ? "(" + orderDetails.GlobalDiscount.Value.ToString() + symbol + ")" : "");
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(txtMsg, PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", orderDetails.GlobalDiscount.Calculate(totalHtSansRemise)) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });

            }

            tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk("Montant HT", PDFFonts.H15))){ Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantHt) + " €", PDFFonts.H15)))  { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });

            if (orderDetails.Proportion != 0)
            {
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk("\nMontant HT  (prorata" + orderDetails.Proportion + "% inclus ):", PDFFonts.H15))) { Padding = 3f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantHt) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }
           
            if (orderDetails.Proportion != 0)
            {
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk("\nPart Prorata ", PDFFonts.H15))) { Padding = 5f, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantHt - orderDetails.TotalHT) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }

            foreach (var item in taxDetails)
            {
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk("\nT.V.A " + string.Format("{0:0.00}%", item.Tax), PDFFonts.H15))) { Padding = 5f, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", item.TotalTax) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }

            double MontanthtMontantht = (orderDetails.TotalHT * (orderDetails.PUC / 100));
            Phrase phrasep = new Phrase
            {
                new Chunk("\n PARTICIPATION PUC" + orderDetails.PUC + " % calculé sur", PDFFonts.H15),
                new Chunk("\n H.T(Prorata non compris) (H.T -" + orderDetails.PUC + "% * " + orderDetails.PUC / 100 + ")", PDFFonts.H15)
            };

            if (orderDetails.PUC != 0)
            {
                tableCalcul.AddCell(new PdfPCell(phrasep) { Padding = 5f, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontanthtMontantht) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }

            tableCalcul.SpacingAfter = 10;
            tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk("Total TTC ", PDFFonts.H15Bold)))
            { Padding = 5f, BorderWidthLeft = 0, BorderWidth = 0,  BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_CENTER, PaddingTop = 12f });
            tableCalcul.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", orderDetails.TotalTTC) + " €", PDFFonts.H15Bold)))
            { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidthLeft = 0, BorderWidth = 0,
                BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_CENTER, PaddingTop = 12f });

            tableCalcul.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });
            condition.AddCell(new PdfPCell(tableCalcul) { BorderWidth = 0, });
            tabtva.AddCell(new PdfPCell(condition) { BorderWidth = 0, });
            return tabtva;
        }

        private static Paragraph CreateHTMLParagraph(string text)
        {
            Paragraph p = new Paragraph();
            using (StringReader sr = new StringReader(text))
            {
                foreach (IElement e in HtmlWorker.ParseToList(sr, null))
                {
                    p.Add(e);
                }
            }
            return p;
        }


        public partial class Footer : PdfPageEventHelper
        {
            private PdfOptions _pdfOptions;

            public Footer(PdfOptions pdfOptions)
            {
                _pdfOptions = pdfOptions;
            }

            public override void OnEndPage(PdfWriter wri, iTextSharp.text.Document doc)
            {
                //var pdfOption = _configurationDb.GetAsync<PdfOptions>(ApplicationConfigurationType.PdfOptions).GetAwaiter().GetResult();
                PdfTemplate templateNumPage;

                templateNumPage = wri.DirectContent.CreateTemplate(30, 60);


                base.OnEndPage(wri, doc);
                BaseColor borderTopColor = new BaseColor(224, 224, 224);


                Font fontH13 = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(161, 165, 169));
                Font fontH14 = FontFactory.GetFont("Arial", 7, Font.NORMAL, BaseColor.Black);
                int courantPageNumber = wri.CurrentPageNumber;
                String pageText = "Page " + courantPageNumber.ToString();
                PdfPTable footerTab = new PdfPTable(3);
                PdfPTable pdfTab = new PdfPTable(1);
                pdfTab.HorizontalAlignment = Element.ALIGN_BOTTOM;
                pdfTab.TotalWidth = 550;
                pdfTab.AddCell(new PdfPCell() { BorderWidth = 0, FixedHeight = 5f });

                PdfPTable footerContenu = new PdfPTable(2);
                footerContenu.SetWidths(new float[] { 65f, 35f });
                footerContenu.AddCell(new PdfPCell(new Paragraph()) { BorderWidth = 0, PaddingLeft = 15, PaddingTop = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                footerContenu.AddCell(new PdfPCell(new Paragraph(pageText, fontH14)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, PaddingRight = 15 });

                pdfTab.AddCell(new PdfPCell(footerContenu) { BorderWidth = 0 });
                pdfTab.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });


                PdfPTable footerPied = new PdfPTable(1);

                footerPied.AddCell(new PdfPCell() { FixedHeight = 1f, Padding = 1f, BorderWidthBottom = 0, BorderWidthTop = 0.5f, BorderWidth = 0, BorderColorTop = borderTopColor, PaddingTop = 2 });

                pdfTab.AddCell(new PdfPCell(footerPied) { BorderWidth = 0, PaddingLeft = 40, PaddingRight = 40 });

                pdfTab.AddCell(new PdfPCell(new Paragraph((_pdfOptions.Footer.Line1 == null) ? "" : _pdfOptions.Footer.Line1, PDFFonts.H15)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_CENTER });
                pdfTab.AddCell(new PdfPCell(new Paragraph((_pdfOptions.Footer.Line2 == null) ? "" : _pdfOptions.Footer.Line2, PDFFonts.H15)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_CENTER });
                pdfTab.AddCell(new PdfPCell(new Paragraph((_pdfOptions.Footer.Line3 == null) ? "" : _pdfOptions.Footer.Line3, PDFFonts.H15)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_CENTER });

                pdfTab.WriteSelectedRows(0, -2, 18, 70, wri.DirectContent);


                footerTab.AddCell(new PdfPCell(pdfTab) { BorderWidth = 0 });
            }
        }
    }
}
