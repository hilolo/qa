﻿namespace Axiobat.Services.Configuration
{
    using Application.Data;
    using Application.Exceptions;
    using Application.Models.ValidationModels;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// this class is used to control the subscription requirement for a company
    /// and make the proper changes if any.
    /// </summary>
    public partial class ValidationService
    {
        /// <summary>
        /// validate if the workshop has any document associated with it
        /// </summary>
        /// <param name="workshop">the workshop instant</param>
        /// <param name="documentTypes">the documents types to validate</param>
        public async Task WorkshopHasNoDocumentAsync(ConstructionWorkshop workshop, params DocumentType[] documentTypes)
        {
            // check if it has any Quotes
            if (documentTypes.Length <= 0 || documentTypes.Contains(DocumentType.Quote))
            {
                var documentCount = await _quoteDataAccess.GetCountAsync(e => e.WorkshopId == workshop.Id);
                if (documentCount > 0)
                    throw new ValidationException(T("the workshop has {0} quotes associated with", documentCount), MessageCode.ValidationFailed);
            }

            // check if it has any invoices
            if (documentTypes.Length <= 0 || documentTypes.Contains(DocumentType.Invoice))
            {
                var documentCount = await _invoiceDataAccess.GetCountAsync(e => e.WorkshopId == workshop.Id);
                if (documentCount > 0)
                    throw new ValidationException(T("the workshop has {0} invoices associated with", documentCount), MessageCode.ValidationFailed);
            }

            // check if it has any SupplierOrders
            if (documentTypes.Length <= 0 || documentTypes.Contains(DocumentType.SupplierOrder))
            {
                var documentCount = await _supplierOrderDataAccess.GetCountAsync(e => e.WorkshopId == workshop.Id);
                if (documentCount > 0)
                    throw new ValidationException(T("the workshop has {0} supplier order associated with", documentCount), MessageCode.ValidationFailed);
            }

            // check if it has any OperationSheets
            if (documentTypes.Length <= 0 || documentTypes.Contains(DocumentType.OperationSheet))
            {
                var documentCount = await _operationSheetDataAccess.GetCountAsync(e => e.WorkshopId == workshop.Id);
                if (documentCount > 0)
                    throw new ValidationException(T("the workshop has {0} operation sheets associated with", documentCount), MessageCode.ValidationFailed);
            }

            // check if it has any CreditNotes
            if (documentTypes.Length <= 0 || documentTypes.Contains(DocumentType.CreditNote))
            {
                var documentCount = await _creditNoteDataAccess.GetCountAsync(e => e.WorkshopId == workshop.Id);
                if (documentCount > 0)
                    throw new ValidationException(T("the workshop has {0} Credit Note associated with", documentCount), MessageCode.ValidationFailed);
            }

            // check if it has any Expenses
            if (documentTypes.Length <= 0 || documentTypes.Contains(DocumentType.Expenses))
            {
                var documentCount = await _expenseDataAccess.GetCountAsync(e => e.WorkshopId == workshop.Id);
                if (documentCount > 0)
                    throw new ValidationException(T("the workshop has {0} Expenses associated with", documentCount), MessageCode.ValidationFailed);
            }
        }

        public async Task<DeleteUserDocumentValidation> HasDocumentsAsync(Supplier supplier)
        {
            var result = new DeleteUserDocumentValidation
            {
                IsSuccess = true
            };

            var supplierOrders = await _supplierOrderDataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddPredicate(supplierOrder => supplierOrder.SupplierId == supplier.Id);
            },
            supplierOrder => new { supplierOrder.Id, supplierOrder.DocumentType, supplierOrder.Reference });

            if (supplierOrders.Any())
            {
                result.IsSuccess = false;
                foreach (var supplierOrder in supplierOrders)
                {
                    result.Documents.Add(new AssociatedDocumentValidationItem
                    {
                        Reference = supplierOrder.Reference,
                        DocumentId = supplierOrder.ToString(),
                        DocType = supplierOrder.DocumentType,
                    });
                }
            }

            var expenses = await _expenseDataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddPredicate(expense => expense.SupplierId == supplier.Id);
            },
            expense => new { expense.Id, expense.DocumentType, expense.Reference });

            if (expenses.Any())
            {
                result.IsSuccess = false;
                foreach (var expense in expenses)
                {
                    result.Documents.Add(new AssociatedDocumentValidationItem
                    {
                        Reference = expense.Reference,
                        DocumentId = expense.ToString(),
                        DocType = expense.DocumentType,
                    });
                }
            }

            var products = await _productDataAccess.GetBySupplierIdAsync(supplier.Id);

            if (products.Any())
            {
                result.IsSuccess = false;
                foreach (var product in products)
                {
                    result.Documents.Add(new AssociatedDocumentValidationItem
                    {
                        DocumentId = product.Id,
                        Reference = product.Reference,
                        DocType = product.DocumentType,
                    });
                }
            }

            return result;
        }

        public async Task<DeleteUserDocumentValidation> HasDocumentsAsync(User user)
        {
            var result = new DeleteUserDocumentValidation
            {
                IsSuccess = true
            };

            var contracts = await _publishingContractDataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddPredicate(e => e.UserId == user.Id);
            },
            contract => contract.Id);

            if (contracts.Any())
            {
                result.IsSuccess = false;
                foreach (var contractId in contracts)
                {
                    result.Documents.Add(new AssociatedDocumentValidationItem
                    {
                        DocType = DocumentType.PublishingContract,
                        DocumentId = contractId.ToString(),
                    });
                }
            }

            var maintenanceOperationSheets = await _maintenanceOperationSheetDataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddPredicate(e => e.TechnicianId == user.Id);
            },
            operationSheet => new { operationSheet.Id, operationSheet.Reference });

            if (maintenanceOperationSheets.Length > 0)
            {
                result.IsSuccess = false;
                foreach (var operationSheet in maintenanceOperationSheets)
                {
                    result.Documents.Add(new AssociatedDocumentValidationItem
                    {
                        DocumentId = operationSheet.Id,
                        Reference = operationSheet.Reference,
                        DocType = DocumentType.MaintenanceOperationSheet,
                    });
                }
            }

            var OperationSheets = await _operationSheetDataAccess.GetTechnicianOperationSheetsAsync(user.Id);

            if (OperationSheets.Any())
            {
                result.IsSuccess = false;
                foreach (var operationSheet in OperationSheets)
                {
                    result.Documents.Add(new AssociatedDocumentValidationItem
                    {
                        DocumentId = operationSheet.Id,
                        Reference = operationSheet.Reference,
                        DocType = DocumentType.OperationSheet,
                    });
                }
            }

            return result;
        }

        public async Task CanCancelInvoiceAsync(Invoice invoice)
        {
            if (invoice.TypeInvoice == InvoiceType.Situation ||
                invoice.TypeInvoice == InvoiceType.Acompte)
            {
                var quoteSituation = await _quoteDataAccess.GetSingleAsync(
                    options =>
                    {
                        options.AddDefaultIncludes(false);
                        options.AddPredicate(quote => quote.Id == invoice.QuoteId);
                    },
                    quote => quote.Situations);

                if (quoteSituation is null)
                    throw new ValidationException($"invoice [Ref: {invoice.Reference}] must have a quote", MessageCode.InvoiceMustHaveQuote);

                if (quoteSituation.Any(situation => situation.TypeInvoice == InvoiceType.Cloture))
                    throw new ValidationException("invoice [Ref: {invoice.Reference}] cannot be canceled, because associated quote is closed", MessageCode.InvoiceCannotBeCanceledQuoteClosed);
            }
        }
    }

    /// <summary>
    /// the partial part for <see cref="ValidationService"/>
    /// </summary>
    public partial class ValidationService : BaseService, IValidationService
    {
        private readonly IQuoteDataAccess _quoteDataAccess;
        private readonly IProductDataAccess _productDataAccess;
        private readonly IInvoiceDataAccess _invoiceDataAccess;
        private readonly IExpenseDataAccess _expenseDataAccess;
        private readonly ICreditNoteDataAccess _creditNoteDataAccess;
        private readonly ISupplierOrderDataAccess _supplierOrderDataAccess;
        private readonly IOperationSheetDataAccess _operationSheetDataAccess;
        private readonly IDataAccess<PublishingContract, int> _publishingContractDataAccess;
        private readonly IMaintenanceOperationSheetDataAccess _maintenanceOperationSheetDataAccess;

        /// <summary>
        /// name of the entity
        /// </summary>
        protected override string EntityName => typeof(ValidationService).Name;

        public ValidationService(
            IQuoteDataAccess quoteDataAccess,
            IProductDataAccess productDataAccess,
            IInvoiceDataAccess invoiceDataAccess,
            IExpenseDataAccess expenseDataAccess,
            ICreditNoteDataAccess creditNoteDataAccess,
            ISupplierOrderDataAccess supplierOrderDataAccess,
            IOperationSheetDataAccess operationSheetDataAccess,
            IDataAccess<PublishingContract, int> publishingContractDataAccess,
            IMaintenanceOperationSheetDataAccess maintenanceOperationSheetDataAccess,
            ILoggedInUserService currentUser,
            IApplicationConfigurationService appConfigService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(currentUser, appConfigService, translationService, loggerFactory, mapper)
        {
            _quoteDataAccess = quoteDataAccess;
            _productDataAccess = productDataAccess;
            _invoiceDataAccess = invoiceDataAccess;
            _creditNoteDataAccess = creditNoteDataAccess;
            _supplierOrderDataAccess = supplierOrderDataAccess;
            _operationSheetDataAccess = operationSheetDataAccess;
            _publishingContractDataAccess = publishingContractDataAccess;
            _maintenanceOperationSheetDataAccess = maintenanceOperationSheetDataAccess;
            _expenseDataAccess = expenseDataAccess;
        }
    }
}
