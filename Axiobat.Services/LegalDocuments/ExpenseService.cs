﻿namespace Axiobat.Services.Documents
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Axiobat.Application.Services.Contacts;
    using Axiobat.Domain.Enums;
    using Domain.Constants;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Expense"/>
    /// </summary>
    public partial class ExpenseService : DocumentService<Expense>, IExpenseService
    {
        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memos.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToList();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await base._dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// cancel the expense with the given id
        /// </summary>
        /// <param name="expenseId">the id of the expense to be canceled</param>
        /// <param name="cancelationDocument">the cancellation document</param>
        /// <returns>the operation result</returns>
        public async Task<Result<MemoModel>> CancelExpenseAsync(string expenseId, MemoModel cancelationDocument)
        {
            var expense = await _dataAccess.GetByIdAsync(expenseId);
            if (expense is null)
                throw new NotFoundException();

            // check if the expense is already canceled
            if (expense.Status == ExpenseStatus.Canceled)
                Result.Failed<Attachment>("the given expense is already canceled");

            // check if it has any paiements
            if (await _dataAccess.HasPaymentsAsync(expenseId))
                Result.Failed<Attachment>("the expense has a payment, cannot be canceled");

            // saving the file
            var fileSaved = await _fileService.SaveAsync(cancelationDocument.Attachments.ToArray());
            if (fileSaved.Count <= 0)
                return Result.Failed<MemoModel>("Failed to save the file");

            // update the status
            expense.Status = ExpenseStatus.Canceled;
            expense.CancellationDocument = new Memo
            {
                Attachments = fileSaved,
                CreatedOn = DateTime.Now,
                Comment = cancelationDocument.Comment,
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
                Id = cancelationDocument.Id.IsValid() ? cancelationDocument.Id : Generator.GenerateRandomId(),
            };

            // save the changes
            var updateResult = await _dataAccess.UpdateAsync(expense);
            if (!updateResult.IsSuccess)
                return Result.From<MemoModel>(updateResult);

            // return the file
            return Map<MemoModel>(expense.CancellationDocument);
        }
    }

    /// <summary>
    /// partial part for <seealso cref="IExpenseService"/>
    /// </summary>
    public partial class ExpenseService : DocumentService<Expense>, IExpenseService
    {
        private readonly ISupplierService _supplierService;
        private readonly ISupplierOrderService _supplierOrderService;
        private new IExpenseDataAccess _dataAccess => base._dataAccess as IExpenseDataAccess;

        public ExpenseService(
            ISupplierService supplierService,
            IExpenseDataAccess dataAccess,
            ISupplierOrderService supplierOrderService,
            ISynchronizationResolverService synchronizeDataService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizeDataService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _supplierService = supplierService;
            _supplierOrderService = supplierOrderService;
        }


        /// <summary>
        /// before inserting the expense
        /// </summary>
        /// <typeparam name="TCreateModel">the expense create model</typeparam>
        /// <param name="expense">the expense entity</param>
        /// <param name="model">the create model instant</param>
        protected override async Task InCreate_BeforInsertAsync<TCreateModel>(Expense expense, TCreateModel createModel)
        {
            if (!(createModel is ExpensePutModel model))
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(ExpensePutModel));

            expense.Attachments = await _fileService.SaveAsync(model.Attachments.ToArray());

            // get the supplier orders if any
            expense.SupplierOrders = model.SupplierOrdersId
                .Select(e => new SupplierOrders_Expenses
                {
                    ExpenseId = expense.Id,
                    SupplierOrderId = e,
                })
                .ToList();
        }

        protected override async Task InUpdate_BeforUpdateAsync<TUpdateModel>(Expense expense, TUpdateModel updateModel)
        {
            if (!(updateModel is ExpensePutModel model))
                throw new ModelTypeIsNotValidException(nameof(updateModel), typeof(ExpensePutModel));

            var intersection = expense.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(model.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = model.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));

            // remove the attachments from the list
            foreach (var attachment in expense.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)))
                expense.Attachments.Remove(attachment);

            // add the new attachments
            foreach (var attachment in await _fileService.SaveAsync(Added.ToArray()))
                expense.Attachments.Add(attachment);

            // remove attachments from server
            await _fileService.DeleteAsync(removed);

            expense.Workshop = null;
        }

        /// <summary>
        /// after successfully creating the expense
        /// </summary>
        /// <typeparam name="TCreateModel">the expense create model</typeparam>
        /// <param name="entity">the expense entity</param>
        /// <param name="model">the create model instant</param>
        protected override async Task InCreate_AfterInsertAsync<TCreateModel>(Expense entity, TCreateModel createModel)
        {
            if (!(createModel is ExpensePutModel model))
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(ExpensePutModel));

            await _supplierOrderService.UpdateStatusAsync(SupplierOrderStatus.Billed, model.SupplierOrdersId.ToArray());
        }

        /// <summary>
        /// after getting the entity by the id
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="mappedEntity">the mapped entity instant</param>
        protected override Task InGet_AfterMappingAsync<TOut>(Expense entity, TOut mappedEntity)
        {
            if (mappedEntity is ExpenseModel expenseModel)
            {
                var documents = entity.Payments.Select(e => new AssociatedDocument
                {
                    Id = e.Payment.Id,
                    DocumentType = DocumentType.Payment,
                    CreationDate = e.Payment.CreatedOn.DateTime,
                })
                .Concat(entity.SupplierOrders.Select(e => new AssociatedDocument
                {
                    Id = e.SupplierOrderId,
                    Status = e.SupplierOrder.Status,
                    Reference = e.SupplierOrder.Reference,
                    CreationDate = e.SupplierOrder.CreationDate,
                    DocumentType = DocumentType.SupplierOrder,
                }));

                foreach (var item in documents)
                    expenseModel.AssociatedDocuments.Add(item);
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }
    }
}
