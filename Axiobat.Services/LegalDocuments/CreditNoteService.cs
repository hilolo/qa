﻿namespace Axiobat.Services.Documents
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Axiobat.Application.Enums;
    using Axiobat.Application.Services.Contacts;
    using Axiobat.Application.Services.MailService;
    using Axiobat.Domain.Exceptions;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="CreditNote"/>
    /// </summary>
    public partial class CreditNoteService : DocumentService<CreditNote>, ICreditNoteService
    {

        /// <summary>
        /// send the quote with the given id with an email using the given options
        /// </summary>
        /// <param name="invoiceId">the id of the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendAsync(string creditNoteId, SendEmailOptions emailOptions)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(creditNoteId);
            if (document is null)
                throw new NotFoundException("there is no invoice with the given id");

            // generate the document Pdf
            //var filePDF = await ExportDataAsync(creditNoteId, new DataExportOptions { ExportType = ExportType.PDF });
            //if (!filePDF.IsSuccess)
            //    return filePDF;

            // generate an id to the email
            emailOptions.Id = $"{creditNoteId}-".AppendTimeStamp();

            // add the PDF file to the email attachments
            //emailOptions.Attachments.Add(new AttachmentModel
            //{
            //    FileType = "application/pdf",
            //    Content = Convert.ToBase64String(filePDF),
            //    FileName = $"{DocumentType.Invoice}-{DateHelper.Timestamp}.pdf",
            //});

            // send the email
            var result = await _emailService.SendEmailAsync(emailOptions);
            if (!result.IsSuccess)
                return result;

            // add the email document
            document.Emails.Add(new DocumentEmail
            {
                To = emailOptions.To,
                SentOn = DateTime.Now,
                MailId = emailOptions.Id,
                Content = emailOptions.Body,
                Subject = emailOptions.Subject,
                User = _loggedInUserService.GetMinimalUser(),
            });

            // update the document
            return await _dataAccess.UpdateAsync(document);
        }
        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                User = _loggedInUserService.GetMinimalUser(),
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memos.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await base._dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// duplicate the CreditNote with the given id
        /// </summary>
        /// <param name="documentId">the id of the CreditNote to be duplicated</param>
        /// <returns>the generated CreditNote</returns>
        public async Task<Result<TOut>> DuplicateAsync<TOut>(string documentId)
        {
            // retrieve the quote
            var creditNote = await _dataAccess.GetByIdAsync(documentId);
            if (creditNote is null)
                throw new NotFoundException("there is no quote with the given id");

            // generate a new reference
            var reference = await _configuration.GenerateRefereceAsync(DocumentType.Quote);

            // empty the quote date
            creditNote.Id = creditNote.GenerateId();
            creditNote.Reference = reference;
            creditNote.ChangesHistory.Clear();
            creditNote.Workshop = null;
            creditNote.Invoice = null;

            // insert the new quote
            var addResult = await _dataAccess.AddAsync(creditNote);
            if (!addResult.IsSuccess)
                return Result.From<TOut>(addResult);

            // increment reference
            await IncrementRefenereceAsync();

            // return result
            return Map<TOut>(creditNote);
        }

        /// <summary>
        /// generate a credit note to cancel an invoice
        /// </summary>
        /// <param name="invoice">the invoice to be canceled</param>
        /// <returns>the generated credit note</returns>
        public async Task<Result<CreditNoteModel>> GenerateForInvoiceCancelationAsync(Invoice invoice)
        {
            // retrieve configuration
            var config = await _configuration.GetDocumentConfigAsync(DocumentType.CreditNote);

            // generate a reference
            var reference = await _configuration.GenerateRefereceAsync(DocumentType.CreditNote);

            // create credit note
            var creditNote = new CreditNote
            {
                Reference = reference,
                InvoiceId = invoice.Id,
                ClientId = invoice.ClientId,
                Client = invoice.Client,
                Status = CreditNoteStatus.Used,
                WorkshopId = invoice.WorkshopId,
                CreationDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(config.Validity),
                Note = config.Note,
                PaymentCondition = config.PaymentCondition,
                Purpose = config.Purpose,
            };

            creditNote.OrderDetails = new WorkshopOrderDetails
            {
                TotalHT = invoice.OrderDetails.TotalHT * -1,
                TotalTTC = invoice.OrderDetails.TotalTTC * -1,
                GlobalDiscount = invoice.OrderDetails.GlobalDiscount,
                GlobalVAT_Value = invoice.OrderDetails.GlobalVAT_Value,
                HoldbackDetails = invoice.OrderDetails.HoldbackDetails,
                ProductsDetailsType = invoice.OrderDetails.ProductsDetailsType,
                ProductsDetails_File = invoice.OrderDetails.ProductsDetails_File,
                ProductsPricingDetails = invoice.OrderDetails.ProductsPricingDetails,
                Proportion = invoice.OrderDetails.Proportion,
                PUC = invoice.OrderDetails.PUC,
                LineItems = invoice.OrderDetails.LineItems
                .Select(productDetail =>
                {
                    var details = new OrderProductDetails
                    {
                        Type = productDetail.Type,
                        Quantity = productDetail.Quantity
                    };

                    if (productDetail.Type == ProductType.Product)
                    {
                        var product = (productDetail.Product as JObject).ToObject<MinimalProductDetails>();
                        product.HourlyCost = product.HourlyCost * -1;
                        product.TotalHT = product.TotalHT * -1;
                        product.TotalTTC = product.TotalTTC * -1;
                        product.MaterialCost = product.MaterialCost * -1;
                        //


                        details.Product = product.ToJObject();
                    }

                    if (productDetail.Type == ProductType.Lot)
                    {
                        var lot = (productDetail.Product as JObject).ToObject<MinimalLotDetails>();
                        foreach (var product in lot.Products)
                        {
                            product.ProductDetails.HourlyCost = product.ProductDetails.HourlyCost * -1;
                            product.ProductDetails.TotalHT = product.ProductDetails.TotalHT * -1;
                            product.ProductDetails.TotalTTC = product.ProductDetails.TotalTTC * -1;
                            product.ProductDetails.MaterialCost = product.ProductDetails.MaterialCost * -1;
                            //
                        }

                        details.Product = lot.ToJObject();
                    }

                    return details;
                })
               .ToArray()
            };

            // insert credit note to database
            var result = await _dataAccess.AddAsync(creditNote);
            if (!result.IsSuccess)
                return Result.From<CreditNoteModel>(result);

            // increment reference
            await IncrementRefenereceAsync();

            // return result
            return Map<CreditNoteModel>(result.Value);
        }

        /// <summary>
        /// update the status of the credit note with the given id
        /// </summary>
        /// <param name="creditNoteId">the id of the credit note</param>
        /// <param name="status">the status to be assigned to it</param>
        public async Task UpdateStatusAsync(string creditNoteId, string status)
        {
            var creditNote = await _dataAccess.GetByIdAsync(creditNoteId);
            if (!(creditNote is null))
            {
                creditNote.Status = status;

                // update the credit note
                await _dataAccess.UpdateAsync(creditNote);
            }
        }

        /// <summary>
        /// create a credit note for the payment with the given invoice
        /// </summary>
        /// <param name="invoice">the invoice to generate the credit note for it</param>
        /// <returns>the created credit note with the desired output</returns>
        public async Task<Result<TOut>> CreateForPaymentAsync<TOut>(Invoice invoice, double totalPayment)
        {
            var text = $"{T("Paiement par avoir de la facture")} {invoice.Reference}";

            var creditNote = new CreditNote
            {
                InvoiceId = invoice.Id,
                Reference = await _configuration.GenerateRefereceAsync(DocumentType.CreditNote),
                PaymentCondition = invoice.PaymentCondition,
                Purpose = invoice.Purpose,
                Note = invoice.Note,
                Status = CreditNoteStatus.Used,
                WorkshopId = invoice.WorkshopId,
                ClientId = invoice.ClientId,
                Client = invoice.Client,
                CreationDate = DateTime.Now,
                DueDate = invoice.DueDate,
                OrderDetails = new WorkshopOrderDetails
                {
                    TotalHT =(float) totalPayment * (-1),
                    TotalTTC = (float)totalPayment * (-1),
                    LineItems = new HashSet<OrderProductDetails>
                    {
                        new OrderProductDetails
                        {
                            Type = ProductType.Product,
                            Quantity = 1,
                            Product = new MinimalProductDetails
                            {
                                Name = text,
                                Description = text,
                                TotalHT = (float)totalPayment * (-1),
                                TotalTTC = (float)totalPayment * (-1),
                                Category = await _classificationService.GetDefaultClassificationAsync(),
                            },
                        }
                    }
                }
            };

            _history.Recored(creditNote, ChangesHistoryType.Added, text);

            var addResult = await _dataAccess.AddAsync(creditNote);
            if (!addResult.IsSuccess)
                return Result.From<TOut>(addResult);

            await IncrementRefenereceAsync();

            return Map<TOut>(creditNote);
        }
    }

    /// <summary>
    /// partial part for <see cref="CreditNoteService"/>
    /// </summary>
    public partial class CreditNoteService : DocumentService<CreditNote>, ICreditNoteService
    {
        private readonly IEmailService _emailService;
        private readonly IClientService _clientService;
        private readonly IClassificationService _classificationService;

        public CreditNoteService(
            ICreditNoteDataAccess dataAccess,
            IClientService clientService,
            IClassificationService classificationService,
            ISynchronizationResolverService synchronizeDataService,
            IFileService fileService,
            IValidationService validation,
            IEmailService emailService,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizeDataService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _emailService = emailService;
            _clientService = clientService;
            _classificationService = classificationService;
        }


        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(CreditNote creditNote, TUpdateModel updateModel)
        {
            if (!(updateModel is CreditNotePutModel))
                throw new ModelTypeIsNotValidException(nameof(updateModel), typeof(QuotePutModel));

            creditNote.Workshop = null;

            return Task.CompletedTask;
        }

        /// <summary>
        /// after getting the entity by the id
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="mappedEntity">the mapped entity instant</param>
        protected override Task InGet_AfterMappingAsync<TOut>(CreditNote entity, TOut mappedEntity)
        {
            if (mappedEntity is CreditNoteModel model)
            {
                var documents = Map<IEnumerable<AssociatedDocument>>(entity.Payments)
                    .Concat(new[] { Map<AssociatedDocument>(entity.Invoice) });

                foreach (var item in documents)
                    model.AssociatedDocuments.Add(item);
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }
    }
}
