﻿namespace Axiobat.Services.Accounting
{
    using Application.Services.Accounting;
    using AutoMapper;
    using Axiobat.Application.Data;
    using Axiobat.Application.Models;
    using Axiobat.Application.Services;
    using Axiobat.Application.Services.AccountManagement;
    using Axiobat.Application.Services.Configuration;
    using Axiobat.Application.Services.Localization;
    using Axiobat.Domain.Constants;
    using Axiobat.Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IDashboardService"/>
    /// </summary>
    public partial class DashboardService
    {
        public async Task<Result<DashboardTurnOverDetailsModel>> GetTurnOverDetailsAsync(int year)
        {
            var invoices = await _dashboardDataAccess.GetInvoicesForTurnOverDetailsAsync(year);
            var creditNotes = await _dashboardDataAccess.GetCreditNotesForTurnOverDetailsAsync(year);
            var quotes = await _dashboardDataAccess.GetQuotesForTurnOverDetailsAsync(year);

            var result = new DashboardTurnOverDetailsModel();

            for (int month = 1; month <= 12; month++)
            {
                var totalInvoices_current = invoices
                    .Where(e => e.CreationDate.Date.Year == year && e.CreationDate.Date.Month == month)
                    .Sum(e => e.GetTotalHT());

                var totalCreditNotes_current = creditNotes
                    .Where(e => e.CreationDate.Date.Year == year && e.CreationDate.Date.Month == month)
                    .Sum(e => e.GetTotalHT());

                var totalQuotes_current = quotes
                    .Where(e => e.CreationDate.Date.Year == year && e.CreationDate.Date.Month == month)
                    .Sum(e => e.GetTotalHT());

                var totalInvoices_pre = invoices
                    .Where(e => e.CreationDate.Date.Year == year && e.CreationDate.Date.Month == month)
                    .Sum(e => e.GetTotalHT());

                var totalCreditNotes_pre = creditNotes
                    .Where(e => e.CreationDate.Date.Year == year && e.CreationDate.Date.Month == month)
                    .Sum(e => e.GetTotalHT());

                var totalQuotes_pre = quotes
                    .Where(e => e.CreationDate.Date.Year == year && e.CreationDate.Date.Month == month)
                    .Sum(e => e.GetTotalHT());

                result.CurrentPeriod.Add(new DashboardPerMonthTurnOverDetailsModel(year, month, totalInvoices_current - totalCreditNotes_current, totalQuotes_current));
                result.PreCurrentPeriod.Add(new DashboardPerMonthTurnOverDetailsModel(year, month, totalInvoices_pre - totalCreditNotes_pre, totalQuotes_pre));
            }

            return result;
        }

        public async Task<Result<DashboardExpensesDetailsModel>> GetExpensesDetailsAsync(int year)
        {
            var expenses = await _dashboardDataAccess.GetExpensesForDetailsAsync(year);
            var result = new DashboardExpensesDetailsModel();

            for (int month = 1; month <= 12; month++)
            {
                var totalExpenses_current = expenses
                    .Where(e => e.CreationDate.Date.Year == year && e.CreationDate.Date.Month == month)
                    .Sum(e => e.OrderDetails.TotalTTC);

                var totalExpenses_pre = expenses
                    .Where(e => e.CreationDate.Date.Year == year - 1 && e.CreationDate.Date.Month == month)
                    .Sum(e => e.OrderDetails.TotalTTC);

                result.CurrentPeriod.Add(new PerMonthTotalDetails(year, month, totalExpenses_current));
                result.PreCurrentPeriod.Add(new PerMonthTotalDetails(year - 1, month, totalExpenses_pre));
            }

            return result;
        }

        public async Task<Result<DashboardCashSummaryDetailsModel>> GetCashSummaryDetailsAsync()
        {
            var invoices = await _dashboardDataAccess.GetInvoicesForCashSummaryDetailsAsync();
            var expenses = await _dashboardDataAccess.GetExpensesForCashSummaryDetailsAsync();

            var totalInvoicePayment = invoices.Sum(e => e.GetTotalPaid());
            var totalInvoiceLeftToPay = invoices.Sum(e => e.GetRestToPay());

            var totalExpensePayment = expenses.Sum(e => e.GetTotalPaid());
            var totalExpenseLeftToPay = expenses.Sum(e => e.GetRestToPay());

            return new DashboardCashSummaryDetailsModel
            {
                TotalInvoicePayment = (float)totalInvoicePayment,
                TotalInvoiceLeftToPay = (float)totalInvoiceLeftToPay,
                TotalExpensePayment = (float)totalExpensePayment,
                TotalExpenseLeftToPay = (float)totalExpenseLeftToPay,
            };
        }

        public async Task<Result<DashboardDocumentsDetailsModel>> GetDocumentsDetailsAsync(DateRangeFilterOptions filterOptions)
        {
            var charts = await _chartOfAccountsService.GetChartOfAccountsAsync();

            var quotes = await _dashboardDataAccess.GetQuotesForDocumentDetailsAsync(filterOptions);
            var invoices = await _dashboardDataAccess.GetInvoicesForDocumentDetailsAsync(filterOptions);

            var workshops = await _dashboardDataAccess.GetWorkshopsForDocumentDetailsAsync(filterOptions);
            var contracts = await _dashboardDataAccess.GetContractsForDocumentDetailsAsync(filterOptions);

            var operationSheet = await _dashboardDataAccess.GetOperationSheetForDocumentDetailsAsync(filterOptions);
            var operationSheetMaintence = await _dashboardDataAccess.GetOperationSheetMaintenceForDocumentDetailsAsync(filterOptions);

            return new DashboardDocumentsDetailsModel(
               AnalyticsService.GetQuoteDetails(quotes),
               AnalyticsService.GetInvoiceDetails(invoices),
               AnalyticsService.GetWorkshopDetails(workshops, charts[ChartOfAccounts.Expenses]),
               AnalyticsService.GetContractDetails(contracts),
               AnalyticsService.GetOperationSheetDetails(operationSheet, operationSheetMaintence)
           );
        }

        public async Task<Result<DashboardTopsDetailsModel>> GetTopsDetailsAsync(int topValue)
        {
            var defaultValues = await _configuration.GetAsync<DefaultValue>(ApplicationConfigurationType.DefaultValues);

            var workshops = await _dashboardDataAccess.GetWorkshopsForTopsDetailsAsync();
            var contracts = await _dashboardDataAccess.GetContractsForTopsDetailsAsync();
            var invoices = await _dashboardDataAccess.GetInvoicesForTopsDetailsAsync();
            var creditNotes = await _dashboardDataAccess.GetCreditNotesForTopsDetailsAsync();
            var expenses = await _dashboardDataAccess.GetExpensesForTopsDetailsAsync();
            var labels = await _labelDataAccess.GetAllAsync(null);

            var result = new DashboardTopsDetailsModel
            {
                TopTechniciansByWorkingHours = GetTopTechnicians(contracts, defaultValues),
                TopClients = GetTopClients(invoices, creditNotes, topValue)
            };

            SetTopWorkshopsDetails(result, defaultValues, workshops, topValue);
            SetTopProducts(result, invoices, labels, topValue);
            SetTopSuppliersDetails(result, expenses, topValue);

            return result;
        }
    }

    /// <summary>
    /// partial part for <see cref="DashboardService"/>
    /// </summary>
    public partial class DashboardService : BaseService, IDashboardService
    {
        protected override string EntityName => "Dashboard";

        private readonly IChartOfAccountsService _chartOfAccountsService;
        private readonly ILabelDataAccess _labelDataAccess;
        private readonly IDashboardDataAccess _dashboardDataAccess;
        private readonly IDateService _dateService;

        public DashboardService(
            IChartOfAccountsService chartOfAccountsService,
            IDashboardDataAccess dashboardDataAccess,
            ILabelDataAccess labelDataAccess,
            IDateService dateService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService configurationService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(loggedInUserService, configurationService, translationService, loggerFactory, mapper)
        {
            _chartOfAccountsService = chartOfAccountsService;
            this._dashboardDataAccess = dashboardDataAccess;
            _labelDataAccess = labelDataAccess;
            _dateService = dateService;
        }

        private double GetTotalWorkingHours(DateTime startDate, DateTime endDate, DefaultValue defaultValues)
           => _dateService.GetTotalWorkingHours(startDate, endDate, defaultValues.StartingHour, defaultValues.EndingHour);

        private IEnumerable<ExternalPartnerTransactionDetails> GetTopClients(IEnumerable<Invoice> invoices, IEnumerable<CreditNote> creditNotes, int totalTake = 10)
        {
            return invoices
                .GroupBy(e => e.ClientId)
                .Select(e =>
                {
                    var client = e.First().Client;
                    return new ExternalPartnerTransactionDetails
                    {
                        Id = client.Id,
                        Name = client.FullName,
                        Reference = client.Reference,
                        Total = e.Sum(i => i.OrderDetails.TotalTTC)
                    };
                })
                .Concat(creditNotes
                .GroupBy(e => e.ClientId)
                .Select(e =>
                {
                    var client = e.First().Client;
                    return new ExternalPartnerTransactionDetails
                    {
                        Id = client.Id,
                        Name = client.FullName,
                        Reference = client.Reference,
                        Total = e.Sum(i => i.OrderDetails.TotalTTC)
                    };
                }))
                .OrderBy(e => e.Total)
                .Take(totalTake);
        }

        private IEnumerable<TechniciansWorkingHoursDetails> GetTopTechnicians(MaintenanceContract[] contracts, DefaultValue defaultValues)
        {
            return contracts.SelectMany(contract => contract.MaintenanceOperationsSheets
                .Select(operationSheet => new
                {
                    ContractId = contract.Id,
                    OperationSheetId = operationSheet.Id,
                    Technician = Map<UserMinimalModel>(operationSheet.Technician),
                    TotalWorkingHours = GetTotalWorkingHours(operationSheet.StartDate, operationSheet.EndDate, defaultValues),
                }))
                .GroupBy(technicianDetails => technicianDetails.Technician)
                .Select(grouping => new TechniciansWorkingHoursDetails
                {
                    Technician = grouping.Key,
                    TotalWorkingHours = grouping.Sum(e => e.TotalWorkingHours),
                    OperationSheets = grouping.Select(e => e.OperationSheetId).Distinct(),
                    Contracts = grouping.Select(e => e.ContractId).Distinct(),
                });
        }

        private void SetTopSuppliersDetails(DashboardTopsDetailsModel result, Expense[] expenses, int topValue)
        {
            // extract the articles from the expenses
            var articleInfo = expenses.Where(x => x.Status != ExpenseStatus.Canceled && x.Status != ExpenseStatus.Draft)
                .SelectMany(e => e.OrderDetails
                    .GetAllProduct()
                    .Select(p => new
                    {
                        p.Product.Id,
                        p.Product.Name,
                        p.Product.Reference,
                        p.Product.Category,
                        Supplier = new ExternalPartnerProductsAnalytics
                        {
                            Quantity = p.Quantity,
                            Id = e.Supplier.Id,
                            Name = e.Supplier.FullName,
                            Reference = e.Supplier.Reference,
                            Price = p.Product.GetPrice(e.SupplierId),
                        },
                        ExpensesDetails = new MinimalDocumentModel
                        {
                            Id = e.Id,
                            Status = e.Status,
                            Reference = e.Reference,
                            TotalHT = e.GetTotalHT(),
                            TotalTTC = e.GetTotalTTC(),
                            DocumentType = e.DocumentType,
                        }
                    }));

            // article grouping
            var articleGrouping = articleInfo
                .GroupBy(e => e.Id)
                .Select(p =>
                {
                    var product = p.First();
                    return new ProductAnalyticsResult
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Reference = product.Reference,
                        Suppliers = p.Select(e => e.Supplier),
                        Classification = product.Category,
                    };
                });

            // top 10 suppliers
            result.TopSuppliers = articleInfo
                 .GroupBy(e => e.Supplier.Id)
                 .Select(g =>
                 {
                     var supplierInfo = g.First().Supplier;
                     return new SuppliersAnalyticsResult
                     {
                         ExpensesDetails = g.Select(e => e.ExpensesDetails),
                         Supplier = new ExternalPartnerMinimalInfo
                         {
                             Id = supplierInfo.Id,
                             Name = supplierInfo.Name,
                             Reference = supplierInfo.Reference,
                         },
                     };
                 })
                 .OrderByDescending(e => e.TotalTransaction)
                 .Take(topValue);

            // top products by quantity
            result.TopProductsByQuantity = articleGrouping
                 .OrderByDescending(e => e.CountOrders)
                 .Take(topValue);

            // top products by supplier
            result.TopProductsBySupplierPrice = articleGrouping
                 .OrderByDescending(e => e.Suppliers.Max(p => p.Price))
                 .Take(topValue);
        }

        private void SetTopProducts(DashboardTopsDetailsModel result, Invoice[] invoices, Label[] labels, int topValue)
        {
            // top clients
            var productDetails = invoices
                .SelectMany(e => e.OrderDetails
                    .GetAllProduct()
                    .Select(p => new
                    {
                        p.Product.Id,
                        p.Product.Name,
                        p.Product.Reference,
                        p.Product.Category,
                        Labels = labels.Where(l => l.Products.Any(pl => pl.ProductId == p.Product.Id))
                            .Select(l => new LabelModel
                            {
                                Id = l.Id,
                                Value = l.Value,
                            }),
                        Client = new ExternalPartnerProductsAnalytics
                        {
                            Id = e.Client.Id,
                            Quantity = p.Quantity,
                            Name = e.Client.FullName,
                            Price = p.Product.TotalTTC,
                            Reference = e.Client.Reference,
                        },
                        Document = new MinimalDocumentModel
                        {
                            Id = e.Id,
                            Status = e.Status,
                            Reference = e.Reference,
                            TotalTTC = e.GetTotalTTC(),
                            TotalHT = e.GetTotalHT(),
                            DocumentType = e.DocumentType,
                        }
                    }))
                .GroupBy(e => e.Id)
                .Select(g =>
                {
                    var product = g.First();
                    return new ProductAnalyticsResult
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Reference = product.Reference,
                        Classification = product.Category,
                        Labels = g.SelectMany(e => e.Labels).Distinct(),
                        Suppliers = g.Select(e => e.Client),
                    };
                });

            result.TopProductsByTotalSales = productDetails
                    .OrderByDescending(e => e.TotalOrders)
                    .Take(topValue);

            result.TopLabelsByTotalSales = productDetails
                .SelectMany(p => p.Labels.Select(l => new
                {
                    Label = l,
                    Product = p,
                }))
                .GroupBy(e => e.Label)
                .Select(e => new LabelAnalyticsResult
                {
                    Label = e.Key,
                    Total = e.Sum(p => p.Product.TotalOrders)
                })
                .OrderByDescending(e => e.Total)
                .Take(topValue);
        }

        private void SetTopWorkshopsDetails(DashboardTopsDetailsModel result, DefaultValue defaultValues, ConstructionWorkshop[] workshops, int topValue)
        {
            // get the workshop info
            var workshopInfo = workshops
                .Select(w => new MinimalWorkshopInfo
                {
                    Id = w.Id,
                    Name = w.Name,
                    Status = w.Status,
                    Turnover = AnalyticsService.GetTurnover(w.Invoices.ToArray(), w.CreditNotes.ToArray()),
                    Client = Map<ClientModel>(w.Client),
                    OperationSheets = w.OperationSheets
                        .Select(operationSheet => new OperationSheetsMinimalInfo
                        {
                            Id = operationSheet.Id,
                            Reference = operationSheet.Reference,
                            Status = operationSheet.Status,
                            WorkingHours = GetTotalWorkingHours(operationSheet.StartDate, operationSheet.EndDate, defaultValues),
                        }),
                })
                .ToList();

            result.TopWorkshopsByTurnover = workshopInfo
                .Where(e => e.Status == WorkshopStatus.Accepted || e.Status == WorkshopStatus.Finished)
                .OrderByDescending(e => e.Turnover)
                .Take(topValue);

            result.TopWorkshopsByWorkingHours = workshopInfo
                .Where(e => (e.Status == WorkshopStatus.Accepted || e.Status == WorkshopStatus.Finished) &&
                             e.OperationSheets.Any(o => o.Status == OperationSheetStatus.Billed || o.Status == OperationSheetStatus.Realized))
                .OrderByDescending(e => e.WorkingHours)
                .Take(topValue);
        }
    }
}
