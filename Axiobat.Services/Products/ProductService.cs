﻿namespace Axiobat.Services.Products
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Products;
    using AutoMapper;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implantation for <see cref="ProductService"/>
    /// </summary>
    public partial class ProductService : ProductsBaseService<Product>//, IProductService
    {
        /// <summary>
        /// check if the given reference is unique
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsRefrenceUniqueAsync(string reference)
            => !await _dataAccess.IsExistAsync(e => e.Reference == reference);

        /// <summary>
        /// check if the given category type id belongs to an existing category
        /// </summary>
        /// <param name="categoryTypeId">the id of the category to check</param>
        /// <returns>true if exist, false if not</ret
        public Task<bool> IsProductCategoryTypeExistAsync(string categoryTypeId)
            => _dataAccess.IsProductCategoryTypeExistAsync(categoryTypeId);

        public async Task<TOut[]> GetProductCategoriesTypeAsync<TOut>()
        {
            var categories = await _dataAccess.GetProductCategoriesTypeAsync();
            return _mapper.Map<TOut[]>(categories);
        }

        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                User = _loggedInUserService.GetMinimalUser(),
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memo.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memo.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memo.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memo.Remove(memo);

            var deleteResult = await _dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// get the list of products of the supplier
        /// </summary>
        /// <typeparam name="TOut">the out put type</typeparam>
        /// <param name="supplierId">the id of the supplier</param>
        /// <returns>the list of products</returns>
        public async Task<ListResult<TOut>> GetSupplierProductsAsync<TOut>(string supplierId)
        {
            var result = await _dataAccess.GetBySupplierIdAsync(supplierId);
            return Result.ListSuccess(Map<IEnumerable<TOut>>(result));
        }
    }

    /// <summary>
    /// partial part for <see cref="ProductService"/>
    /// </summary>
    public partial class ProductService : ProductsBaseService<Product>, IProductService
    {
        protected new IProductDataAccess _dataAccess => base._dataAccess as IProductDataAccess;

        public ProductService(
            IProductDataAccess dataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolverService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }


        protected override Task InCreate_BeforInsertAsync<TCreateModel>(Product product, TCreateModel createModel)
        {
            if (!(createModel is ProductPutModel model))
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(ProductPutModel));

            // set the list of the products labels
            product.Labels = model.Labels.Select(e =>
            {
                var productLabel = new ProductLabel
                {
                    ProductId = product.Id,
                    LabelId = e.Id,
                };

                if (!e.Id.IsValid())
                {
                    productLabel.LabelId = null;
                    productLabel.Label = new Label()
                    {
                        Value = e.Value,
                    };
                }

                return productLabel;
            }).ToList();

            // set the products suppliers
            product.Suppliers = model.ProductSuppliers.Select(e => new ProductSupplier
            {
                ProductId = product.Id,
                SupplierId = e.SupplierId,
                IsDefault = e.IsDefault,
                Price = e.Price,
            }).ToList();

            // all done
            return Task.CompletedTask;
        }

        protected override async Task InUpdate_AfterUpdateAsync<TUpdateModel>(Product product, TUpdateModel updateModel)
        {
            if (!(updateModel is ProductPutModel model))
                throw new ModelTypeIsNotValidException(nameof(updateModel), typeof(ProductPutModel));

            // get suppliers product details informations
            var supplierProdcuts = model.ProductSuppliers.Select(e => new ProductSupplier
            {
                ProductId = product.Id,
                SupplierId = e.SupplierId,
                IsDefault = e.IsDefault,
                Price = e.Price,
            });

            // insert it to database
            await _dataAccess.DeleteProductSuppliersAsync(product.Id);
            await _dataAccess.AddProductsSuppliersAsync(supplierProdcuts.ToArray());

            // get the product labels
            var productLables = model.Labels.Select(e =>
            {
                var productLabel = new ProductLabel
                {
                    ProductId = product.Id,
                    LabelId = e.Id,
                };

                if (!e.Id.IsValid())
                {
                    productLabel.LabelId = null;
                    productLabel.Label = new Label()
                    {
                        Value = e.Value,
                    };
                }

                return productLabel;
            });

            // insert the labels
            await _dataAccess.DeleProductLabelsAsync(product.Id);
            await _dataAccess.AddProductLabelsAsync(productLables.ToArray());
        }

        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(Product product, TUpdateModel updateModel)
        {
            if (!(updateModel is ProductPutModel))
                throw new ModelTypeIsNotValidException(nameof(updateModel), typeof(ProductPutModel));

            product.Classification = null;
            product.ProductCategoryType = null;

            return base.InUpdate_BeforUpdateAsync(product, updateModel);
        }
    }
}
