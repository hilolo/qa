﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="Group"/>
    /// </summary>
    [ModelFor(typeof(Group))]
    public partial class GroupModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// name of the group
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// list of the clients associated with the groupe
        /// </summary>
        public virtual ICollection<ClientModel> Clients { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="GroupModel"/>
    /// </summary>
    public partial class GroupModel : IModel<Group, int>
    {
        /// <summary>
        /// create an instant of <see cref="GroupModel"/>
        /// </summary>
        public GroupModel()
        {
            Clients = new HashSet<ClientModel>();
        }
    }
}
