﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the base module for <see cref="ClientModel"/> and <see cref="SupplierModel"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity</typeparam>
    [ModelFor(typeof(ExternalPartner))]
    public partial class ExternalPartnerModel<TEntity>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the supplier reference, should be unique in every company database only
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// first name of the client
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// last name of the client
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// the client full name
        /// </summary>
        [Newtonsoft.Json.JsonProperty("Name")]
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the Fax
        /// </summary>
        public string LandLine { get; set; }

        /// <summary>
        /// the email of the client, should be in a proper email format but is not required
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// client website
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Directory Identification System institutions
        /// </summary>
        public string Siret { get; set; }

        /// <summary>
        /// Intra-community VAT means value added tax (VAT) applied to commercial transactions between different countries of the European Union.
        /// for more informations <see href="https://debitoor.fr/termes-comptables/tva-intracommunautaire">HERE</see>
        /// </summary>
        public string IntraCommunityVAT { get; set; }

        /// <summary>
        /// the accounting identifier of the client
        /// </summary>
        public string AccountingCode { get; set; }

        /// <summary>
        /// the list of contacts for this supplier
        /// </summary>
        /// <remarks>
        /// this is JSON Object
        /// </remarks>
        public ICollection<ContactInformation> ContactInformations { get; set; }

        /// <summary>
        /// the history of changes
        /// </summary>
        public ICollection<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// memos of the supplier
        /// </summary>
        public IList<Memo> Memos { get; set; }

        /// <summary>
        /// the conditions of the note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// the conditions of the payment
        /// </summary>
        public string PaymentCondition { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="ExternalPartnerModel{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity, must be a <see cref="ContactBase{TEntity}"/></typeparam>
    public partial class ExternalPartnerModel<TEntity> : IModel<TEntity>
        where TEntity : ExternalPartner
    {
        /// <summary>
        /// the default constructor
        /// </summary>
        public ExternalPartnerModel()
        {
            ContactInformations = new HashSet<ContactInformation>();
            ChangesHistory = new HashSet<ChangesHistory>();
            Memos = new List<Memo>();
        }
    }
}
