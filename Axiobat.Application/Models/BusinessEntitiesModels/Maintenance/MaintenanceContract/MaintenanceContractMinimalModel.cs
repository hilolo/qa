﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// minimal model for <see cref="MaintenanceContract"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceContract))]
    public partial class MaintenanceContractMinimalModel
    {
        /// <summary>
        /// the id of the model
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the reference for the entity
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the contract
        /// </summary>
        public string Status { get; set; }
    }
}
