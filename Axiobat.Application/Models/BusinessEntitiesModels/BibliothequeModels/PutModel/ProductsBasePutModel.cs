﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the model for the products
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    [ModelFor(typeof(ProductsBase))]
    public class ProductsBasePutModel<TEntity> : IUpdateModel<TEntity>
        where TEntity : ProductsBase
    {
        /// <summary>
        /// the Designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public virtual void Update(TEntity entity)
        {
            entity.Designation = Designation;
            entity.Description = Description;
        }
    }
}
