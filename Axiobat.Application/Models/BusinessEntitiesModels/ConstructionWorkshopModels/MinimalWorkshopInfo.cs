﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Constants;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// a minimal version of the workshop
    /// </summary>
    [DocType(DocumentType.Workshop)]
    public partial class MinimalWorkshopInfo
    {
        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the construction workshop status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// name of the construction workshop
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the workshop client
        /// </summary>
        public ClientModel Client { get; set; }

        /// <summary>
        /// the workshop turn over
        /// </summary>
        public float Turnover { get; set; }

        /// <summary>
        /// the total working hours on the workshop
        /// </summary>
        public double WorkingHours => GetTotalWorkingHours();

        /// <summary>
        /// list of operation sheets associated with the workshop
        /// </summary>
        public IEnumerable<OperationSheetsMinimalInfo> OperationSheets { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MinimalWorkshopInfo"/>
    /// </summary>
    public partial class MinimalWorkshopInfo
    {
        /// <summary>
        /// create an instant of <see cref="MinimalWorkshopInfo"/>
        /// </summary>
        public MinimalWorkshopInfo()
        {
            OperationSheets = new HashSet<OperationSheetsMinimalInfo>();
        }

        /// <summary>
        /// calculate the Total Working Hours from the list of operation sheets
        /// </summary>
        /// <returns>the total working hours</returns>
        private double GetTotalWorkingHours()
        {
            return OperationSheets.Where(o => o.Status == OperationSheetStatus.Billed || o.Status == OperationSheetStatus.Realized)
                .Sum(e => e.WorkingHours);
        }
    }

    public partial class OperationSheetsMinimalInfo
    {
        public string Id { get; set; }

        public string Status { get; set; }

        public string Reference { get; set; }

        public double WorkingHours { get; set; }

        public MinimalWorkshopInfo Workshops { get; set; }
    }
}
