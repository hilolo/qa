﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="ConstructionWorkshop"/>
    /// </summary>
    [ModelFor(typeof(ConstructionWorkshop))]
    public partial class ConstructionWorkshopPutModel : IUpdateModel<ConstructionWorkshop>
    {
        /// <summary>
        /// name of the construction workshop
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// a description of the construction workshop
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// a Comment about the construction workshop
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the total hours spent on this workshop
        /// </summary>
        public int TotalHours { get; set; }

        /// <summary>
        /// the total amount spent on the workshop
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// the id of the client that owns this workshop
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the construction workshop status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Progress Rate on the workshop
        /// </summary>
        public int ProgressRate { get; set; }

        /// <summary>
        /// list of rubrics to add
        /// </summary>
        public ICollection<string> Rubrics { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ConstructionWorkshopPutModel"/>
    /// </summary>
    public partial class ConstructionWorkshopPutModel
    {
        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(ConstructionWorkshop entity)
        {
            entity.Name = Name;
            entity.Description = Description;
            entity.Comment = Comment;
            entity.TotalHours = TotalHours;
            entity.Amount = Amount;
            entity.ClientId = ClientId;
            entity.Status = Status;
            entity.ProgressRate = ProgressRate;

        }
    }
}
