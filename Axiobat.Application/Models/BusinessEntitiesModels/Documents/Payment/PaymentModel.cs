﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Payment"/>
    /// </summary>
    [ModelFor(typeof(Payment))]
    public partial class PaymentModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// a description associated wit the payment
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the type of the payment operation
        /// </summary>
        public PaymentOperation Operation { get; set; }

        /// <summary>
        /// the type of this payment
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// the date of the payment
        /// </summary>
        public DateTime DatePayment { get; set; }

        /// <summary>
        /// the payment amount
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// the credit note associated with this payment
        /// </summary>
        public MinimalDocumentModel CreditNote { get; set; }

        /// <summary>
        /// the account that the payment has been made with it
        /// </summary>
        public FinancialAccountModel Account { get; set; }

        /// <summary>
        /// the payment method associated with payment
        /// </summary>
        public PaymentMethodModel PaymentMethod { get; set; }

        /// <summary>
        /// the list of changes history
        /// </summary>
        public ICollection<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// list of associated documents
        /// </summary>
        public ICollection<AssociatedDocument> AssociatedDocuments { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="PaymentModel"/>
    /// </summary>
    public partial class PaymentModel : IModel<Payment>
    {
        /// <summary>
        /// create an instant of <see cref="PaymentModel"/>
        /// </summary>
        public PaymentModel()
        {
            ChangesHistory = new List<ChangesHistory>();
            AssociatedDocuments = new HashSet<AssociatedDocument>();
        }
    }
}
