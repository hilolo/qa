﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;

    [ModelFor(typeof(ProductCategoryType))]
    public class ProductCategoryTypeModel : IModel<ProductCategoryType>
    {
        /// <summary>
        /// the id of the category
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// a label for the category type
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// a description for the product category type
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// type of the product category
        /// </summary>
        public CategoryType Type { get; set; }

        /// <summary>
        /// the implicit conversion between the <see cref="ProductCategoryType"/> and <see cref="ProductCategoryType"/>
        /// </summary>
        /// <param name="categoryType">the <see cref="ProductCategoryType"/> instant</param>
        public static implicit operator ProductCategoryTypeModel(ProductCategoryType categoryType)
            => categoryType is null ? null : new ProductCategoryTypeModel
            {
                Id = categoryType.Id,
                Type = categoryType.Type,
                Label = categoryType.Label,
                Description = categoryType.Description,
            };
    }
}
