﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the memo model
    /// </summary>
    [ModelFor(typeof(Memo))]
    public class MemoModel
    {
        /// <summary>
        /// the default constructor
        /// </summary>
        public MemoModel()
        {
            Attachments = new HashSet<AttachmentModel>();
        }

        /// <summary>
        /// the type of the document this memo is for it
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the id of the <see cref="Memo"/>
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the comment associated with the Memo
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the date this memo has been issued
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// the list of Attachments
        /// </summary>
        public ICollection<AttachmentModel> Attachments { get; set; }

        /// <summary>
        /// the string reApplication of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"Attachments count: {Attachments.Count}";
    }

    /// <summary>
    /// <see cref="Memo"/> delete model
    /// </summary>
    [ModelFor(typeof(Memo))]
    public class DeleteMemoModel
    {
        /// <summary>
        /// the type of the document this memo is for it
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// list of the <see cref="Memo"/> identifiers
        /// </summary>
        public ICollection<string> MemosIds { get; set; }
    }
}
