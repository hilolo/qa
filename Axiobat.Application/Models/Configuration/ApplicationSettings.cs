﻿namespace Axiobat
{
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// this class hold the global application settings extracted from <see cref="IConfiguration"/> in the appSettings.json file
    /// any changes to the AppSetting file should be applied here also
    /// </summary>
    public class ApplicationSettings
    {
        /// <summary>
        /// create an instant of <see cref="ApplicationSettings"/>
        /// </summary>
        public ApplicationSettings()
        {
            ApplicationsUrls = new Dictionary<string, string>();
        }

        /// <summary>
        /// a unique identifier that identifies the project
        /// </summary>
        public string ProjectId { get; set; } = "7DFA3AD5-2F6D-4D4A-BD77-2DFE84CF60EC";

        /// <summary>
        /// file output path
        /// </summary>
        public string FileOutputPath { get; set; }

        /// <summary>
        /// the Authentication Options
        /// </summary>
        public AuthenticationOptions AuthenticationOptions { get; set; }

        /// <summary>
        /// the shard services configurations
        /// </summary>
        public SharedServicesConfiguration SharedServicesConfigurations { get; set; }

        /// <summary>
        /// the options related the one signal service
        /// </summary>
        public OneSignalOptions OneSignal { get; set; }

        /// <summary>
        /// the publishing settings
        /// </summary>
        public Publishing Publishing { get; set; }

        /// <summary>
        /// the list of application related URLs
        /// </summary>
        public Dictionary<string, string> ApplicationsUrls { get; set; }

        /// <summary>
        /// a flag to stop the background process from running
        /// </summary>
        public bool AllowBackgroundServices { get; set; }
    }

    /// <summary>
    /// the Authentication Options
    /// </summary>
    public class AuthenticationOptions
    {
        /// <summary>
        /// the token issuer
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// the Audience
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// the expiration value in minutes for the authentication token
        /// </summary>
        public int AuthenticationTokenExpiration { get; set; }

        /// <summary>
        /// Generates a random value (nonce) for each generated token.
        /// </summary>
        /// <remarks>The default nonce is a random GUID.</remarks>
        public static Func<Task<string>> NonceGenerator { get; set; }
            = () => Task.FromResult(Guid.NewGuid().ToString());

        /// <summary>
        /// the SignInKey value, this only used to pass the option to the authConfigator, should not be set in the application settings
        /// </summary>
        public string SignInKey { get; set; }
    }

    /// <summary>
    /// the configuration for the shared services
    /// </summary>
    public class SharedServicesConfiguration
    {
        /// <summary>
        /// the file service URL
        /// </summary>
        public string FileServiceURL { get; set; }

        /// <summary>
        /// the email service URL
        /// </summary>
        public string EmailServiceURL { get; set; }
    }

    /// <summary>
    /// the OneSignal options
    /// </summary>
    public class OneSignalOptions
    {
        public string APIKey { get; set; }
        public string AppId { get; set; }
        public string BaseUrl { get; set; }
    }

    /// <summary>
    /// a class define directory of publishing module
    /// </summary>
    public partial class Publishing
    {
        /// <summary>
        /// create an instant of <see cref="Publishing"/>
        /// </summary>
        public Publishing()
        {
        }

        private string _files;
        /// <summary>
        /// the path when we save contracts
        /// </summary>
        public string Files
        {
            get => _files;
            set
            {
                if (!Directory.Exists(value))
                    Directory.CreateDirectory(value);
                _files = value;
            }
        }
    }
}
