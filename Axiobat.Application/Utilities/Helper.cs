﻿namespace Axiobat
{
    using App.Common;
    using Application.Services.Contacts;
    using Application.Services;
    using Application.Services.AccountManagement;
    using Application.Services.Documents;
    using Application.Services.Maintenance;
    using Application.Services.Products;
    using Domain.Enums;
    using Domain.Entities;
    using System;
    using Application.Services.Configuration;
    using Axiobat.Application.Services.Mission;

    /// <summary>
    /// a helper to do type related information
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// get the type of the entity base on the given string id
        /// </summary>
        /// <param name="id">the id to extract the type from it</param>
        /// <returns>the entity type</returns>
        public static Type GetEntityType(string id)
            => GetEntityType(EntityId.Get(id).DocType);

        /// <summary>
        /// get the type of the entity base on the given docType
        /// </summary>
        /// <param name="docType">the docType of the entity</param>
        /// <returns>the entity type</returns>
        public static Type GetEntityType(DocumentType docType)
        {
            switch (docType)
            {
                case DocumentType.Lot: return typeof(Lot);
                case DocumentType.Agent: return typeof(User);
                case DocumentType.Quote: return typeof(Quote);
                case DocumentType.Client: return typeof(Client);
                case DocumentType.Invoice: return typeof(Invoice);
                case DocumentType.Payment: return typeof(Payment);
                case DocumentType.Product: return typeof(Product);
                case DocumentType.Mission: return typeof(Mission);
                case DocumentType.Supplier: return typeof(Supplier);
                case DocumentType.Expenses: return typeof(Expense);
                case DocumentType.CreditNote: return typeof(CreditNote);
                case DocumentType.SupplierOrder: return typeof(SupplierOrder);
                case DocumentType.OperationSheet: return typeof(OperationSheet);
                case DocumentType.Classification: return typeof(Classification);
                case DocumentType.Workshop: return typeof(ConstructionWorkshop);
                case DocumentType.MaintenanceVisit: return typeof(MaintenanceVisit);
                case DocumentType.RecurringDocument: return typeof(RecurringDocument);
                case DocumentType.PublishingContract: return typeof(PublishingContract);
                case DocumentType.Configuration: return typeof(ApplicationConfiguration);
                case DocumentType.MaintenanceContract: return typeof(MaintenanceContract);
                case DocumentType.EquipmentMaintenance: return typeof(EquipmentMaintenance);
                case DocumentType.MaintenanceOperationSheet: return typeof(MaintenanceOperationSheet);
                case DocumentType.Undefined:
                default: return null;
            }
        }

        /// <summary>
        /// get the service the correspond to the
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public static Type GetEntityService(this Type entityType)
        {
            if (entityType is null)
                return null;

            Type service = null;
            new TypeSwitch()
                .Case<Lot>(() => service = typeof(ILotService))
                .Case<Quote>(() => service = typeof(IQuoteService))
                .Case<Client>(() => service = typeof(IClientService))
                .Case<Invoice>(() => service = typeof(IInvoiceService))
                .Case<Expense>(() => service = typeof(IExpenseService))
                .Case<Payment>(() => service = typeof(IPaymentService))
                .Case<Product>(() => service = typeof(IProductService))
                .Case<Mission>(() => service = typeof(IMissionService))
                .Case<Supplier>(() => service = typeof(ISupplierService))
                .Case<User>(() => service = typeof(IUsersManagementService))
                .Case<CreditNote>(() => service = typeof(ICreditNoteService))
                .Case<SupplierOrder>(() => service = typeof(ISupplierOrderService))
                .Case<OperationSheet>(() => service = typeof(IOperationSheetService))
                .Case<Classification>(() => service = typeof(IClassificationService))
                .Case<PublishingContract>(() => service = typeof(IPublishingService))
                .Case<RecurringDocument>(() => service = typeof(IRecurringDocumentService))
                .Case<MaintenanceContract>(() => service = typeof(IMaintenanceContractService))
                .Case<ConstructionWorkshop>(() => service = typeof(IConstructionWorkshopService))
                .Case<EquipmentMaintenance>(() => service = typeof(IEquipmentMaintenanceService))
                .Case<ApplicationConfiguration>(() => service = typeof(IApplicationConfigurationService))
                .Case<MaintenanceOperationSheet>(() => service = typeof(IMaintenanceOperationSheetService))
                .Switch(entityType);

            return service;
        }
    }
}
