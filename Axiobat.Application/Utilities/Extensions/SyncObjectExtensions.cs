﻿namespace Axiobat.Application
{
    using Enums;
    using Models;
    using System.Collections.Generic;
    using System.Linq;

    public static class SyncObjectExtensions
    {
        public static IEnumerable<TEntity> EntitiesToAdd<TEntity>(this IEnumerable<SyncObject> objectsToSync)
            => objectsToSync.Where(e => e.Status == SynchronizationStatus.Added)
                .Select(e => e.Object.As<TEntity>());

        public static IEnumerable<TEntity> EntitiesToUpdate<TEntity>(this IEnumerable<SyncObject> objectsToSync)
            => objectsToSync.Where(e => e.Status == SynchronizationStatus.Updated)
                .Select(e => e.Object.As<TEntity>());
    }
}
