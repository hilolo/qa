﻿namespace Axiobat.Application.Models
{
    using Domain.Interfaces;

    /// <summary>
    /// this class define a model that is an update model
    /// all Update Model should have an Update() method
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity</typeparam>
    public interface IUpdateModel<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        /// update the entity form current update model instant
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);
    }
}
