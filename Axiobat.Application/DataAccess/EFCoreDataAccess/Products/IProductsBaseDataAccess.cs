﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// data access for Products Based entities
    /// </summary>
    public interface IProductsBaseDataAccess<TEntity> : IDataAccess<TEntity, string>
        where TEntity : ProductsBase
    {

    }
}
