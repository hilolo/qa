﻿namespace Axiobat.Application.Services.AccountManagement
{
    using Configuration;
    using Domain.Entities;
    using Models;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the User management service, this interface defines the necessary methods to perform CRUD operation on the
    /// <see cref="User"/> entity
    /// </summary>
    public interface IUsersManagementService : IDataService<User, Guid>, IReferenceService, ISynchronize
    {
        /// <summary>
        /// check if the user name is unique
        /// </summary>
        /// <param name="userName">the user name to check</param>
        /// <returns>true if unique false if not</returns>
        Task<bool> IsUserNameExistAsync(string userName);

        /// <summary>
        /// check if the user email exist or not
        /// </summary>
        /// <param name="email">the user email</param>
        /// <returns>true if valid, false if not</returns>
        Task<bool> IsUserEmailExistAsync(string email);

        /// <summary>
        /// validate the login of the user
        /// </summary>
        /// <param name="loginModel">the login model</param>
        /// <returns>validation result</returns>
        Task<User> ValidateUserLoginAsync(UserLoginModel loginModel);

        /// <summary>
        /// send the confirmation email to the given user email
        /// </summary>
        /// <param name="email">the email of the user</param>
        /// <returns></returns>
        Task SendEmailConfirmationAsync(string email);

        /// <summary>
        /// confirm the user email
        /// </summary>
        /// <param name="model">the email confirmation model</param>
        /// <returns></returns>
        Task ConfirmUserEmailAsync(UserConfirmEmailModel model);

        /// <summary>
        /// update user password
        /// </summary>
        /// <param name="updatePasswordModel">the update password model</param>
        /// <returns>the operation result</returns>
        Task UpdateUserPassword(UserUpdatePasswordModel updatePasswordModel);

        /// <summary>
        /// handle forget password operation
        /// </summary>
        /// <param name="forgetPasswordModel">the forget password model</param>
        Task HandleForgetPasswordAsync(UserForgetPasswordModel forgetPasswordModel);

        /// <summary>
        /// retrieve list of users by role types using the given model
        /// </summary>
        /// <typeparam name="TOut">the out put type</typeparam>
        /// <param name="model">the model</param>
        /// <returns>the list of users</returns>
        Task<ListResult<TOut>> GetByRoleTypeAsync<TOut>(UserRetrievealByRoleType model);

        /// <summary>
        /// rest user password
        /// </summary>
        /// <param name="restPasswordModel">rest password model</param>
        Task RestPasswordAsync(UserRestPasswordModel restPasswordModel);

        /// <summary>
        /// update the password of the user
        /// </summary>
        /// <param name="model">the model user for updating the user</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateUserPassword(UpdateUserPasswordModel model);

        /// <summary>
        /// update the last time the user has logged in
        /// </summary>
        /// <param name="id">the id of the user</param>
        Task UpdateLastTimeLoggedInAsync(Guid id);

        /// <summary>
        /// get Admin User
        /// </summary>
        /// <param name="id">the id of the user</param>
        Task<User> GetAdminUserAsync();
    }
}