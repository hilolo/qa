﻿namespace Axiobat.Application.Services.Configuration
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for interacting with the Google calendar service
    /// </summary>
    public interface IGoogleCalendarService
    {
        /// <summary>
        /// add a new event to Google calendar
        /// </summary>
        /// <param name="documentType">the type of the document</param>
        /// <param name="reference">the document reference</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <returns>the generated code from Google calendar</returns>
        Task<string> RegisterEventAsync(string documentType, string reference, DateTime startDate, DateTime endDate);

        /// <summary>
        /// update Google calendar event with the given id
        /// </summary>
        /// <param name="eventId">the Google Calendar event Id</param>
        /// <param name="documentType">the type of the document</param>
        /// <param name="reference">the document reference</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <returns>the code from Google calendar</returns>
        Task<string> UpdateEventAsync(string eventId, string documentType, string reference, DateTime startDate, DateTime endDate);

        /// <summary>
        /// delete the event with the given id
        /// </summary>
        /// <param name="eventId">the id of the Google calendar event to be deleted</param>
        Task DeleteEventAsync(string eventId);
    }
}
