﻿namespace Axiobat.Application.Services.Mission
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Mission"/>
    /// </summary>
    public interface IMissionService : IDataService<Mission, string>
    {
        /// <summary>
        /// get paged result of the missions types
        /// </summary>
        /// <param name="filterOptions">the filter option to be used for retrieving the list of mission types</param>
        /// <returns>the paged result of the mission types</returns>
        Task<PagedResult<MissionTypeModel>> GetMissionTypesAsync(MissionTypeFilterOptions filterOptions);

        /// <summary>
        /// create or update a mission type, based on the given model
        /// </summary>
        /// <param name="model">the model to be used when creating or updating a mission type</param>
        /// <returns>the updated/created mission type</returns>
        Task<Result<MissionTypeModel>> PutMissionTypeAsync(MissionTypeModel model);

        /// <summary>
        /// delete the mission type with the given id
        /// </summary>
        /// <param name="missionTypeId">the id of the mission type</param>
        Task DeleteMissionTypeAsync(int missionTypeId);
    }
}
