﻿namespace Axiobat.Application.Services.Products
{
    using Application.Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the <see cref="Lot"/> service
    /// </summary>
    public interface ILotService : IProductBaseService<Lot>
    {
        /// <summary>
        /// get list of products of the model
        /// </summary>
        /// <param name="lotId">the id of the lot</param>
        /// <returns>list of products</returns>
        Task<ListResult<ProductModel>> GetLotProductsAsync(string lotId);

        /// <summary>
        /// check if the given lot name is unique or not
        /// </summary>
        /// <param name="name">the name of the lot to be checked</param>
        /// <returns>true if unique false if not</returns>
        Task<bool> IsNameUniqueAsync(string name);
    }
}
