﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the informations about sending a document in an email
    /// </summary>
    public partial class DocumentEmail
    {
        /// <summary>
        /// the id of the email
        /// </summary>
        public string MailId { get; set; }

        /// <summary>
        /// the emails that the document has been sent to.
        /// </summary>
        public ICollection<string> To { get; set; }

        /// <summary>
        /// the email subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// the content of the email
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// the date that the email has been sent on
        /// </summary>
        public DateTime SentOn { get; set; }

        /// <summary>
        /// the id of the user who sent the email
        /// </summary>
        public MinimalUser User { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="DocumentEmail"/>
    /// </summary>
    public partial class DocumentEmail
    {
        /// <summary>
        /// create an instant of <see cref="DocumentEmail"/>
        /// </summary>
        public DocumentEmail()
        {
            To = new HashSet<string>();
        }

        /// <summary>
        /// get string representation of object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"to: {To}, by: {User.UserName}, on: {SentOn.ToLongDateString()}";
    }
}
