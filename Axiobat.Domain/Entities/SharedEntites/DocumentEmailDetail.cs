﻿namespace Axiobat.Domain.Entities
{
    using Enums;

    /// <summary>
    /// hold information about the email sent
    /// </summary>
    public partial class DocumentEmailDetail
    {
        /// <summary>
        /// the id of the mail
        /// </summary>
        public string _id { get; set; }

        /// <summary>
        /// the user who sent this email
        /// </summary>
        public MinimalUser Utilisateur { get; set; }

        /// <summary>
        /// the date the email has been sent
        /// </summary>
        public string DateAction { get; set; }

        /// <summary>
        /// the type of the action
        /// </summary>
        public int Action { get; set; }

        /// <summary>
        /// the <see cref="EmailStatus"/>
        /// </summary>
        public EmailStatus Status { get; set; }

        /// <summary>
        /// the email data
        /// </summary>
        public DocumentEmailData Data { get; set; }
    }
}
