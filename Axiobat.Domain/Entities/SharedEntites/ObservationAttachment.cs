﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the attachment type for <see cref="TechnicianObservation"/>
    /// </summary>
    public partial class ObservationAttachment : Attachment
    {
        /// <summary>
        /// the base 64 file content
        /// </summary>
        public string Base64 { get; set; }
    }
}
