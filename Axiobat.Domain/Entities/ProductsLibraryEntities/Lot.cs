﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Enums;
    using System.Collections.Generic;

    /// <summary>
    /// a class that defines the a Lot entity
    /// </summary>
    [DocType(DocumentType.Lot)]
    public partial class Lot
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.Lot;
        /// <summary>
        /// list of elements
        /// </summary>
        public ICollection<LotProduct> Products { get; set; }
    }

    /// <summary>
    /// the partial part of <see cref="Lot"/>
    /// </summary>
    public partial class Lot : ProductsBase
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public Lot() : base()
        {
            Products = new HashSet<LotProduct>();
        }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Designation}";
    }
}
