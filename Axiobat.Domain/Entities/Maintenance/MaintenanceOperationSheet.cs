﻿namespace Axiobat.Domain.Entities
{
    using Interfaces;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class describe the operation sheet Maintenance entity
    /// </summary>
    [DocType(DocumentType.MaintenanceOperationSheet)]
    public partial class MaintenanceOperationSheet
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.MaintenanceOperationSheet;

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the Maintenance Operation Sheet, one of the values of <see cref="Constants.MaintenanceOperationSheetStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the Report associated with the operation sheet
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the type of the Maintenance OperationSheet
        /// </summary>
        public MaintenanceOperationSheetType Type { get; set; }

        /// <summary>
        /// the count of the Visits the technician has made to the client
        /// </summary>
        public int VisitsCount { get; set; }

        /// <summary>
        /// the total Basket consumption the technician has made
        /// </summary>
        public int TotalBasketConsumption { get; set; }

        /// <summary>
        /// apply the logical delete of the entity
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// the request for creating a quote
        /// </summary>
        public string QuoteRequest { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the Signature of the client
        /// </summary>
        public Signature ClientSignature { get; set; }

        /// <summary>
        /// the technicien signature
        /// </summary>
        public Signature TechnicianSignature { get; set; }

        /// <summary>
        /// the information of the client associated with this MaintenanceOperationSheet
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the Technician
        /// </summary>
        public Guid? TechnicianId { get; set; }

        /// <summary>
        /// the Technician associated with this Operation Sheet Maintenance
        /// </summary>
        public User Technician { get; set; }

        /// <summary>
        /// the invoice associated with the FIM
        /// </summary>
        public Invoice Invoice { get; set; }

        /// <summary>
        /// the Maintenance Contract associated with this operation sheet
        /// </summary>
        public string MaintenanceContractId { get; set; }

        /// <summary>
        /// the Maintenance visit associated with this operation sheet
        /// </summary>
        public string MaintenanceVisitId { get; set; }

        /// <summary>
        /// the Maintenance Contract associated with this visit
        /// </summary>
        public MaintenanceContract MaintenanceContract { get; set; }

        /// <summary>
        /// the Maintenance Visit related to this OperationSheet
        /// </summary>
        public MaintenanceVisit MaintenanceVisit { get; set; }

        /// <summary>
        /// the Data sheets associated with the OperationSheet
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// list of Technician Observation
        /// </summary>
        public ICollection<TechnicianObservation> Observations { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// the equipments associated with this Maintenance Operation Sheet, only check for this if the <see cref="Type"/> is <see cref="MaintenanceOperationSheetType.Maintenance"/>
        /// </summary>
        public ICollection<MaintenanceContractEquipmentDetails> EquipmentDetails { get; set; }

        /// <summary>
        /// the list of UnDone motifs
        /// </summary>
        public ICollection<MaintenanceOperationSheetMotif> UnDoneMotif { get; set; }

        /// <summary>
        /// the order details associated with this Maintenance Operation Sheet, only check for this if the <see cref="Type"/> is <see cref="MaintenanceOperationSheetType.AfterSalesService"/>
        /// </summary>
        public OrderDetails OrderDetails { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceOperationSheet"/>
    /// </summary>
    public partial class MaintenanceOperationSheet : Entity<string>, IReferenceable<MaintenanceOperationSheet>, IRecordable, IDeletable
    {
        /// <summary>
        /// partial part for <see cref="MaintenanceOperationSheet"/>
        /// </summary>
        public MaintenanceOperationSheet()
        {
            Id = GenerateId();
            Memos = new HashSet<Memo>();
            Emails = new HashSet<DocumentEmail>();
            ChangesHistory = new List<ChangesHistory>();
            Observations = new HashSet<TechnicianObservation>();
            UnDoneMotif = new HashSet<MaintenanceOperationSheetMotif>();
            EquipmentDetails = new HashSet<MaintenanceContractEquipmentDetails>();
        }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Client.FullName} {Purpose} {Reference}".ToLower();

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        public bool CanIncrementOnCreate() => true;

        public bool CanIncrementOnUpdate(MaintenanceOperationSheet oldState) => false;

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return OrderDetails?.TotalHT ?? 0;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            return OrderDetails?.GetTaxDetails().Sum(e => e.TotalTax) ?? 0;
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
        {
            return OrderDetails?.TotalTTC ?? 0;
        }
    }
}
