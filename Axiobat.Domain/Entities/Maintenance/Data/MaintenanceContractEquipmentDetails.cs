﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// a minimal version of the <see cref="EquipmentMaintenance"/> entity
    /// </summary>
    public partial class MaintenanceContractEquipmentDetails
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the name of the Equipment
        /// </summary>
        public string EquipmentName { get; set; }

        /// <summary>
        /// equipment model
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// the band of the equipment
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// serial number of the equipment
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// IdMaterial of the equipment
        /// </summary>
        public string IdMaterial { get; set; }

        /// <summary>
        /// the installation date of the equipment
        /// </summary>
        public DateTime? InstallationDate { get; set; }

        /// <summary>
        /// list of the equipment operations
        /// </summary>
        public ICollection<EquipmentMaintenanceOperation> MaintenanceOperations { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceContractEquipmentDetails"/>
    /// </summary>
    public partial class MaintenanceContractEquipmentDetails : IEquatable<MaintenanceContractEquipmentDetails>
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceContractEquipmentDetails"/>
        /// </summary>
        public MaintenanceContractEquipmentDetails()
        {
            Id = Guid.NewGuid().ToString();
            MaintenanceOperations = new HashSet<EquipmentMaintenanceOperation>();
        }

        /// <summary>
        /// check if the given object equals this current instant
        /// </summary>
        /// <param name="obj">the object to be checked</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
            => Equals(obj as MaintenanceContractEquipmentDetails);

        /// <summary>
        /// check if the given object equals this current instant
        /// </summary>
        /// <param name="other">the object to be checked</param>
        /// <returns>true if equals, false if not</returns>
        public bool Equals(MaintenanceContractEquipmentDetails other)
            => other != null && Id == other.Id && EquipmentName == other.EquipmentName;

        /// <summary>
        /// get the has code value of the object
        /// </summary>
        /// <returns>the hash code</returns>
        public override int GetHashCode()
        {
            int hashCode = 1947556724;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EquipmentName);
            return hashCode;
        }

        /// <summary>
        /// build the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Equipment {EquipmentName} has {MaintenanceOperations.Count} maintenance operations";

        /// <summary>
        /// check if the Equipment has any operation in the given month
        /// </summary>
        /// <param name="mount">the month to be checked</param>
        /// <returns>true if exist false if not</returns>
        public bool HasOperationIn(int mount)
            => MaintenanceOperations.Any(e => e.HasOperationIn(mount));

        public static bool operator ==(MaintenanceContractEquipmentDetails left, MaintenanceContractEquipmentDetails right) => EqualityComparer<MaintenanceContractEquipmentDetails>.Default.Equals(left, right);
        public static bool operator !=(MaintenanceContractEquipmentDetails left, MaintenanceContractEquipmentDetails right) => !(left == right);

        /// <summary>
        /// the implicit conversion between the <see cref="EquipmentMaintenance"/> and <see cref="MaintenanceContractEquipmentDetails"/>
        /// </summary>
        /// <param name="client">the <see cref="Client"/> instant</param>
        public static implicit operator MaintenanceContractEquipmentDetails(EquipmentMaintenance equipmentMaintenance)
            => equipmentMaintenance is null ? null : new MaintenanceContractEquipmentDetails
            {
                Id = equipmentMaintenance.Id,
                EquipmentName = equipmentMaintenance.EquipmentName,
                MaintenanceOperations = equipmentMaintenance.MaintenanceOperations,
            };

        /// <summary>
        /// generate a new instant of this <see cref="MaintenanceContractEquipmentDetails"/>
        /// with the <see cref="MaintenanceOperations"/> set to the given month
        /// </summary>
        /// <param name="month">the month to set the Periodicity of the MaintenanceOperations to it</param>
        /// <returns>the new instant of the object</returns>
        public MaintenanceContractEquipmentDetails GenerateFor(int month)
        {
            var result = new MaintenanceContractEquipmentDetails
            {
                Id = Id,
                Brand = Brand,
                Model = Model,
                SerialNumber = SerialNumber,
                EquipmentName = EquipmentName,
                InstallationDate = InstallationDate,
            };

            foreach (var operation in MaintenanceOperations
                .Where(e => (e.Periodicity.Contains((Month)month) && !e.SubOperations.Any()) ||
                            (!e.Periodicity.Any() && e.SubOperations.Any(s => s.HasOperationIn(month)))))
            {
                var equipmentMaintenanceOperation = new EquipmentMaintenanceOperation
                {
                    Id = operation.Id,
                    Name = operation.Name,
                    Periodicity = operation.Periodicity.Count <= 0 ? Array.Empty<Month>() : new Month[] { (Month)month },
                    SubOperations = operation.SubOperations
                        .Where(e => e.Periodicity.Contains((Month)month))
                        .Select(e => new EquipmentMaintenanceOperation
                        {
                            Id = e.Id,
                            Name = e.Name,
                            Periodicity = new Month[] { (Month)month },
                        })
                        .ToArray(),
                };

                if (equipmentMaintenanceOperation.Periodicity.Any() ||
                    equipmentMaintenanceOperation.SubOperations.Any())
                {
                    result.MaintenanceOperations.Add(equipmentMaintenanceOperation);
                }
            }

            return result;
        }
    }
}
