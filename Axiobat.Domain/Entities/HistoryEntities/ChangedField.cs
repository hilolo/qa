﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class that defines the fields that has changed
    /// </summary>
    public partial class ChangedField
    {
        /// <summary>
        /// the name of the property on the entity
        /// </summary>
        public string PropName { get; set; }

        /// <summary>
        /// the name of the field
        /// </summary>
        public string Champ { get; set; }

        /// <summary>
        /// the before/old value
        /// </summary>
        public string ValeurInitial { get; set; }

        /// <summary>
        /// the after/new value
        /// </summary>
        public string ValeurFinal { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ChangedField"/>
    /// </summary>
    public partial class ChangedField : IEquatable<ChangedField>
    {
        /// <summary>
        /// create an instant of <see cref="ChangedField"/>
        /// </summary>
        public ChangedField()
        {
        }

        /// <summary>
        /// create an instant of <see cref="ChangedField"/>
        /// </summary>
        /// <param name="champ">effected champ with the changes</param>
        /// <param name="valeurInitial">the original value</param>
        /// <param name="valeurFinal">the new value</param>
        public ChangedField(string champ, string valeurInitial, string valeurFinal)
        {
            Champ = PropName = champ;
            ValeurInitial = valeurInitial;
            ValeurFinal = valeurFinal;
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"field: {Champ}, initial Value: {ValeurInitial}, final value: {ValeurFinal}";

        /// <summary>
        /// check if the given object is equals the current instant
        /// </summary>
        /// <param name="obj">the object to check the equality for it</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj) => Equals(obj as ChangedField);

        /// <summary>
        /// check if the given object is equals the current instant
        /// </summary>
        /// <param name="obj">the object to check the equality for it</param>
        /// <returns>true if equals, false if not</returns>
        public bool Equals(ChangedField other)
            => other != null && PropName == other.PropName &&
                   ValeurInitial == other.ValeurInitial &&
                   ValeurFinal == other.ValeurFinal;

        /// <summary>
        /// get the has value of the object
        /// </summary>
        /// <returns>the hash value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = 2062681644;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Champ);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ValeurInitial);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ValeurFinal);
                return hashCode;
            }
        }

        public static bool operator ==(ChangedField left, ChangedField right) => EqualityComparer<ChangedField>.Default.Equals(left, right);
        public static bool operator !=(ChangedField left, ChangedField right) => !(left == right);
    }
}
