﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Axiobat.Domain.Exceptions;
    using Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the details of an order, this order details is inside every document that describe a goods transaction operation
    /// </summary>
    public partial class WorkshopOrderDetails
    {
        /// <summary>
        /// the id of the OrderDetails
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// the proportion percentage value of the unknown/non-estimated charges like electricity, water etc
        /// </summary>
        public float Proportion { get; set; }

        /// <summary>
        /// the global VAT Value applied to the order
        /// </summary>
        public float GlobalVAT_Value { get; set; }

        /// <summary>
        /// PUC: single site policy (construction insurance),
        /// <see href="https://www.assurancedesmetiers.com/puc.php">More details</see>
        /// </summary>
        public float PUC { get; set; }

        /// <summary>
        /// the details of the holdback
        /// </summary>
        public HoldbackDetails HoldbackDetails { get; set; }

        /// <summary>
        /// the type of product details
        /// </summary>
        public OrderProductsDetailsType ProductsDetailsType { get; set; }

        /// <summary>
        /// the pricing details of the products, only look for this property if the <see cref="ProductsDetailsType"/> is equals to
        /// <see cref="OrderProductsDetailsType.File"/>
        /// </summary>
        public CustomProductsDetails ProductsPricingDetails { get; set; }

        /// <summary>
        /// the product order details saved as an attachment
        /// </summary>
        public Attachment ProductsDetails_File { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="WorkshopOrderDetails"/>
    /// </summary>
    public partial class WorkshopOrderDetails : OrderDetails
    {
        /// <summary>
        /// create an instant of <see cref="WorkshopOrderDetails"/>
        /// </summary>
        public WorkshopOrderDetails()
        {
            Id = Generator.GenerateRandomId();
            LineItems = new HashSet<OrderProductDetails>();
        }

        /// <summary>
        /// calculate the total HT of the included products
        /// </summary>
        /// <returns>the total</returns>
        public override float GetTotalHT()
        {
            if (ProductsDetailsType == OrderProductsDetailsType.File)
            {
                if (ProductsPricingDetails is null)
                    throw new AxiobatException("you must supply a Products pricing details, because you ProductsDetailsType is set to file");

                return ProductsPricingDetails.TotalHT;
            }

            return base.GetTotalHT();
        }

        /// <summary>
        /// get the product cost details
        /// </summary>
        /// <returns>the product cost details</returns>
        public override ProductCostDetails GetCostDetails()
        {
            if (ProductsDetailsType == OrderProductsDetailsType.File)
            {
                if (ProductsPricingDetails is null)
                    throw new AxiobatException("you must supply a Products pricing details, because you ProductsDetailsType is set to file");

                return new ProductCostDetails
                {
                    TotalHours = ProductsPricingDetails.TotalHours,
                    TotalMaterialCost = ProductsPricingDetails.MaterialCost,
                    TotalCost = ProductsPricingDetails.HourlyCost * ProductsPricingDetails.TotalHours,
                };
            }

            return base.GetCostDetails();
        }

        /// <summary>
        /// retrieve the list of product with their quantity
        /// </summary>
        public override IEnumerable<ProductsDetails> GetAllProduct()
        {
            if (ProductsDetailsType == OrderProductsDetailsType.File)
            {
                if (ProductsPricingDetails is null)
                    throw new AxiobatException("you must supply a Products pricing details, because the ProductsDetailsType is set to file");

                return new List<ProductsDetails>
                {
                    new ProductsDetails(1, new MinimalProductDetails
                    {
                        HourlyCost = ProductsPricingDetails.HourlyCost,
                        MaterialCost = ProductsPricingDetails.MaterialCost,
                        TotalHours = ProductsPricingDetails.TotalHours,
                    })
                };
            }

            return base.GetAllProduct();
        }
    }
}
