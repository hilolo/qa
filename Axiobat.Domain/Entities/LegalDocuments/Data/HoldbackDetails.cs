﻿namespace Axiobat.Domain.Entities
{
    using System;

    /// <summary>
    /// the details of the Holdback
    /// </summary>
    public class HoldbackDetails
    {
        /// <summary>
        /// the status of the holdback
        /// </summary>
        public string Status { get; set; } = Constants.HoldbackStatus.InProgress;

        /// <summary>
        /// The holdback is a sum of money representing a percentage of the total amount of the work.
        /// Note: this value of this is a percentage
        /// </summary>
        /// <remarks>
        /// It is a term used during a public order corresponding to works contracts.
        /// The retention of guarantee refers to the law of July 16, 1971.
        /// If the client (the owner) or the supervisor (the person who organized and supervised the works)
        /// finds defects or faults, the company having carried out the work is withheld a
        /// sum equal to 5% of the amount of the work. The retention of this sum allows the client to exert
        /// pressure on the company until the work is completed and correctly.
        /// </remarks>
        public float Holdback { get; set; }

        /// <summary>
        /// the Warranty period
        /// </summary>
        public int WarrantyPeriod { get; set; }

        /// <summary>
        /// the Warranty expiration Date
        /// </summary>
        public DateTime WarrantyExpirationDate { get; set; }
    }
}
