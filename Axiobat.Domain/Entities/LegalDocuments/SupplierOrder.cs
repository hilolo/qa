﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent an order to a supplier
    /// </summary>
    [DocType(DocumentType.SupplierOrder)]
    public partial class SupplierOrder
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.SupplierOrder;

        /// <summary>
        /// the date the Supplier Order should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Supplier Order creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        public string SupplierId { get; set; }

        /// <summary>
        /// the supplier associated with this supplier order
        /// </summary>
        public SupplierDocuement Supplier { get; set; }

        /// <summary>
        /// The id of the quote
        /// </summary>
        public string QuoteId { get; set; }

        /// <summary>
        /// the quote associated with the supplier order
        /// </summary>
        public Quote Quote { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// list of memos associated with the invoice
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// the list of expenses associated with this supplier order
        /// </summary>
        public ICollection<SupplierOrders_Expenses> Expenses { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="SupplierOrder"/>
    /// </summary>
    public partial class SupplierOrder : Document
    {
        /// <summary>
        /// create an instant of <see cref="SupplierOrder"/>
        /// </summary>
        public SupplierOrder() : base()
        {
            Expenses = new HashSet<SupplierOrders_Expenses>();
            Memos = new HashSet<Memo>();
        }

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return OrderDetails.TotalHT;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            return OrderDetails.GetTaxDetails().Sum(e => e.TotalTax);
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
        {
            return OrderDetails.TotalTTC;
        }
    }
}
