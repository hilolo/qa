﻿namespace Axiobat.Domain.Entities
{
    using Interfaces;
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent an invoice
    /// </summary>
    [DocType(DocumentType.Invoice)]
    public partial class Invoice
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.Invoice;

        /// <summary>
        /// the type of invoice
        /// </summary>
        public InvoiceType TypeInvoice { get; set; }

        /// <summary>
        /// the situation that this invoice representing, only check this value if the invoice type is deferent from <see cref="InvoiceType.General"/>
        /// </summary>
        public float Situation { get; set; }

        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// invoice creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the invoice order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// whether to include the Lot details or not
        /// </summary>
        public bool LotDetails { get; set; }

        /// <summary>
        /// the id of the Quote
        /// </summary>
        public string QuoteId { get; set; }

        /// <summary>
        /// the information of the client associated with this Quote
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the information of the adresse
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the Quote associated with the Invoice
        /// </summary>
        public Quote Quote { get; set; }

        /// <summary>
        /// the id of the contrac tMaintenance
        /// </summary>
        public string ContractId { get; set; }

        /// <summary>
        /// the Contrat associated with the Invoice
        /// </summary>
        public MaintenanceContract Contract { get; set; }

        /// <summary>
        /// the id of the operation sheet maintenance
        /// </summary>
        public string OperationSheetMaintenanceId { get; set; }

        /// <summary>
        /// the OperationSheetMaintenance associated with the Invoice
        /// </summary>
        public MaintenanceOperationSheet OperationSheetMaintenance { get; set; }

        /// <summary>
        /// list of memos associated with the invoice
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of credit notes associated with this invoice
        /// </summary>
        public ICollection<CreditNote> CreditNotes { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// the list of the payments associated with this invoices
        /// </summary>
        public ICollection<Invoices_Payments> Payments { get; set; }

        /// <summary>
        /// the list of operation sheets associated with this invoice
        /// </summary>
        public ICollection<OperationSheet> OperationSheets { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Invoice"/>
    /// </summary>
    public partial class Invoice : Document, IReferenceable<Invoice>
    {
        /// <summary>
        /// create an instant of <see cref="Invoice"/>
        /// </summary>
        public Invoice() : base()
        {
            Memos = new HashSet<Memo>();
            Emails = new HashSet<DocumentEmail>();
            CreditNotes = new HashSet<CreditNote>();
            Payments = new HashSet<Invoices_Payments>();
            OperationSheets = new HashSet<OperationSheet>();
        }

        /// <summary>
        /// create search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Client.FullName} {Purpose} {Reference}".ToLower();

        /// <summary>
        /// check if we can increment the reference on the update
        /// </summary>
        /// <param name="oldState">the old state of the entity</param>
        /// <returns>true if we can, false if not</returns>
        public bool CanIncrementOnUpdate(Invoice oldState) => base.CanIncrementOnUpdate(oldState);

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return OrderDetails.TotalHT;

            //    if (TypeInvoice == InvoiceType.Situation || TypeInvoice == InvoiceType.Acompte)
            //        return OrderDetails.TotalHT;

            //    var totalHT = OrderDetails.TotalHT;

            //    if (TypeInvoice == InvoiceType.Cloture)
            //    {
            //        if (Quote is null)
            //        {
            //            totalHT -= totalHT * Situation / 100;
            //        }
            //        else
            //        {
            //            var acompte_sutiation_Total = Quote.Situations
            //                .Where(document =>
            //                    document.TypeInvoice == InvoiceType.Acompte ||
            //                    document.TypeInvoice == InvoiceType.Situation
            //                )
            //                .Sum(document => document.TotalHT);

            //            totalHT -= acompte_sutiation_Total;
            //        }
            //    }

            //    return totalHT;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            var totalTax = OrderDetails.GetTaxDetails().Sum(e => e.TotalTax);

            if (TypeInvoice == InvoiceType.Situation || TypeInvoice == InvoiceType.Acompte)
                return totalTax;

            if (TypeInvoice == InvoiceType.Cloture)
            {
                if (Quote is null)
                {
                    totalTax -= totalTax * Situation / 100;
                }
                else
                {
                    var acompte_sutiation_Total = Quote.Situations
                        .Where(document =>
                            document.TypeInvoice == InvoiceType.Acompte ||
                            document.TypeInvoice == InvoiceType.Situation
                        )
                        .Sum(document => document.GetTotalTax());

                    totalTax -= acompte_sutiation_Total;
                }
            }

            return totalTax;
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
            => OrderDetails.TotalTTC;

        /// <summary>
        /// get the rest to pay for the invoice, for this function to work the list of payment should be included
        /// </summary>
        /// <returns>the rest to pay</returns>
        public double GetRestToPay()
        {
            
            var res = GetTotalTTC() - GetTotalPaid();
            if(res>0 && res< 0.01)
            {
                return 0;
            }
            if (res <0)
            {
                return 0;
            }
            else
            {
                return GetTotalTTC() - GetTotalPaid();

            }
        }

        /// <summary>
        /// get the total paid amount, for this function to work the list of payment should be included
        /// </summary>
        /// <returns>the total paid amount</returns>
        public double GetTotalPaid()
            => Payments.Sum(e => e.Amount);
    }
}
