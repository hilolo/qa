﻿namespace Axiobat.Domain.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the relationships between <see cref="Invoice"/> and <see cref="Payment"/>
    /// </summary>
    public partial class Invoices_Payments
    {
        /// <summary>
        /// the id of the invoices
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// the id of the payments
        /// </summary>
        public string PaymentId { get; set; }

        /// <summary>
        /// the amount that the payment is made for the invoice
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// the invoice
        /// </summary>
        public Invoice Invoice { get; set; }

        /// <summary>
        /// the payment
        /// </summary>
        public Payment Payment { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Invoices_Payments"/>
    /// </summary>
    public partial class Invoices_Payments
    {
        /// <summary>
        /// check if the given instant equals to the current instant
        /// </summary>
        /// <param name="other">the object to check</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(Invoices_Payments other)
            => !(other is null) && other.InvoiceId == InvoiceId && other.PaymentId == PaymentId;

        /// <summary>
        /// check if the given instant equals to the current instant
        /// </summary>
        /// <param name="other">the object to check</param>
        /// <returns>true if equals false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Invoices_Payments)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals(obj as Invoices_Payments);
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Invoice: {InvoiceId}, Payment: {PaymentId}";

        /// <summary>
        /// get the hash value of the object
        /// </summary>
        /// <returns>the hash value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 901867850;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(InvoiceId);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PaymentId);
                return hashCode;
            }
        }

        public static bool operator ==(Invoices_Payments left, Invoices_Payments right) => EqualityComparer<Invoices_Payments>.Default.Equals(left, right);
        public static bool operator !=(Invoices_Payments left, Invoices_Payments right) => !(left == right);
    }

    /// <summary>
    /// the EqualityComparer for <see cref="Invoices_Payments"/>
    /// </summary>
    public partial class Invoices_PaymentsEqualityComparer : IEqualityComparer<Invoices_Payments>
    {
        public bool Equals(Invoices_Payments x, Invoices_Payments y)
        {
            if (x is null || y is null) return false;
            if (ReferenceEquals(x, y)) return true;
            return x.InvoiceId == y.InvoiceId && x.PaymentId == y.PaymentId && x.Amount == x.Amount;
        }

        public int GetHashCode(Invoices_Payments obj)
        {
            unchecked
            {
                var hashCode = 901867850;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.InvoiceId);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.PaymentId);
                hashCode = hashCode * -1521134295 + EqualityComparer<double>.Default.GetHashCode(obj.Amount);
                return hashCode;
            }
        }
    }
}
