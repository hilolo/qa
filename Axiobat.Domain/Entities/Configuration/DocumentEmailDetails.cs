﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// a class that defines the document default emails values for email subject and email content
    /// </summary>
    public partial class DocumentEmailDetails
    {
        /// <summary>
        /// default subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// default content
        /// </summary>
        public string Content { get; set; }
    }


}
