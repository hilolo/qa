﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Newtonsoft.Json;

    /// <summary>
    /// the Document Configuration class
    /// </summary>
    public partial class DocumentConfiguration
    {
        /// <summary>
        /// the type of the document this configuration is for
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// whether to include the lot details in the generated document or not
        /// </summary>
        public bool LotDetails { get; set; }

        /// <summary>
        /// the Holdback default value
        /// </summary>
        public float Holdback { get; set; }

        /// <summary>
        /// default document Object
        /// </summary>
        [JsonProperty("Object")]
        public string Purpose { get; set; }

        /// <summary>
        /// default document Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// default document payments terms
        /// </summary>
        [JsonProperty("PaymentTerms")]
        public string PaymentCondition { get; set; }

        /// <summary>
        /// default document header
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// the document email configuration
        /// </summary>
        public DocumentEmailDetails Email { get; set; }

        /// <summary>
        /// the document Reflation Email default value
        /// </summary>
        public DocumentEmailDetails ReflationEmail { get; set; }

        /// <summary>
        /// the default validity of the document
        /// </summary>
        public int Validity { get; set; }

        /// <summary>
        /// the Pdf configurations
        /// </summary>
        public DocumentPDFConfiguration PDFConfiguration { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="DocumentPDFConfiguration"/>
    /// </summary>
    public partial class DocumentConfiguration
    {
        /// <summary>
        /// create an instant of <see cref="DocumentPDFConfiguration"/>
        /// </summary>
        public DocumentConfiguration()
        {
            PDFConfiguration = new DocumentPDFConfiguration();
        }
    }
}
