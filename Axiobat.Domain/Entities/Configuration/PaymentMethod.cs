﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the payment method
    /// </summary>
    public partial class PaymentMethod
    {
        /// <summary>
        /// the name of the payment method
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// is this payment method the default one
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// the type of the payment method
        /// </summary>
        public PaymentMethodType Type { get; set; }

        /// <summary>
        /// list of payments associated with the payment method
        /// </summary>
        public ICollection<Payment> Payments { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="PaymentMethod"/>
    /// </summary>
    public partial class PaymentMethod : Entity<int>
    {
        /// <summary>
        /// create an instant of <see cref="PaymentMethod"/>
        /// </summary>
        public PaymentMethod()
        {
            Type = PaymentMethodType.Custom;
            Payments = new HashSet<Payment>();
        }

        /// <summary>
        /// build the search term of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Value}";

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"id: {Id}, name: {Value}, is default: {IsDefault}";
    }
}
