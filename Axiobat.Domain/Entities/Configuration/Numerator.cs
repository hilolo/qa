﻿namespace Axiobat.Domain.Entities.Configuration
{
    using App.Common;
    using Enums;
    using System;

    /// <summary>
    /// this class represent the counter
    /// </summary>
    public partial class Numerator
    {
        /// <summary>
        /// the type of the document the counter instant is for it
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the Prefix of the reference
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// Counter Length
        /// </summary>
        public int CounterLength { get; set; }

        /// <summary>
        /// date format as <see cref="DateFormat"/>
        /// </summary>
        public DateFormat DateFormat { get; set; }

        /// <summary>
        /// the counter value
        /// </summary>
        public int Counter { get; set; }

        /// <summary>
        /// the Counter
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string Value => Generate();
    }

    /// <summary>
    /// partial part for <see cref="Counter"/>
    /// </summary>
    public partial class Numerator
    {
        /// <summary>
        /// create an instant of <see cref="Counter"/>
        /// </summary>
        public Numerator()
        {

        }

        /// <summary>
        /// generate a reference
        /// </summary>
        /// <param name="incrementCounter">increment counter after generating the reference</param>
        /// <returns>a reference</returns>
        public string Generate(bool incrementCounter = false)
        {
            // start with the prefix
            var reference = $"{Prefix}";

            // add date format
            switch (DateFormat)
            {
                case DateFormat.Year: reference += DateTime.Today.Year; break;
                case DateFormat.Year_Month: reference += $"{DateTime.Today.Year}{DateTime.Today.Month}"; break;
            }

            // set the counter value
            reference += $"{new string('0', Math.Abs(CounterLength - Counter.ToString().Length))}{Counter}";

            // increment if required
            if (incrementCounter)
                Increment();

            // return the value
            return reference;
        }

        /// <summary>
        /// increment Counter value
        /// </summary>
        public void Increment()
            => Counter = Counter.Increment();

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"prefix: {Prefix}, counter: {Counter}, length: {CounterLength}, Date Format: {DateFormat}, Value: {Value}";
    }
}
