﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of changes
    /// </summary>
    public enum ChangesHistoryType
    {
        /// <summary>
        /// an add operation
        /// </summary>
        Added = 0,

        /// <summary>
        /// update operation
        /// </summary>
        Updated = 1,

        /// <summary>
        /// delete operation
        /// </summary>
        Deleted = 2,

        /// <summary>
        /// send email operation
        /// </summary>
        SendEmail = 3,

        /// <summary>
        /// an general information
        /// </summary>
        Information = 4,

        /// <summary>
        /// Duplication operation
        /// </summary>
        Duplicated = 5,
    }
}
