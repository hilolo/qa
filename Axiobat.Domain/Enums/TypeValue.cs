﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// an enum that defines the type of the value, (None, Percentage, Amount)
    /// </summary>
    public enum TypeValue
    {
        /// <summary>
        /// Noting is applicable
        /// </summary>
        None = 0,

        /// <summary>
        /// the discount is a Percentage
        /// </summary>
        Percentage = 1,

        /// <summary>
        /// the discount is an amount
        /// </summary>
        Amount = 2,
    }
}
