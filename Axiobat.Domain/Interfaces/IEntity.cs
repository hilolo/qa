﻿namespace Axiobat.Domain.Interfaces
{
    /// <summary>
    /// a class that defines an entity
    /// </summary>
    public interface IEntity : ISearchable, System.ICloneable
    {
        /// <summary>
        /// the creation time of the model
        /// </summary>
        System.DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// the last time the model has been modified
        /// </summary>
        System.DateTimeOffset? LastModifiedOn { get; set; }
    }

    /// <summary>
    /// a class that defines an entity with a an id
    /// </summary>
    /// <typeparam name="Tkey">the type of the id</typeparam>
    public interface IEntity<Tkey> : IEntity, IPrimaryKey<Tkey> { }
}
