﻿namespace Axiobat.Domain.Constants
{
    /// <summary>
    /// type of the client
    /// </summary>
    public static class ClientType
    {
        /// <summary>
        /// the client is a particular
        /// </summary>
        public const string Particular = "particular";

        /// <summary>
        /// the client is a professional
        /// </summary>
        public const string Professional = "professional";
    }
}
