namespace Axiobat.Test
{
    using Axiobat.Domain.Entities;
    using Axiobat.Domain.Enums;
    using System;
    using Xunit;

    public class RecurringDocumentTestShould
    {
        [Fact]
        public void Check_IF_Finished_SpecificDate()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.SpecificDate,
                PeriodicityType = PeriodicityType.BiMonthly,
                StartingDate = new DateTime(2020, 2, 3, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 3, 10, 12, 33),
                EndingDate = new DateTime(2020, 2, 3, 11, 12, 33),
            };

            Assert.False(recurringDocument.HasFinished);
        }

        [Fact]
        public void Check_IF_Finished_SpecificNumberOfTime()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.SpecificNumberOfTime,
                PeriodicityType = PeriodicityType.BiMonthly,
                StartingDate = new DateTime(2020, 2, 3, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 3, 10, 12, 33),
                Counter = 3,
                OccurringCount = 3,
            };

            Assert.True(recurringDocument.HasFinished);
        }

        [Fact]
        public void Check_IF_Finished_Undifined()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.Undifined,
                PeriodicityType = PeriodicityType.BiMonthly,
                StartingDate = new DateTime(2020, 2, 3, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 3, 10, 12, 33),
            };

            Assert.False(recurringDocument.HasFinished);
        }

        [Fact]
        public void Generate_Next_Occurring_Date_BiMonthly()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.SpecificDate,
                PeriodicityType = PeriodicityType.BiMonthly,
                StartingDate = new DateTime(2020, 2, 3, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 3, 10, 12, 33),
            };

            var next = recurringDocument.GetNextOccurring();

            Assert.Equal(new DateTime(2020, 4, 3, 10, 12, 33), next);
        }

        [Fact]
        public void Generate_Next_Occurring_Date_Annually()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.SpecificDate,
                PeriodicityType = PeriodicityType.Annually,
                StartingDate = new DateTime(2020, 2, 3, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 3, 10, 12, 33),
            };

            var next = recurringDocument.GetNextOccurring();

            Assert.Equal(new DateTime(2021, 2, 3, 10, 12, 33), next);
        }

        [Fact]
        public void Generate_Next_Occurring_Date_Custom_EveryDay()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.SpecificDate,
                PeriodicityType = PeriodicityType.Custom,
                StartingDate = new DateTime(2020, 2, 28, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 28, 10, 12, 33),

                PeriodicityOptions = new PeriodicityOptions
                {
                    RecurringType = PeriodicityRecurringType.EveryDay,
                    RepeatEvery = 3,
                }
            };

            var next = recurringDocument.GetNextOccurring();

            Assert.Equal(new DateTime(2020, 3, 2, 10, 12, 33), next);
        }

        [Fact]
        public void Generate_Next_Occurring_Date_Custom_EveryMonth()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.SpecificDate,
                PeriodicityType = PeriodicityType.Custom,
                StartingDate = new DateTime(2020, 2, 28, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 28, 10, 12, 33),

                PeriodicityOptions = new PeriodicityOptions
                {
                    RecurringType = PeriodicityRecurringType.EveryMonth,
                    RepeatEvery = 2,
                    DayOfMonth = 12,
                }
            };

            var next = recurringDocument.GetNextOccurring();

            Assert.Equal(new DateTime(2020, 4, 12, 10, 12, 33), next);
        }

        [Fact]
        public void Generate_Next_Occurring_Date_Custom_EveryWeek()
        {
            var recurringDocument = new RecurringDocument()
            {
                EndingType = PeriodicityEndingType.SpecificDate,
                PeriodicityType = PeriodicityType.Custom,
                StartingDate = new DateTime(2020, 2, 28, 10, 12, 33),
                NextOccurring = new DateTime(2020, 2, 28, 10, 12, 33),

                PeriodicityOptions = new PeriodicityOptions
                {
                    RecurringType = PeriodicityRecurringType.EveryWeek,
                    RepeatEvery = 2,
                    DaysOfWeek = new[]
                    {
                        DayOfWeek.Monday, // => 24/2/2020
                        DayOfWeek.Friday, // => 28/2/2020
                        DayOfWeek.Sunday, // => 01/3/2020
                    },
                }
            };

            var next = recurringDocument.GetNextOccurring();

            Assert.Equal(new DateTime(2020, 3, 1, 10, 12, 33), next);
        }
    }
}
